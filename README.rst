About Epock
===========

General Purpose
---------------

Epock is a program specialized in rapid pocket volume evaluation for
molecular dynamics simulations. 
This open-source package includes the C++ source code of the command-line
program version, Python scripts to easily plot the program output,
and a graphical user interface plugin for VMD.
Epock developers strongly believe in open-source projects, particularly for
science.
They decided to make Epock's source code accessible to anyone so that
developers could enhance the program over years.

Documentation
-------------

Epock's documentation is hosted in the `documentation section`_ on `Epock's website`_.


Developers
----------

Epock development started in 2013 with a team of 4 main developers.

- Benoist Laurent, Laboratoire de Biochimie Théorique, Paris, France
- Matthieu Chavent, University of Oxford, Oxford, United Kingdom
- Tristan Cragnolini, Laboratoire de Biochimie Théorique, Paris, France
- Caroline Dahl, University of Oxford, Oxford, United Kingdom

This work has been supervised by

- Dr. Marc Baaden, CNRS, Laboratoire de Biochimie Théorique, Paris, France
- Pr. Mark S.P. Sansom, University of Oxford, Oxford, United Kingdom
- Pr. Philippe Derreumaux, Université Paris Diderot, Laboratoire de Biochimie Théorique, Paris, France

Contact and support
-------------------

For any general purpose question on Epock, please contact 
benoist.laurent [at] gmail.com

To report any bug or suggest enhancements, please use `Epock's
issue tracker`_


.. _Epock's issue tracker: http://bitbucket.org/epock/epock/issues?status=new&status=open
.. _documentation section: https://epock.bitbucket.io/docs/index.html
.. _Epock's website: https://epock.bitbucket.io
