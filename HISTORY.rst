
History
=======


1.0.5 (2018-07-05)
------------------

New features:

- setup continuous integration on Epock bitbucket repository

Bug fixes:

- Non contiguous point removal fixed
- Corrected some issues in the unit tests
- Test programs are now linked to pthread (as requested per Google test developers)


1.0.4 (2017-10-12)
------------------

New features:

- Migration from Mercurial source control manager to Git
- Added 130 unit tests
- Several more checks to prevent unexpected program behaviors
- Parameters and values read from configuration file are now case-insensitive
- Added C++11 support for Clang

Bug fixes:

- Fix vector comparison functions
- Fix configuration wrong error messages
- Fix `TrajectoryPDB::getNumberOfFrames()` return value
- Fix `TrajectoryPDB::nextFrame()` which now checks if line starts with `ATOM` 

Other changes:

- Miscellaneous API enhancements
- Miscellaneous prettifications


1.0.3 (2015-12-07)
------------------

Bug fixes:

- Fix core-dumped error that occured when exclude regions overlaped include regions (#48)
- VMD plugin: Fix configuration file writing issues (#42)
- VMD plugin: Fix a permission problem (#47)
- VMD plugin: Fix a drawing bug


1.0.2rc (2014-10-30)
--------------------

Bug fixes:

- Fix the calculations of a set of sheres / atoms

Other changes:

- point_boundaries return a std::array<double, 6>
- point_boundaries: minimum and maximum coordinates are initialized in a safer fashion
- Miscellaneous API enhancements
- Miscellaneous prettifications


1.0.1rc (2014-09-25)
--------------------

- First release