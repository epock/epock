namespace eval ::epock:: {
	
	variable list_value ""
	variable list_value_deft ""	
	variable list_value_ranked_contrib ""
	variable list_value_evol ""
	variable list_value_deft_evol ""	
	variable list_value_ranked_contrib_evol ""	
	variable list_value_ranked_z_evol ""		
	variable id_rep -1
	variable molid_rep 0
	variable id_rep_ref -2
	variable ranked_list "resnum"
}

 
proc ::epock::table_res {filename} {
	
	set ::epock::list_value_deft ""

	tk::toplevel .t

 	wm title .t "Residue Contribution Table for $filename"
 	wm minsize .t 300 400

  	button .t.ranked_z -text "Z rank" -width 30 -command {
  		set ::epock::list_value $::epock::list_value_ranked_z	
  		set ::epock::ranked_list "z"
  	}
  	button .t.ranked_contrib -text "Contrib. rank" -width 30 -command {
  		set ::epock::list_value $::epock::list_value_ranked_contrib
  		set ::epock::ranked_list "contrib"  		
  	}
  	
  	set filename_end "_contrib.dat"
	set f [open "$filename$filename_end"]
	#puts "$filename$filename_end"
	set ::epock::list_value_deft  ""
	while {1} {
    		set line [gets $f]
    		if {[eof $f]} {
        		close $f
        		break
    		}
		puts $line
    		set wordList [regexp -inline -all -- {\S+} $line]  
    		set resname [format "%-15s" [lindex $wordList 0]]
    		set res_z [format "%-8s" [lindex $wordList 1]]
    		set res_contrib [format "%-8s" [lindex $wordList 2]]        
    		lappend ::epock::list_value_deft "$resname $res_contrib $res_z"
	}

  	set ::epock::list_value_ranked_contrib [lsort -real -decreasing -index 1 $::epock::list_value_deft]
  	set ::epock::list_value_ranked_z [lsort -real -decreasing -index 2 $::epock::list_value_deft]
    	set ::epock::list_value $::epock::list_value_deft
  	

	set row 0
	grid [tk::listbox .t.l -listvariable ::epock::list_value -yscrollcommand ".t.s set" -height 20 -width 35 -font Courier] -column 0 -row  $row -sticky nwes 
	grid [ttk::scrollbar .t.s -command ".t.l yview" -orient vertical] -column 1 -row 0 -sticky ns
	incr row	
	grid [ttk::label .t.stat -text "Residue names       contributions           z" -anchor w] -column 0 -row $row -sticky we
	grid [ttk::sizegrip .t.sz] -column 1 -row $row  -sticky se
	incr row
	grid .t.ranked_z -row $row -column 0 -columnspan 1 -pady 5 -padx 5
	incr row
	grid .t.ranked_contrib -row $row -column 0 -columnspan 1 -pady 5 -padx 5


  bind .t.l <<ListboxSelect>> \
  { set curent_sel [expr 1*[.t.l  curselection]]
    set value ""
   	if {$::epock::ranked_list == "contrib"} { 
		set value [lindex $::epock::list_value_ranked_contrib $curent_sel]
   	} elseif {$::epock::ranked_list == "z"} {
		set value [lindex $::epock::list_value_ranked_z $curent_sel]   	
   	} else {
   		set value [lindex $::epock::list_value_deft  $curent_sel]
   	}
   
    #selection handle [.t.l  curselection]
    #tk_messageBox -message "you have selected the value \n $value"
    set value2 [regexp -inline -all -- {\S+} $value]
    set splitted_val [split [lindex $value2 0] :]
    set chain [lindex $splitted_val 0]
    set residue_name  [lindex $splitted_val 1]
    set residue_id  [lindex $splitted_val 2]    
    set ::epock::id_rep_ref [expr [molinfo $::epock::molid_rep get numreps] -1] 
    
    	if {$::epock::id_rep == -1} {
    		mol addrep $::epock::molid_rep
    		set ::epock::id_rep [expr [molinfo $::epock::molid_rep get numreps] -1] 
    		#puts "::epock::$id_rep"
    		#puts "chain $chain and resname $residue_name and resid residue_id"
    		mol modselect $::epock::id_rep $::epock::molid_rep "chain $chain and resname $residue_name and resid $residue_id"
		if {$::epock::martini == 1 } {
    			mol modstyle $::epock::id_rep $::epock::molid_rep VDW 2.0 13
		} else {
    			mol modstyle $::epock::id_rep $::epock::molid_rep Licorice
		}
    		mol modcolor $::epock::id_rep $::epock::molid_rep ColorID 4	
    		set ::epock::id_rep_ref $::epock::id_rep 	
    	} elseif {$::epock::id_rep_ref != $::epock::id_rep} {
			mol addrep $::epock::molid_rep
    		set ::epock::id_rep [expr [molinfo $::epock::molid_rep get numreps] -1] 
    		set ::epock::id_rep_ref $::epock::id_rep
    		#puts "chain $chain and resname $residue_name and resid residue_id"
    		mol modselect $::epock::id_rep $::epock::molid_rep "chain $chain and resname $residue_name and resid $residue_id"
		if {$::epock::martini == 1 } {
    			mol modstyle $::epock::id_rep $::epock::molid_rep VDW 2.0 13
		} else {
	    		mol modstyle $::epock::id_rep $::epock::molid_rep Licorice
		}
    		mol modcolor $::epock::id_rep $::epock::molid_rep ColorID 4	 	
    	} else {
    		mol modselect $::epock::id_rep $::epock::molid_rep "chain $chain and resname $residue_name and resid $residue_id"
    	}
    
      bind .t.l <Leave> {break} 
   }
   
}
   

proc ::epock::table_res_dyn {filename} {
	
	set ::epock::list_value_deft ""
	set ::epock::list_value_deft_evol ""
	
	tk::toplevel .t_dyn

 	wm title .t_dyn "Residue Contribution Table for $filename"
 	wm minsize .t_dyn 300 400

  	button .t_dyn.ranked_z -text "Z rank" -width 30 -command {
  		set ::epock::list_value $::epock::list_value_ranked_z	
  		set ::epock::ranked_list "z"
  	}
  	button .t_dyn.ranked_contrib -text "Contrib. rank" -width 30 -command {
  		set ::epock::list_value $::epock::list_value_ranked_contrib
  		set ::epock::ranked_list "contrib"  		
  	}

  	button .t_dyn.ranked_resnum -text "Res. nb rank" -width 30 -command {
  		set ::epock::list_value $::epock::list_value_deft
  		set ::epock::ranked_list "resnum"  		
  	}
 
   	button .t_dyn.plot_contrib -text "Plot Contrib." -width 30 -command {

	set curent_sel [expr 1*[.t_dyn.l  curselection]]
	
    set value ""
   	if {$::epock::ranked_list == "contrib"} { 
		set value [lindex $::epock::list_value_ranked_contrib_evol $curent_sel]
   	} elseif {$::epock::ranked_list == "z"} {
		set value [lindex $::epock::list_value_ranked_z_evol $curent_sel]   	
   	} else {
   		set value [lindex $::epock::list_value_evol  $curent_sel]
   	}

	set resname [lindex $value 2]
	set value [lreplace $value 0 2]
	puts $::epock::list_frame
	puts $value	
  	set plothandle [multiplot -x $::epock::list_frame -y $value   -title "$resname" -xlabel "Frame nb" -ylabel "Contribution" -lines -linewidth 3 -marker point -plot]
	
  	} 	
  	

  	
  	
  	set filename_end "_contrib.dat"
	set f [open "$filename$filename_end"]
	#puts "$filename$filename_end"
	while {1} {
    	set line [gets $f]
    	if {[eof $f]} {
        	close $f
        	break
    	}
		
    	set wordList [regexp -inline -all -- {\S+} $line]  
    	set resname [format "%-15s" [lindex $wordList 0]]
    	set res_z [format "%-8s" [lindex $wordList 1]]
    	set res_contrib_avg 0
    	set list_contrib ""
    	for {set i 2} {$i < [llength $wordList]} {incr i} {
    		set res_contrib_avg [expr $res_contrib_avg + [lindex $wordList $i]]
    		lappend list_contrib [lindex $wordList $i]
    	}	
    	set res_contrib_avg [expr $res_contrib_avg/(1.00*([llength $wordList]-2)) ]
    	set res_contrib_avg [format "%.1f" $res_contrib_avg]
    	set res_contrib_avg [format "%-8s" $res_contrib_avg ]        
    	lappend ::epock::list_value_deft "$resname $res_contrib_avg $res_z"
    	lappend ::epock::list_value_deft_evol "$res_contrib_avg $res_z $resname $list_contrib"
	}

  	set ::epock::list_value_ranked_contrib [lsort -real -decreasing -index 1 $::epock::list_value_deft]
  	set ::epock::list_value_ranked_z [lsort -real -decreasing -index 2 $::epock::list_value_deft]
    set ::epock::list_value $::epock::list_value_deft
    
  	set ::epock::list_value_ranked_contrib_evol [lsort -real -decreasing -index 0 $::epock::list_value_deft_evol]
  	set ::epock::list_value_ranked_z_evol [lsort -real -decreasing -index 1 $::epock::list_value_deft_evol]
    set ::epock::list_value_evol $::epock::list_value_deft_evol  	

	set row 0
	grid [tk::listbox .t_dyn.l -listvariable ::epock::list_value -yscrollcommand ".t_dyn.s set" -height 20 -width 35 -font Courier] -column 0 -row  $row -sticky nwes 
	grid [ttk::scrollbar .t_dyn.s -command ".t_dyn.l yview" -orient vertical] -column 1 -row 0 -sticky ns
	incr row	
	grid [ttk::label .t_dyn.stat -text "Residue names       contrib_avg         z_avg" -anchor w] -column 0 -row $row -sticky we
	grid [ttk::sizegrip .t_dyn.sz] -column 1 -row $row  -sticky se
	incr row
	grid .t_dyn.ranked_z -row $row -column 0 -columnspan 1 -pady 5 -padx 5
	incr row
	grid .t_dyn.ranked_contrib -row $row -column 0 -columnspan 1 -pady 5 -padx 5
	incr row
	grid .t_dyn.ranked_resnum -row $row -column 0 -columnspan 1 -pady 5 -padx 5
	incr row
	grid .t_dyn.plot_contrib -row $row -column 0 -columnspan 1 -pady 5 -padx 5

  bind .t_dyn.l <<ListboxSelect>> \
  { set curent_sel [expr 1*[.t_dyn.l  curselection]]
    set value ""
   	if {$::epock::ranked_list == "contrib"} { 
		set value [lindex $::epock::list_value_ranked_contrib $curent_sel]
   	} elseif {$::epock::ranked_list == "z"} {
		set value [lindex $::epock::list_value_ranked_z $curent_sel]   	
   	} else {
   		set value [lindex $::epock::list_value_deft  $curent_sel]
   	}
   
    #selection handle [.t_dyn.l  curselection]
    #tk_messageBox -message "you have selected the value \n $value"
    set value2 [regexp -inline -all -- {\S+} $value]
    set splitted_val [split [lindex $value2 0] :]
    set chain [lindex $splitted_val 0]
    set residue_name  [lindex $splitted_val 1]
    set residue_id  [lindex $splitted_val 2]    
    set ::epock::id_rep_ref [expr [molinfo $::epock::molid_rep get numreps] -1] 
    
    	if {$::epock::id_rep == -1} {
    		mol addrep $::epock::molid_rep
    		set ::epock::id_rep [expr [molinfo $::epock::molid_rep get numreps] -1] 
    		#puts "::epock::$id_rep"
    		#puts "chain $chain and resname $residue_name and resid residue_id"
    		mol modselect $::epock::id_rep $::epock::molid_rep "chain $chain and resname $residue_name and resid $residue_id"
		if {$::epock::martini == 1 } {
    			mol modstyle $::epock::id_rep $::epock::molid_rep VDW 2.0 13
		} else {
    			mol modstyle $::epock::id_rep $::epock::molid_rep Licorice
		}
    		mol modcolor $::epock::id_rep $::epock::molid_rep ColorID 4	
    		set ::epock::id_rep_ref $::epock::id_rep 	
    	} elseif {$::epock::id_rep_ref != $::epock::id_rep} {
		mol addrep $::epock::molid_rep
    		set ::epock::id_rep [expr [molinfo $::epock::molid_rep get numreps] -1] 
    		set ::epock::id_rep_ref $::epock::id_rep
    		#puts "chain $chain and resname $residue_name and resid residue_id"
    		mol modselect $::epock::id_rep $::epock::molid_rep "chain $chain and resname $residue_name and resid $residue_id"
		if {$::epock::martini == 1 } {
    			mol modstyle $::epock::id_rep $::epock::molid_rep VDW 2.0 13
		} else {
    			mol modstyle $::epock::id_rep $::epock::molid_rep Licorice
		}
    		mol modcolor $::epock::id_rep $::epock::molid_rep ColorID 4	 	
    	} else {
    		mol modselect $::epock::id_rep $::epock::molid_rep "chain $chain and resname $residue_name and resid $residue_id"
    	}
    
      bind .t_dyn.l <Leave> {break} 
   }
   
}


