
### functions to draw inclusion sphere ----------------------------

proc ::epock::display_maximum_inclusion_volume_sphere {seltxt molid grid_size radius color} {
	
	# calculate the center of the selection used as the center of the cavity	
    	set center [measure center [atomselect $molid $seltxt]]
    
    	graphics $molid color $color
    	graphics $molid material Transparent 
    	graphics $molid sphere " [lindex $center 0]  [lindex $center 1]  [lindex $center 2]" radius $radius resolution 25
}

proc ::epock::display_maximum_inclusion_volume_sphere_byFile {center molid grid_size radius color} {

	graphics $molid color $color
  	graphics $molid material Transparent 
 	 graphics $molid sphere " [lindex $center 0]  [lindex $center 1]  [lindex $center 2]" radius $radius resolution 25
}

### End functions to draw inclusion sphere ------------------------


### functions to draw inclusion cylinder --------------------------

proc ::epock::display_maximum_inclusion_volume_cylinder {seltxt1 seltxt2 molid grid_size radius color} {
	
	# calculate the center of the selection used as the center of the cavity	
    	set center1 [measure center [atomselect $molid $seltxt1]]
    	set center2 [measure center [atomselect $molid $seltxt2]]
    
    	graphics $molid color $color
    	graphics $molid material Transparent 
    	graphics $molid cylinder "[lindex $center1 0]  [lindex $center1 1]  [lindex $center1 2]" "[lindex $center2 0]  [lindex $center2 1]  [lindex $center2 2]" radius $radius resolution 25
}

proc ::epock::display_maximum_inclusion_volume_cylinder_byFile {center1 center2 molid grid_size radius color} {
	puts "Passed to display_maximum_inclusion_volume_cylinder_byFile: $center1 and $center2"
	# calculate the center of the selection used as the center of the cavity	
    
    	graphics $molid color $color
    	graphics $molid material Transparent 
    	graphics $molid cylinder "[lindex $center1 0]  [lindex $center1 1]  [lindex $center1 2]" "[lindex $center2 0]  [lindex $center2 1]  [lindex $center2 2]" radius $radius resolution 25
}

### end functions to draw inclusion cylinder --------------------------

### functions to draw inclusion box --------------------------------

proc ::epock::display_maximum_inclusion_volume_box {seltxt1 x_box y_box z_box molid grid_size color} {
	
	#list_id name
	set list_id {}

	# calculate the center of the selection used as the center of the cavity	
    	set center1 [measure center [atomselect $molid $seltxt1]]
    	set xmin [expr [lindex $center1 0] - $x_box/2]
    	set xmax [expr [lindex $center1 0] + $x_box/2]
    	set ymin [expr [lindex $center1 1] - $y_box/2]
    	set ymax [expr [lindex $center1 1] + $y_box/2]
    	set zmin [expr [lindex $center1 2] - $z_box/2]
    	set zmax [expr [lindex $center1 2] + $z_box/2]
    	    
    	graphics $molid color $color
    	# bottom of the box
    	set id1 [graphics $molid line "$xmin $ymin $zmin" "$xmin $ymax $zmin" width 2]
	lappend list_id $id1 
    	set id2 [graphics $molid line "$xmin $ymin $zmin" "$xmax $ymin $zmin" width 2]	
	lappend list_id $id2
    	set id3 [graphics $molid line "$xmax $ymax $zmin" "$xmax $ymin $zmin" width 2]	
	lappend list_id $id3
    	set id4 [graphics $molid line "$xmax $ymax $zmin" "$xmin $ymax $zmin" width 2]
	lappend list_id $id4
	
    	# top of the box
    	set id5 [graphics $molid line "$xmin $ymin $zmax" "$xmin $ymax $zmax" width 2]
	lappend list_id $id5
    	set id6 [graphics $molid line "$xmin $ymin $zmax" "$xmax $ymin $zmax" width 2]
	lappend list_id $id6	
    	set id7 [graphics $molid line "$xmax $ymax $zmax" "$xmax $ymin $zmax" width 2]
	lappend list_id $id7	
    	set id8 [graphics $molid line "$xmax $ymax $zmax" "$xmin $ymax $zmax" width 2]	
	lappend list_id $id8

    	# sides of the box    
    	set id9 [graphics $molid line "$xmin $ymin $zmin" "$xmin $ymin $zmax" width 2]
	lappend list_id $id9
    	set id10 [graphics $molid line "$xmin $ymax $zmin" "$xmin $ymax $zmax" width 2]
	lappend list_id $id10
    	set id11 [graphics $molid line "$xmax $ymax $zmin" "$xmax $ymax $zmax" width 2]
	lappend list_id $id11
    	set id12 [graphics $molid line "$xmax $ymin $zmin" "$xmax $ymin $zmax" width 2]   
	lappend list_id $id12

	return $list_id    	    		
}

proc ::epock::display_maximum_inclusion_volume_box_byFile {center1 x_box y_box z_box molid grid_size color} {
	
	#list_id name
	set list_id {}

	# calculate the center of the selection used as the center of the cavity	

    	set xmin [expr [lindex $center1 0] - $x_box/2]
    	set xmax [expr [lindex $center1 0] + $x_box/2]
    	set ymin [expr [lindex $center1 1] - $y_box/2]
    	set ymax [expr [lindex $center1 1] + $y_box/2]
    	set zmin [expr [lindex $center1 2] - $z_box/2]
    	set zmax [expr [lindex $center1 2] + $z_box/2]
    	    
    	graphics $molid color $color
    	# bottom of the box
    	set id1 [graphics $molid line "$xmin $ymin $zmin" "$xmin $ymax $zmin" width 2]
	lappend list_id $id1
    	set id2 [graphics $molid line "$xmin $ymin $zmin" "$xmax $ymin $zmin" width 2]	
	lappend list_id $id2
    	set id3 [graphics $molid line "$xmax $ymax $zmin" "$xmax $ymin $zmin" width 2]	
	lappend list_id $id3
    	set id4 [graphics $molid line "$xmax $ymax $zmin" "$xmin $ymax $zmin" width 2]
	lappend list_id $id4
	
    	# top of the box
    	set id5 [graphics $molid line "$xmin $ymin $zmax" "$xmin $ymax $zmax" width 2]
	lappend list_id $id5
    	set id6 [graphics $molid line "$xmin $ymin $zmax" "$xmax $ymin $zmax" width 2]
	lappend list_id $id6	
    	set id7 [graphics $molid line "$xmax $ymax $zmax" "$xmax $ymin $zmax" width 2]
	lappend list_id $id7	
    	set id8 [graphics $molid line "$xmax $ymax $zmax" "$xmin $ymax $zmax" width 2]	
	lappend list_id $id8

    	# sides of the box    
    	set id9 [graphics $molid line "$xmin $ymin $zmin" "$xmin $ymin $zmax" width 2]
	lappend list_id $id9
    	set id10 [graphics $molid line "$xmin $ymax $zmin" "$xmin $ymax $zmax" width 2]
	lappend list_id $id10
    	set id11 [graphics $molid line "$xmax $ymax $zmin" "$xmax $ymax $zmax" width 2]
	lappend list_id $id11
    	set id12 [graphics $molid line "$xmax $ymin $zmin" "$xmax $ymin $zmax" width 2]   
	lappend list_id $id12

	return $list_id    	    		
}



### End functions to draw inclusion box ----------------------------

# launch epock prog. and display the result in VMD with VdW spheres
proc ::epock::launch_epock {molid pdb_path config_path output_path tree_view_path exec_path martini_option} {

	if {$martini_option == 1} {
        puts "command line: $exec_path -s $pdb_path -c $config_path -o $output_path --martini --ox"
		exec $exec_path -s $pdb_path -c $config_path -o $output_path --martini --ox	
	} else {
        puts "command line: $exec_path -s $pdb_path -c $config_path -o $output_path --ox"
		exec $exec_path -s $pdb_path -c $config_path -o $output_path --ox
	}

	set ::epock::molid_volume_list ""

	foreach lvl1 [$tree_view_path children {}] {
		set molnewid [mol new "$lvl1.pdb"]
		mol addfile "$lvl1.xtc"
		set sel [atomselect $molnewid "all"]
		$sel set radius $::epock::padding
		mol modstyle 0 $molnewid VDW
		lappend ::epock::molid_volume_list $molnewid 	
	}
	
}

# launch epock prog. for dynamic trajectory and display the result in VMD with VdW spheres
proc ::epock::launch_epock_dynamic {molid pdb_path trajectory_path config_path output_path tree_view_path exec_path martini_option} {

	if {$martini_option == 1} {
		exec $exec_path -s $pdb_path -f $trajectory_path -c $config_path -o $output_path --martini --ox	
	} else {
		exec $exec_path -s $pdb_path -f $trajectory_path -c $config_path -o $output_path --ox
	}
	
	set ::epock::molid_volume_list ""

	foreach lvl1 [$tree_view_path children {}] {
		set molnewid [mol new "$lvl1.pdb"]
		mol addfile "$lvl1.xtc"
		set sel [atomselect $molnewid "all"]
		$sel set radius $::epock::padding
		mol modstyle 0 $molnewid VDW
		lappend ::epock::molid_volume_list $molnewid 	
	}
	
}




