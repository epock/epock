
proc ::epock::plot_radius {filename} {
	#puts $filename
	set  list_radius ""
	set filename_end "_profile.dat"
	set f [open "$filename$filename_end"]
	while {1} {
    	set line [gets $f]
    	if {[eof $f]} {
        	close $f
        	break
    	}
    	set wordList [regexp -inline -all -- {\S+} $line]    
    	lappend list_radius $wordList
	}
	
	set list_z [lindex $list_radius 0]
	set list_radius [lreplace [lindex $list_radius 1] 0 0]
	#puts $list_z
	#puts $list_radius
	
	set plothandle [multiplot -x $list_radius -y $list_z -title "Radius plot" -xlabel "Radius Size" -ylabel "Z" -lines -linewidth 3 -marker point -plot]
}


proc ::epock::plot_radius_dyn {filename} {
	
	set  list_radius ""
	set filename_end "_profile.dat"
	set f [open "$filename$filename_end"]
	while {1} {
    	set line [gets $f]
    	if {[eof $f]} {
        	close $f
        	break
    	}
    	set wordList [regexp -inline -all -- {\S+} $line]    
    	lappend list_radius $wordList
	}
	
	set list_z [lindex $list_radius 0]
	set list_radius [lreplace $list_radius 0 0]
	
	
	set list_radius_t [transpose $list_radius]	 			
	set list_radius_t [lreplace $list_radius_t 0 0]
	
	set list_avg ""
	set list_min ""
	set list_max ""		
	
   	foreach radius $list_radius_t {
   
   		set value_avg 0
   		set radius_min 9999
		set radius_max 0
		
   		foreach value $radius {
   			
   			if {$value < $radius_min } {
   				set radius_min $value
   			}
   			
   			if {$value > $radius_max } {
   				set radius_max $value
   			}
			
		set value_avg [expr $value_avg + $value]	
   		
   		}
   		
   		set value_avg [expr  $value_avg / ([llength [lindex $list_radius_t 0]]*1.00)]
   		
   		lappend list_avg $value_avg
		lappend list_min $radius_min
		lappend list_max $radius_max	
   	}
  	 	
	set plothandle [multiplot -x $list_avg -y $list_z -title "Radius plot" -xlabel "Radius Size" -ylabel "Z" -lines -linewidth 3 -marker point -plot]
	$plothandle add $list_min $list_z -fillcolor green -linewidth 3 -dash - -plot
	$plothandle add $list_max $list_z -fillcolor green -linewidth 3 -dash .	-plot
}


proc transpose {ll} {
    set transpose [list]
    foreach l $ll {
        set ntranspose [list]
        foreach e $l t $transpose {
            lappend t $e
            lappend ntranspose $t
        }
        set transpose $ntranspose
    }
    return $transpose
}
