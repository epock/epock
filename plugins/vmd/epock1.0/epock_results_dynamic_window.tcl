namespace eval ::epock:: {
	
	variable volume_dynamic ""
	variable volume_name ""
	variable tree_view ""
	variable list_cavity ""
	variable list_frame ""	
}
proc ::epock::epock_results_dynamic_GUI {filename molid tree_view_path} {
	
		#set molnewid [mol new "$lvl1.pdb"]
		#mol addfile "$lvl1.xtc"
		#mol modstyle 0 $molnewid VDW	
		set list_cavity ""
		set ::epock::list_cavity ""

		foreach lvl1 [$tree_view_path children {}] {
		lappend list_cavity $lvl1		
		}

		set ::epock::volume_name $filename
		set ::epock::tree_view $tree_view_path

		set f [open "volume.dat"]
		set counter 0
		set frames_graph 0
		set ::epock::list_cavity ""
		set ::epock::list_frame  ""
		while {1} {
    			set line [gets $f]
    			if {[eof $f]} {
        			close $f
        			break
    			}
    			set wordList [regexp -inline -all -- {\S+} $line] 		
		
			if {$counter > 0} {
 
				for {set i 1} {$i < [llength $wordList]} {incr i} {
					set list_temp [lindex $::epock::list_cavity [expr $i-1]] 
					lappend list_temp [lindex $wordList $i]
					set ::epock::list_cavity [lreplace $::epock::list_cavity [expr $i-1] [expr $i-1] $list_temp]
				}    		
			
			lappend ::epock::list_frame $frames_graph		
			set frames_graph [expr $frames_graph + $::epock::trajectory_step]
			
			} else {	
				for {set i 1} {$i < [llength $wordList]} {incr i} {
	   				lappend ::epock::list_cavity [lindex $wordList $i]
				} 
			
			}
			
			incr counter
		}
	
		
		set welcome_message ""
	
		variable rw
		if { [winfo exists .epock_res] } {
        		wm deiconify $rw
        		return
    		}
		
    		set rw [tk::toplevel ".epock_res"]
    		wm title $rw "Analysis of results"	
    		frame $rw.top
    
    		wm minsize $rw 400 200
    
    		set welcome_message "the volume(s) is (are): \n"
    		message $rw.welcome -width 370 -text "$welcome_message"

		button $rw.volume_time -text "Volume Vs Time" -width 50 -command {
			set cavity_id [.epock_res.tree selection]
			::epock::plot_volume $::epock::list_cavity $::epock::list_frame $cavity_id
  		}  
  		
		button $rw.residue_contribution -text "Residues Contribution" -width 50 -command {
			set cavity_id [.epock_res.tree selection]
  			::epock::table_res_dyn $cavity_id
  		}   

		button $rw.radius_calculation -text "Radius Profile" -width 50 -command {
			set cavity_id [.epock_res.tree selection]
  			::epock::plot_radius_dyn $cavity_id
  		}   

		button $rw.center_pos -text "Center pos." -width 50 -command {
			set cavity_id [.epock_res.tree selection]
  			::epock::show_center $cavity_id
  		}  
  	

		# create a treeview to store cavity/pore infos
		ttk::treeview $rw.tree -columns "Volume"
		set ::epock::treeview $rw.tree
		$rw.tree column Volume -width 50 -anchor center 
		$rw.tree heading Volume -text "Volume"
  	   	foreach cavity $::epock::list_cavity {
			if { [.epock_res.tree exists [lindex $cavity 0]] == 1} {
				set id [.epock_res.tree insert [lindex $cavity 0] 0 -text "[lindex $cavity 0]"]
				.epock_res.tree set  $id Volume "[::epock::volume_avg $cavity]"
			} else {
				$rw.tree insert {} end -id [lindex $cavity 0] -text "[lindex $cavity 0]"
				$rw.tree set  [lindex $cavity 0] Volume "[::epock::volume_avg $cavity]"
				$rw.tree selection set [lindex $cavity 0]
			}
		}

   		set row 1
       	incr row
		grid $rw.welcome -row $row -column 0 -sticky w -padx 20
		incr row
		grid $rw.tree -row  $row -column 0 -columnspan 2 -sticky w	
		incr row
      	grid $rw.volume_time -row $row -column 0 -sticky w -padx 20
		incr row
    	grid $rw.residue_contribution -row $row -column 0 -sticky w -padx 20
		incr row
    	grid $rw.radius_calculation -row $row -column 0 -sticky w -padx 20
		incr row
    	grid $rw.center_pos -row $row -column 0 -sticky w -padx 20

 		
		#wm protocol $rw WM_DELETE_WINDOW {
		#	wm withdraw $rw
		#}     
	

}

proc ::epock::plot_volume {list_volume list_frames cavity_id} {
	
  	   	foreach cavity $list_volume {

		#puts $cavity
		#puts [lindex $cavity 0]
		#puts $cavity_id
		set cavity_name [lindex $cavity 0]
		set list_volume_plot [lreplace $cavity 0 0] 
		 	
		 	if {$cavity_name == $cavity_id} {
				#puts $list_volume_plot 
				#puts $list_frames
			
			    set plothandle [multiplot -x $list_frames -y $list_volume_plot  -title "Volume plot" -xlabel Frame nb" -ylabel "Volume" -lines -linewidth 3 -marker point -plot]
				
			}
		}
}


proc ::epock::volume_avg {volume_list} {

  set volume_avg 0
  for {set i 1} {$i < [llength $volume_list]} {incr i} {
  	set volume_avg [expr $volume_avg + [lindex $volume_list $i]]
  }	
	
 set volume_avg [expr $volume_avg/( [llength $volume_list] - 1.00)]		

  return $volume_avg
}





