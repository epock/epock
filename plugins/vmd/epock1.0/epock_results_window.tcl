namespace eval ::epock:: {
	
	variable volume ""
	variable volume_name ""
	variable tree_view ""
	variable list_cavity ""
}
proc ::epock::epock_results_GUI {filename molid tree_view_path} {
	
		#set molnewid [mol new "$lvl1.pdb"]
		#mol addfile "$lvl1.xtc"
		#mol modstyle 0 $molnewid VDW	
		set list_cavity ""
		set ::epock::list_cavity ""

		foreach lvl1 [$tree_view_path children {}] {
		lappend list_cavity $lvl1		
		}

		set ::epock::volume_name $filename
		set ::epock::tree_view $tree_view_path

		set f [open "volume.dat"]
		set counter 0
		while {1} {
    			set line [gets $f]
    			if {[eof $f]} {
        			close $f
        			break
    			}
    			set wordList [regexp -inline -all -- {\S+} $line] 		
		
			if {$counter > 0} { 
		
				#puts "time= [lindex $wordList 0]"
			for {set i 1} {$i < [llength $wordList]} {incr i} {
	   			#puts "volume [lindex $wordList $i]"
				set list_temp [lindex $::epock::list_cavity [expr $i-1]] 
				lappend list_temp [lindex $wordList $i]
				set ::epock::list_cavity [lreplace $::epock::list_cavity [expr $i-1] [expr $i-1] $list_temp]
			}    		
		
			} else {	
				for {set i 1} {$i < [llength $wordList]} {incr i} {
	   			lappend ::epock::list_cavity [lindex $wordList $i]
				} 
			}
			incr counter
		}
	
		puts $::epock::list_cavity

		set welcome_message ""
	
		variable rw
		if { [winfo exists .epock_res] } {
        		wm deiconify $rw
        		return
    		}
		
    		set rw [tk::toplevel ".epock_res"]
    		wm title $rw "Analysis of results"	
    		frame $rw.top
    
    		wm minsize $rw 400 200
    
    		set welcome_message "the volume(s) is (are): \n"
    		message $rw.welcome -width 370 -text "$welcome_message"


		button $rw.residue_contribution -text "Residues Contribution" -width 50 -command {
			set cavity_id [.epock_res.tree selection]
  			::epock::table_res $cavity_id
  		}   

		button $rw.radius_calculation -text "Radius Profile" -width 50 -command {
			set cavity_id [.epock_res.tree selection]
  			::epock::plot_radius $cavity_id
  		}   

		button $rw.center_pos -text "Center pos." -width 50 -command {
			set cavity_id [.epock_res.tree selection]
  			::epock::show_center $cavity_id
  		}  
 
		button $rw.volmap -text "display surface" -width 50 -command {
			set cavity_id [.epock_res.tree selection]
			set curent_sel [expr 1*[.epock_res.tree  index $cavity_id]]
			::epock::display_volmap_cavity [lindex $::epock::molid_volume_list $curent_sel]
  		}   	

		# create a treeview to store cavity/pore infos
		ttk::treeview $rw.tree -columns "Volume"
		set ::epock::treeview $rw.tree
		$rw.tree column Volume -width 50 -anchor center 
		$rw.tree heading Volume -text "Volume"
  	   	foreach cavity $::epock::list_cavity {
			if { [.epock_res.tree exists [lindex $cavity 0]] == 1} {
				set id [.epock_res.tree insert [lindex $cavity 0] 0 -text "[lindex $cavity 0]"]
				.epock_res.tree set  $id Volume "[lindex $cavity 1]"
				$rw.tree selection set [lindex $cavity 1]
			} else {
				$rw.tree insert {} end -id [lindex $cavity 0] -text "[lindex $cavity 0]"
				$rw.tree set  [lindex $cavity 0] Volume "[lindex $cavity 1]"
				$rw.tree selection set [lindex $cavity 0]
			}
		}

   		set row 1
    	incr row
		grid $rw.welcome -row $row -column 0 -sticky w -padx 20
		incr row
		grid $rw.tree -row  $row -column 0 -columnspan 2 -sticky w	
		incr row
    	grid $rw.residue_contribution -row $row -column 0 -sticky w -padx 20
		incr row
    	grid $rw.radius_calculation -row $row -column 0 -sticky w -padx 20
		incr row
    	grid $rw.center_pos -row $row -column 0 -sticky w -padx 20
		incr row
      	grid $rw.volmap -row $row -column 0 -sticky w -padx 20
 		
		#wm protocol $rw WM_DELETE_WINDOW {
		#	wm withdraw $rw
		#}     
	

}


# create a volmap density visualization
proc ::epock::display_volmap_cavity {molid} {
	#mol showrep $molnewid 0 0

	volmap density [atomselect $molid "all"] -mol $molid -res 0.5 -weight mass
	mol addrep $molid
	mol modstyle 1 $molid Isosurface 0.2 5 0 0 1 1
	mol top $molid
	display resetview

}

# load the center position
proc ::epock::show_center {cavity} {

	set molnewid [mol new "${cavity}_center.pdb"]
	mol addfile "${cavity}_center.xtc"
	mol modstyle 0 $molnewid VDW
	animate delete beg 0 end 0 $molnewid
}


