

#include "../src/Atom.hpp"

#include <gtest/gtest.h>
#include <string>

class AtomTest: public ::testing::Test
{
    
    public:
        AtomTest(): aInit(), a() {}

    protected:
        virtual void SetUp()
        {
            aInit.name = "N1";
            aInit.resname = "ALA";
            aInit.element = "N";
            aInit.chain = "A";
            aInit.index = 42;
            aInit.resid = 17;
            aInit.vdw = 1.4;
        }

        Atom a, aInit;
};


// -- Initialization ----------------------------------------------------------
TEST_F(AtomTest, NonInitialized)
{
    EXPECT_EQ("X", a.name);
    EXPECT_EQ("XXX", a.resname);
    EXPECT_EQ("X", a.element);
    EXPECT_EQ("", a.chain);
    EXPECT_EQ(0, a.index);
    EXPECT_EQ(0, a.resid);
    EXPECT_FLOAT_EQ(-1.0, a.vdw);
    EXPECT_EQ(0, a.serial);    
}

TEST_F(AtomTest, Initialized)
{
    EXPECT_EQ("N1", aInit.name);
    EXPECT_EQ("ALA", aInit.resname);
    EXPECT_EQ("N", aInit.element);
    EXPECT_EQ("A", aInit.chain);
    EXPECT_EQ(42, aInit.index);
    EXPECT_EQ(17, aInit.resid);
    EXPECT_FLOAT_EQ(1.4, aInit.vdw);
    EXPECT_EQ(1, aInit.serial);
}


// -- ToString functions ------------------------------------------------------
TEST_F(AtomTest, getIdentifier)
{
    EXPECT_EQ("XXX:0:X", a.getIdentifier());
    EXPECT_EQ("XXX:0", a.getIdentifier(false));

    EXPECT_EQ("A:ALA:17:N1", aInit.getIdentifier());
    EXPECT_EQ("A:ALA:17", aInit.getIdentifier(false));
}

TEST_F(AtomTest, topdb)
{
    EXPECT_EQ("ATOM      0    X XXX     0    9999.9009999.9009999.900  1.00  0.00           X", a.topdb());
    EXPECT_EQ("ATOM     42   N1 ALA A  17    9999.9009999.9009999.900  1.00  0.00           N", aInit.topdb());
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

