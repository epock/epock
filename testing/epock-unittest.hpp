
#ifndef __EPOCK_UNITTEST_HPP__
#define __EPOCK_UNITTEST_HPP__

#include "../src/Vec3f.hpp"

#include <gtest/gtest.h>
#include <array>
#include <vector>

/* --------------------------------------------------------------------------
 * Templates for array comparison.
 * -------------------------------------------------------------------------- */
template<typename T> 
::testing::AssertionResult ArraysMatch(const std::vector<T> (&expected), 
                                       const std::vector<T> (&actual))
{
    if (expected.size() != actual.size())
    {

        return ::testing::AssertionFailure() << "array sizes differ: " << 
                expected.size() << " (expected) != " << actual.size() << " (actual)";
    }
    for (size_t i(0); i < expected.size(); ++i)
    {
        if (expected[i] != actual[i])
        {
            return ::testing::AssertionFailure() 
                << "expected["    << i << "] (" << expected[i] << ") != "
                << "actual["      << i << "] (" << actual[i] << ")";
        }
    }
    return ::testing::AssertionSuccess();
}


template<typename T, size_t N>
::testing::AssertionResult ArraysMatch(const std::array<T, N> (&expected),
                                       const std::array<T, N> (&actual))
{
    std::vector<T> a(expected.begin(), expected.end());
    std::vector<T> b(actual.begin(), actual.end());
    return ArraysMatch(a, b);
}

::testing::AssertionResult ArraysMatch(const Vec3f (&expected),
                                       const Vec3f (&actual))
{
    std::vector<float> a({expected.x, expected.y, expected.z});
    std::vector<float> b({actual.x, actual.y, actual.z});
    return ArraysMatch(a, b);
}

#endif
