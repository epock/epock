

#include "epock-unittest.hpp"
#include "test-Topology.hpp"
#include "../src/Topology.hpp"
#include "../src/Atom.hpp"

#include <cstdio>
#include <fstream>
#include <vector>


#include <iostream>
using namespace std;

class TopologyTest: public ::testing::Test
{
    public:
        TopologyTest() {}

        virtual void SetUp()
        {
            // Write temporary PDB file.
            ofs.open(tmpfilename);
            ASSERT_TRUE(ofs.is_open());
            top.setFilename(tmpfilename);
        }

        virtual void TearDown()
        {
            // Remove temporary topology file.
            remove(tmpfilename.c_str());
        }

    protected:
        Topology top;
        std::string tmpfilename;
        std::ofstream ofs;
};

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Dum Files

///////////////////////////////////////////////////////////////////////////////

class TopologyDumTest: public TopologyTest
{
    public:
        TopologyDumTest()
        {
            tmpfilename = "foo.dum";
        }
};

TEST_F(TopologyDumTest, UnrecognizedExtension)
{
    // Write file content and close file.
    ofs << "";
    ofs.close();

    // Check if programs dies with expected error message.
    EXPECT_DEATH(top.read(), "foo.dum: unrecognized extension '.dum'");
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark PDB Files

///////////////////////////////////////////////////////////////////////////////

class TopologyPDBTest: public TopologyTest
{
    public:
        TopologyPDBTest()
        {
            tmpfilename = "foo.pdb";
        }
};

TEST_F(TopologyPDBTest, readEmptyPDB)
{
    // Write file content and close file.
    ofs << "";
    ofs.close();

    // Check if programs dies with expected error message.
    std::string errormsg = tmpfilename + ": empty file";
    EXPECT_DEATH(top.read(), errormsg);
}

TEST_F(TopologyPDBTest, readPDB)
{
    // Write file content and close file.
    ofs << PDBSTRING;
    ofs.close();

    // Read file.    
    top.read();

    // Check read the right number of atoms.
    ASSERT_EQ(1434, top.getAtoms().size());

    // Check properties read for 1st atom are ok.
    Atom atom = top.getAtoms()[0];
    EXPECT_EQ("N", atom.name);
    EXPECT_EQ("LYS", atom.resname);
    EXPECT_EQ("N", atom.element);
    EXPECT_EQ("A", atom.chain);
    EXPECT_EQ(1, atom.index);
    EXPECT_EQ(1, atom.resid);
    EXPECT_EQ(0, atom.serial);
    EXPECT_EQ(VDW_RADII.at(atom.element), atom.vdw);
    EXPECT_FLOAT_EQ(-8.655, atom.x.x);
    EXPECT_FLOAT_EQ(5.770, atom.x.y);
    EXPECT_FLOAT_EQ(8.371, atom.x.z);
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark GRO Files

///////////////////////////////////////////////////////////////////////////////

class TopologyGROTest: public TopologyTest
{
    public:
        TopologyGROTest()
        {
            tmpfilename = "foo.gro";
        }
};

TEST_F(TopologyGROTest, readEmptyGRO)
{
    // Write file content and close file.
    ofs << "";
    ofs.close();

    // Check if programs dies with expected error message.
    std::string errormsg = tmpfilename + ": empty file";
    EXPECT_DEATH(top.read(), errormsg);
}

TEST_F(TopologyGROTest, readGRO)
{
    // Write file content and close file.
    ofs << GROSTRING;
    ofs.close();

    // Read file.
    top.read();

    // Check read the right number of atoms.
    ASSERT_EQ(1434, top.getAtoms().size());

    // Check properties read for 1st atom are ok.
    Atom atom = top.getAtoms()[0];
    EXPECT_EQ("N", atom.name);
    EXPECT_EQ("LYS", atom.resname);
    EXPECT_EQ("N", atom.element);
    EXPECT_EQ("", atom.chain);
    EXPECT_EQ(1, atom.index);
    EXPECT_EQ(1, atom.resid);
    EXPECT_EQ(0, atom.serial);
    EXPECT_EQ(VDW_RADII.at(atom.element), atom.vdw);
    EXPECT_FLOAT_EQ(-8.65, atom.x.x);
    EXPECT_FLOAT_EQ(5.77, atom.x.y);
    EXPECT_FLOAT_EQ(8.37, atom.x.z);
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Set Atom Radius

///////////////////////////////////////////////////////////////////////////////

// -- Set atom radius ---------------------------------------------------------
class TopologyTestSetRadius: public ::testing::Test
{
    public:
        TopologyTestSetRadius(): natoms(10) {};

        virtual void SetUp()
        {
            // Populate a topology with dummy atoms.
            
            std::vector<Atom> & atoms = top.getAtoms();
            ASSERT_EQ(0, top.getNumberOfAtoms());
            for (size_t i=0; i<natoms; ++i)
            {
                atoms.push_back(Atom());
            }
            ASSERT_EQ(natoms, top.getNumberOfAtoms());
        }

    protected:
        Topology top;
        size_t natoms;

};

TEST_F(TopologyTestSetRadius, WithFile)
{
    // Write temporary radii file.
    std::string tmpfilename = "radii.txt";
    std::ofstream tmpofile(tmpfilename);
    ASSERT_TRUE(tmpofile.is_open());
    tmpofile << RADIISTRING;
    tmpofile.close();

    // Set all topology atom name to H.
    for (size_t i=0; i<natoms; ++i)
    {
        top.getAtoms()[i].name = "H";
    }

    // Set atom radius (all atoms initialized
    // to 12, see test-Topology.hpp).
    top.setRadii(tmpfilename);

    // Check atom radius value.
    std::vector<Atom> & atoms = top.getAtoms();
    for (size_t i=0; i<natoms; ++i)
    {
        ASSERT_FLOAT_EQ(12.0f, atoms[i].vdw);
    }

    // Remove temporary file.
    remove(tmpfilename.c_str());

}

TEST_F(TopologyTestSetRadius, WithVector)
{
    // Initialize radius vector.
    std::vector<float> radii(natoms);
    for (size_t i=0; i<natoms; ++i)
    {
        radii[i] = i;
    }

    // Set atom radius.
    top.setRadii(radii);

    // Check atom radius value.
    std::vector<Atom> & atoms = top.getAtoms();
    for (size_t i=0; i<natoms; ++i)
    {
        ASSERT_FLOAT_EQ(radii[i], atoms[i].vdw);
    }
}

TEST_F(TopologyTestSetRadius, WithFloat)
{
    // Set same radius to all atoms.
    float radius = 7.0f;
    top.setRadii(radius);

    // Check atom radius value.
    std::vector<Atom> & atoms = top.getAtoms();
    for (size_t i=0; i<natoms; ++i)
    {
        ASSERT_FLOAT_EQ(radius, atoms[i].vdw);
    }
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Main Function

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

