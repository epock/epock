

#include "../src/ConfigParserEpock.hpp"
#include "../src/Cavity.hpp"

#include <gtest/gtest.h>
#include <fstream>


const char * CFG_3_IN_3_EX =
    "[cav1]\n"
    "include_sphere    =     1.90    26.70   -21.91  8.0             \n"
    "include_box       =     1.90    26.70   -21.91  8.0 8.0 8.0     \n"
    "include_cylinder  =     1.90    26.70   -21.91  8.0 8.0 8.0 8.0 \n"
    "exclude_sphere    =     1.90    26.70   -21.91  8.0             \n"
    "exclude_box       =     1.90    26.70   -21.91  8.0 8.0 8.0     \n"
    "exclude_cylinder  =     1.90    26.70   -21.91  8.0 8.0 8.0 8.0 \n";


class ConfigParserEpockTest: public ::testing::Test
{
    public:
        ConfigParserEpockTest(): config(), cavities() {};

        virtual void SetUp()
        {
            config.setCavities(&cavities);
        }

    protected:
        ConfigParserEpock config;
        std::vector<Cavity *> cavities;
};


// -- Initialization ----------------------------------------------------------
TEST_F(ConfigParserEpockTest, NonInitialized)
{
    EXPECT_EQ("", config.getFilename());
    EXPECT_TRUE(config.getSections().empty());
    EXPECT_EQ(0, config.getDefaultSection().getNumberOfParameters());
    EXPECT_EQ("", config.getDefaultSection().getName());
}

// -- Read --------------------------------------------------------------------
TEST_F(ConfigParserEpockTest, ReadEmpty)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(0, config.getSections().size());

    // An empty configuration as still 3 default parameters namely
    // grid_spacing, padding and precision with default values.
    ConfigSection defaultsec = config.getDefaultSection();
    ASSERT_EQ(3, defaultsec.getNumberOfParameters());
    EXPECT_EQ(EpockDefaultParameters::defaultGridSpacing, defaultsec.getParameter("grid_spacing"));
    EXPECT_EQ(EpockDefaultParameters::defaultPadding, defaultsec.getParameter("padding"));
    EXPECT_EQ(EpockDefaultParameters::defaultPrecision, defaultsec.getParameter("precision"));
}

TEST_F(ConfigParserEpockTest, DefaultParametersAssignement)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_3_IN_3_EX << std::endl;
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(1, cavities.size());

    Cavity * cav = cavities[0];

    float f;
    from_string<float> (f, EpockDefaultParameters::defaultPrecision);
    EXPECT_FLOAT_EQ(1/f, cav->getPrecision());

    from_string<float> (f, EpockDefaultParameters::defaultPadding);
    EXPECT_FLOAT_EQ(f, cav->getPadding());

    from_string<float> (f, EpockDefaultParameters::defaultGridSpacing);
    EXPECT_FLOAT_EQ(f, cav->getGridSpacing());
}

TEST_F(ConfigParserEpockTest, ReadRegions)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_3_IN_3_EX << std::endl;
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(1, cavities.size());

    Cavity * cav = cavities[0];
    EXPECT_EQ(3, cav->getNumberOfIncludeRegions());
    EXPECT_EQ(3, cav->getNumberOfExcludeRegions());
}


/* --------------------------------------------------------------------------
 * Generic class for ConfigParserEpock death tests.
 * -------------------------------------------------------------------------- */
class ConfigParserEpockDeathTest: public ConfigParserEpockTest
{
    public:
        ConfigParserEpockDeathTest(): 
                param(),
                ssuccess(),
                sfail(),
                errormsg()
         {}

         inline void setParam(const std::string s) { param = s; }
         inline void setSuccessString(const std::string s) { ssuccess = s; }
         inline void setFaillureString(const std::string s) { sfail = s; }

         void run(void)
         { 
            _runSuccess(); 
            _runFail();
         }

    protected:
        std::string param, ssuccess, sfail;
        std::string errormsg;

        void _prerun(bool success)
        {
            std::string fmt = "[cav1]\n"
                              "include_sphere=1.0 1.0 1.0 10.0\n"
                              "%s = %s";
            std::string cfg;
            if (success) cfg = String::format(fmt, param.c_str(), ssuccess.c_str());
            else         cfg = String::format(fmt, param.c_str(), sfail.c_str());

            const char * fname = "dummy.cfg";

            // Write dummy config file.
            std::ofstream cfgout(fname);
            ASSERT_TRUE(cfgout.is_open());
            cfgout << cfg << std::endl;
            cfgout.close();

            config.setFilename(fname);
        }

        void _runFail(void)
        {
            _prerun(false);
            EXPECT_DEATH(config.read(), errormsg);
        }

        void _runSuccess(void)
        {
            _prerun(true);
            config.read();
        }
};

/* --------------------------------------------------------------------------
 * Test faillure on unknown argument.
 * -------------------------------------------------------------------------- */

class ConfigParserEpockUnknownArgumentTest: public ConfigParserEpockDeathTest
{
    public:
        virtual void SetUp()
        {
            ConfigParserEpockDeathTest::SetUp();
            errormsg = "epock) ..:..:.. - FATAL ERROR: "
                       "Unknown parameter '.*' in configuration file.";
        }

        void run(void) { _runFail(); }
};


TEST_F(ConfigParserEpockUnknownArgumentTest, Fail)
{
    setParam("includesphere");
    setFaillureString("1.0 1.0 1.0 1.0");
    run();
}

/* --------------------------------------------------------------------------
 * Test string case indifference.
 * -------------------------------------------------------------------------- */

class ConfigParserEpockCaseTest: public ConfigParserEpockDeathTest
{
    public:
        virtual void SetUp()
        {
            ConfigParserEpockDeathTest::SetUp();
            errormsg = "epock) ..:..:.. - FATAL ERROR: "
                       "Unknown parameter '.*' in configuration file.";
        }

        void run(void) { _runSuccess(); }
};

TEST_F(ConfigParserEpockCaseTest, Parameter)
{
    setParam("INCLUDE_SPHERE");
    setSuccessString("1.0 1.0 1.0 1.0");
    run();
    setParam("include_sphere");
    setSuccessString("1.0 1.0 1.0 1.0");
}

TEST_F(ConfigParserEpockCaseTest, Value)
{
    setParam("contiguous");
    setSuccessString("true");
    run();
    setParam("CONTIGUOUS");
    setSuccessString("true");
}


/* --------------------------------------------------------------------------
 * Number of arguments tests.
 * -------------------------------------------------------------------------- */

class ConfigParserEpockNumberOfArgumentsTest: public ConfigParserEpockDeathTest
{
    public:
        virtual void SetUp()
        {
            ConfigParserEpockDeathTest::SetUp();
            errormsg = "epock) ..:..:.. - FATAL ERROR: "
                       "Bad format: .*";
        }
};

TEST_F(ConfigParserEpockNumberOfArgumentsTest, IncludeSphere)
{
    setParam("include_sphere");
    setSuccessString ("1.90    26.70   -21.91   8.0");
    setFaillureString("1.90    26.70   -21.91      ");
    run();
}

TEST_F(ConfigParserEpockNumberOfArgumentsTest, ExcludeSphere)
{
    setParam("exclude_sphere");
    setSuccessString ("1.90    26.70   -21.91   8.0");
    setFaillureString("1.90    26.70   -21.91      ");
    run();
}

TEST_F(ConfigParserEpockNumberOfArgumentsTest, IncludeBox)
{
    setParam("include_box");
    setSuccessString ("1.90    26.70   -21.91  10 10 10");
    setFaillureString("1.90    26.70   -21.91  10 10   ");
    run();
}

TEST_F(ConfigParserEpockNumberOfArgumentsTest, ExcludeBox)
{
    setParam("exclude_box");
    setSuccessString ("1.90    26.70   -21.91  10 10 10");
    setFaillureString("1.90    26.70   -21.91  10 10   ");
    run();
}

TEST_F(ConfigParserEpockNumberOfArgumentsTest, IncludeCylinder)
{
    setParam("include_cylinder");
    setSuccessString ("1.90    26.70   -21.91  10 10 10 8.0");
    setFaillureString("1.90    26.70   -21.91  10 10 10    ");
    run();
}

TEST_F(ConfigParserEpockNumberOfArgumentsTest, ExcludeCylinder)
{
    setParam("exclude_cylinder");
    setSuccessString ("1.90    26.70   -21.91  10 10 10 8.0");
    setFaillureString("1.90    26.70   -21.91  10 10 10    ");
    run();
}


/* --------------------------------------------------------------------------
 * Boolean conversion tests.
 * -------------------------------------------------------------------------- */
class ConfigParserEpockBoolConversionTest: public ConfigParserEpockDeathTest
{
    public:
        virtual void SetUp()
        {
            ConfigParserEpockDeathTest::SetUp();
            errormsg = "epock) ..:..:.. - FATAL ERROR: "
                       ".* invalid conversion from .* to bool";
        }
};

TEST_F(ConfigParserEpockBoolConversionTest, Contiguous)
{
    setParam("contiguous");
    setSuccessString ("ON");
    setFaillureString("FOO");
    run();
}

TEST_F(ConfigParserEpockBoolConversionTest, Profile)
{
    setParam("profile");
    setSuccessString ("ON");
    setFaillureString("FOO");
    run();
}


/* --------------------------------------------------------------------------
 * Float conversion tests.
 * -------------------------------------------------------------------------- */

class ConfigParserEpockFloatConversionTest: public ConfigParserEpockDeathTest
{
    public:
        virtual void SetUp()
        {
            ConfigParserEpockDeathTest::SetUp();
            errormsg = "epock) ..:..:.. - FATAL ERROR: "
                       ".* invalid conversion from .* to float";
        }
};


TEST_F(ConfigParserEpockFloatConversionTest, Precision)
{
    setParam("precision");
    setSuccessString ("1.0");
    setFaillureString("FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, GridSpacing)
{
    setParam("grid_spacing");
    setSuccessString ("1.0");
    setFaillureString("FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, Padding)
{
    setParam("padding");
    setSuccessString ("1.0");
    setFaillureString("FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ContiguousCutoff)
{
    setParam("contiguous_cutoff");
    setSuccessString ("1.0");
    setFaillureString("FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, IncludeSphere)
{
    setParam("include_sphere");
    setSuccessString ("1.0 1.0 1.0 1.0");
    setFaillureString("1.0 1.0 1.0 FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ExcludeSphere)
{
    setParam("exclude_sphere");
    setSuccessString ("1.0 1.0 1.0 1.0");
    setFaillureString("1.0 1.0 1.0 FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, IncludeBox)
{
    setParam("include_box");
    setSuccessString ("1.0 1.0 1.0    1.0 1.0 1.0");
    setFaillureString("1.0 1.0 1.0    1.0 1.0 FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ExcludeBox)
{
    setParam("exclude_box");
    setSuccessString ("1.0 1.0 1.0    1.0 1.0 1.0");
    setFaillureString("1.0 1.0 1.0    1.0 1.0 FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, IncludeCylinder)
{
    setParam("include_cylinder");
    setSuccessString ("1.0 1.0 1.0    1.0 1.0 1.0   1.0");
    setFaillureString("1.0 1.0 1.0    1.0 1.0 1.0   FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ExcludeCylinder)
{
    setParam("exclude_cylinder");
    setSuccessString ("1.0 1.0 1.0    1.0 1.0 1.0   1.0");
    setFaillureString("1.0 1.0 1.0    1.0 1.0 1.0   FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ContiguousSeedSphere)
{
    setParam("contiguous_seed_sphere");
    setSuccessString ("1.0 1.0 1.0 1.0");
    setFaillureString("1.0 1.0 1.0 FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ContiguousSeedBox)
{
    setParam("contiguous_seed_box");
    setSuccessString ("1.0 1.0 1.0    1.0 1.0 1.0");
    setFaillureString("1.0 1.0 1.0    1.0 1.0 FOO");
    run();
}

TEST_F(ConfigParserEpockFloatConversionTest, ContiguousSeedCylinder)
{
    setParam("contiguous_seed_cylinder");
    setSuccessString ("1.0 1.0 1.0    1.0 1.0 1.0   1.0");
    setFaillureString("1.0 1.0 1.0    1.0 1.0 1.0   FOO");
    run();
}



/* --------------------------------------------------------------------------
 * Choice tests.
 * -------------------------------------------------------------------------- */

class ConfigParserAtomContributionTest: public ConfigParserEpockDeathTest
{

    public:
        virtual void SetUp()
        {
            ConfigParserEpockDeathTest::SetUp();
            param = "contribution";
            errormsg = "epock) ..:..:.. - FATAL ERROR: "
                       "Invalid value for parameter '.*': .*\n"
                       "Please choose between 'atom' and 'residue'\n";
        }

        void setParam(const std::string p) = delete;
};

TEST_F(ConfigParserAtomContributionTest, Choice)
{
    setSuccessString ("ATOM");
    setFaillureString("FOO");
    run();
    setSuccessString ("RESIDUE");
    setFaillureString("FOO");
    run();
}




// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
