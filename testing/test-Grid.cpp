

#include "epock-unittest.hpp"
#include "../src/Grid.hpp"
#include "../src/Point.hpp"
#include "../src/Vec3f.hpp"

#include <gtest/gtest.h>
#include <array>
#include <stdexcept>
#include <vector>


/* --------------------------------------------------------------------------
 * Tests.
 * -------------------------------------------------------------------------- */

class GridTest: public ::testing::Test
{
    public:
        // Grid is:
        //    - 5 x 5 x 5 = 125 points
        //    - centered at (0, 0, 0)
        GridTest(): grid({0.0, 4.0, 0.0, 4.0, 0.0, 4.0}, 1.0) {}

        virtual void SetUp()
        {
            std::vector<Point> & g = grid.getGrid();
            for (size_t i=0; i<5; ++i)
            {
                for (size_t j=0; j<5; ++j)
                {
                    for (size_t k=0; k<5; ++k)
                    {
                        g[i*5*5 + j*5 + k] = Point(float(i), float(j), float(k));
                    }
                }
            }
        }

    protected:
        Grid3D<Point> grid;
};


// -- Initialization  ---------------------------------------------------------
TEST(Initialization, FromDimensions)
{
    // Initialize a 10x10x10 grid.
    Grid3D<Point> g(10, 10, 10);

    EXPECT_EQ(1000, g.size());
    EXPECT_FLOAT_EQ(9999.9, g.getGrid()[0].x.x);
}

TEST_F(GridTest, InitializationFromBoundaries)
{
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.getOrigin()));
    EXPECT_TRUE(ArraysMatch({5, 5, 5}, grid.dim()));
    EXPECT_EQ(125, grid.size());

    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.getGrid()[0].x));
    EXPECT_TRUE(ArraysMatch({4.0, 4.0, 4.0}, grid.getGrid()[124].x));
}

// -- Access coordinates through index ----------------------------------------
TEST_F(GridTest, Coordinates)
{
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.coordinates(0, 0, 0)));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 1.0}, grid.coordinates(0, 0, 1)));
    EXPECT_TRUE(ArraysMatch({1.0, 0.0, 0.0}, grid.coordinates(1, 0, 0)));
    EXPECT_TRUE(ArraysMatch({4.0, 4.0, 4.0}, grid.coordinates(4, 4, 4)));
}


// -- Access Point through indices --------------------------------------------
TEST_F(GridTest, AccessThroughParenthesisOperator)
{
    std::vector<Point> & g = grid.getGrid();
    EXPECT_TRUE(ArraysMatch(g[0].x, grid(0, 0, 0).x));
    EXPECT_TRUE(ArraysMatch(g[1].x, grid(0, 0, 1).x));
    EXPECT_TRUE(ArraysMatch(g[25].x, grid(1, 0, 0).x));
    EXPECT_TRUE(ArraysMatch(g[124].x, grid(4, 4, 4).x));
}

TEST_F(GridTest, AccessThroughAt)
{
    std::vector<Point> & g = grid.getGrid();
    EXPECT_TRUE(ArraysMatch(g[0].x, grid.at(0, 0, 0).x));
    EXPECT_TRUE(ArraysMatch(g[1].x, grid.at(0, 0, 1).x));
    EXPECT_TRUE(ArraysMatch(g[25].x, grid.at(1, 0, 0).x));
    EXPECT_TRUE(ArraysMatch(g[124].x, grid.at(4, 4, 4).x));
    EXPECT_THROW(grid.at(5, 4, 4).x, std::out_of_range);
}


// -- Find nearest grid point -------------------------------------------------
TEST_F(GridTest, FindNearestPoint)
{
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPoint(0.0, 0.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPoint(0.1, 0.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPoint(0.45, 0.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({1.0, 0.0, 0.0}, grid.nearestPoint(0.5, 0.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPoint(-1.0, 0.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({4.0, 0.0, 0.0}, grid.nearestPoint(5.0, 0.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({4.0, 4.0, 0.0}, grid.nearestPoint(5.0, 5.0, 0.0).x));
    EXPECT_TRUE(ArraysMatch({4.0, 4.0, 4.0}, grid.nearestPoint(5.0, 5.0, 5.0).x));
}

TEST_F(GridTest, FindNearestPointIndices)
{
    EXPECT_TRUE(ArraysMatch({0, 0, 0}, grid.nearestPointIndices(0.0, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({0, 0, 0}, grid.nearestPointIndices(0.1, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({0, 0, 0}, grid.nearestPointIndices(0.45, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({1, 0, 0}, grid.nearestPointIndices(0.5, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({0, 0, 0}, grid.nearestPointIndices(-1.0, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({4, 0, 0}, grid.nearestPointIndices(5.0, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({4, 4, 0}, grid.nearestPointIndices(5.0, 5.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({4, 4, 4}, grid.nearestPointIndices(5.0, 5.0, 5.0)));
}

TEST_F(GridTest, FindNearestPointCoordinates)
{
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPointCoordinates(0.0, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPointCoordinates(0.1, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPointCoordinates(0.45, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({1.0, 0.0, 0.0}, grid.nearestPointCoordinates(0.5, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, grid.nearestPointCoordinates(-1.0, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({4.0, 0.0, 0.0}, grid.nearestPointCoordinates(5.0, 0.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({4.0, 4.0, 0.0}, grid.nearestPointCoordinates(5.0, 5.0, 0.0)));
    EXPECT_TRUE(ArraysMatch({4.0, 4.0, 4.0}, grid.nearestPointCoordinates(5.0, 5.0, 5.0)));
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

