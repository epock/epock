
#include "../src/ConfigParser.hpp"

#include <gtest/gtest.h>
#include <fstream>

const char * CFG_EMPTY = "";

const char * CFG_MISFORMATTED_SECTION_NAME = 
    "# This is the default section.\n"
    "\[DEFAULT\n" // Backslash before bracket required for proper CMake parsing
    "grid_spacing = 1.0\n";

const char * CFG_MISFORMATTED_PARAM_NAME = 
    "# This is the default section.\n"
    "[DEFAULT]\n"
    "grid_spacing 1.0\n";

const char * CFG_DEFAUT = 
    "# This is the default section.\n"
    "[DEFAULT]\n"
    "grid_spacing = 1.0\n"
    "contiguous = false\n"
    "contiguous_cutoff = 4.0\n";

const char * CFG_DEFAULT_2_SECTIONS =
    "# This is the default section.\n"
    "[DEFAULT]\n"
    "grid_spacing = 1.0\n"
    "contiguous = false\n"
    "contiguous_cutoff = 4.0\n"
    "\n"
    "# This is a comment\n"
    "[cav1]\n"
    "include_sphere  =     1.90    26.70   -21.91   8.0   \n"
    "exclude_sphere  =    10.26    25.14   -20.03   6.0   \n"
    "contiguous_seed_sphere =     1.90    26.70   -21.91   4.0\n"
    "\n"
    "[cav2]\n"
    "include_sphere  =    -0.14     5.56   -20.79   8.0\n"
    "exclude_sphere  =     3.69    12.71   -19.56   6.0\n"
    "contiguous_seed_sphere =    -0.14     5.56   -20.79   4.0";

/* --------------------------------------------------------------------------
 * ConfigSectionTest
 * -------------------------------------------------------------------------- */


class ConfigSectionTest: public ::testing::Test
{
    
    public:
        ConfigSectionTest(): cs(), csInit() {}

    protected:
        virtual void SetUp()
        {
            csInit.setName("theSection");
            csInit.addParameter("theParameter", "theValue");
        }

        ConfigSection cs, csInit;
};


// -- Initialization ----------------------------------------------------------
TEST_F(ConfigSectionTest, NonInitialized)
{
    EXPECT_EQ("", cs.getName());
    EXPECT_EQ(0, cs.getNumberOfParameters());
    EXPECT_TRUE(cs.getParameters().empty());
}

TEST_F(ConfigSectionTest, Initialized)
{
    EXPECT_EQ("theSection", csInit.getName());
    EXPECT_EQ(1, csInit.getNumberOfParameters());
    EXPECT_FALSE(csInit.getParameters().empty());
}


// -- GetParameter -------------------------------------------------------------------
TEST_F(ConfigSectionTest, getParameter)
{
    EXPECT_EQ("theValue", csInit.getParameter("theParameter"));
    EXPECT_THROW(csInit.getParameter("dumbParameter"), NoSuchParameterException);
    EXPECT_THROW(cs.getParameter("dumbParameter"), NoSuchParameterException);
}


// // -- Clear -------------------------------------------------------------------
TEST_F(ConfigSectionTest, Clear)
{
    cs.clear();
    EXPECT_EQ("", cs.getName());
    EXPECT_EQ(0, cs.getNumberOfParameters());
    EXPECT_TRUE(cs.getParameters().empty());

    csInit.clear();
    EXPECT_EQ("", csInit.getName());
    EXPECT_EQ(0, csInit.getNumberOfParameters());
    EXPECT_TRUE(csInit.getParameters().empty());
}



/* --------------------------------------------------------------------------
 * ConfigParserTest
 * -------------------------------------------------------------------------- */
class ConfigParserTest: public ::testing::Test
{
    public:
        ConfigParserTest(): emptyConfig(), config() {};

    protected:
        ConfigParser emptyConfig, config;
};


// -- Initialization ----------------------------------------------------------
TEST_F(ConfigParserTest, NonInitialized)
{
    EXPECT_EQ("", emptyConfig.getFilename());
    EXPECT_TRUE(emptyConfig.getSections().empty());
    EXPECT_EQ("", emptyConfig.getDefaultSection().getName());
    EXPECT_EQ(0, emptyConfig.getDefaultSection().getNumberOfParameters());
}


// -- Read --------------------------------------------------------------------
TEST_F(ConfigParserTest, ReadNonInitialized)
{
    // Read dummy config file (this should fail).
    EXPECT_DEATH(emptyConfig.read(), "epock) ..:..:.. - FATAL ERROR: Can't open file: ''");
}

TEST_F(ConfigParserTest, ReadEmpty)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_EMPTY << std::endl;
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(0, config.getSections().size());
    ASSERT_EQ(0, config.getDefaultSection().getNumberOfParameters());
}

TEST_F(ConfigParserTest, ReadMisformattedSectionName)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_MISFORMATTED_SECTION_NAME << std::endl;
    cfgout.close();

    // Read dummy config file (this should fail).
    config.setFilename(fname);
    EXPECT_DEATH(config.read(), "epock) ..:..:.. - FATAL ERROR: dummy.cfg: misformatted section name '\\\[DEFAULT'");
}

TEST_F(ConfigParserTest, ReadMisformattedParameter)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_MISFORMATTED_PARAM_NAME << std::endl;
    cfgout.close();

    // Read dummy config file (this should fail).
    config.setFilename(fname);
    EXPECT_DEATH(config.read(), "epock) ..:..:.. - FATAL ERROR: dummy.cfg: "
                                "misformatted parameter 'grid_spacing 1.0' "
                                "\\(format is param = value\\)");
}

TEST_F(ConfigParserTest, ReadDefaultOnly)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_DEFAUT << std::endl;
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(0, config.getSections().size());
    ASSERT_EQ(3, config.getDefaultSection().getNumberOfParameters());
    EXPECT_EQ("1.0", config.getDefaultSection().getParameter("grid_spacing"));
    EXPECT_EQ("4.0", config.getDefaultSection().getParameter("contiguous_cutoff"));
    EXPECT_EQ("false", config.getDefaultSection().getParameter("contiguous"));
}

TEST_F(ConfigParserTest, ReadSectionNames)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_DEFAULT_2_SECTIONS << std::endl;
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(2, config.getSections().size());
    EXPECT_NO_THROW(config.getSection("cav1"));
    EXPECT_NO_THROW(config.getSection("cav2"));
    EXPECT_NO_THROW(config.getSection("DEFAULT"));
    EXPECT_THROW(config.getSection("[cav1]"), NoSuchSectionException);

    EXPECT_NO_THROW(config.getSection(0));
    EXPECT_NO_THROW(config.getSection(1));
    EXPECT_THROW(config.getSection(2), const char *);
}

TEST_F(ConfigParserTest, ReadDefault2Sections)
{
    const char * fname = "dummy.cfg";

    // Write dummy config file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout << CFG_DEFAULT_2_SECTIONS << std::endl;
    cfgout.close();

    // Read dummy config file (this should not fail).
    config.setFilename(fname);
    config.read();

    // Check file reading and parsing is ok.
    ASSERT_EQ(2, config.getSections().size());
    ASSERT_EQ(3, config.getDefaultSection().getNumberOfParameters());

    // Check default section is ok.
    ConfigSection defaultsec = config.getDefaultSection();
    ASSERT_EQ(3, defaultsec.getNumberOfParameters());
    EXPECT_EQ("1.0", defaultsec.getParameter("grid_spacing"));
    EXPECT_EQ("4.0", defaultsec.getParameter("contiguous_cutoff"));
    EXPECT_EQ("false", defaultsec.getParameter("contiguous"));

    // Sections should contain parameters defined in default section + their
    // own parameters.
    ConfigSection sec1 = config.getSection("cav1");
    ASSERT_EQ(6, sec1.getNumberOfParameters());
    ASSERT_EQ("cav1", sec1.getName());
    EXPECT_EQ("1.0", sec1.getParameter("grid_spacing"));
    EXPECT_EQ("4.0", sec1.getParameter("contiguous_cutoff"));
    EXPECT_EQ("false", sec1.getParameter("contiguous"));
    EXPECT_EQ("1.90    26.70   -21.91   8.0", sec1.getParameter("include_sphere"));
    EXPECT_EQ("10.26    25.14   -20.03   6.0", sec1.getParameter("exclude_sphere"));
    EXPECT_EQ("1.90    26.70   -21.91   4.0", sec1.getParameter("contiguous_seed_sphere"));

    ConfigSection sec2 = config.getSection("cav2");
    ASSERT_EQ(6, sec2.getNumberOfParameters());
    ASSERT_EQ("cav2", sec2.getName());
    EXPECT_EQ("1.0", sec2.getParameter("grid_spacing"));
    EXPECT_EQ("4.0", sec2.getParameter("contiguous_cutoff"));
    EXPECT_EQ("false", sec2.getParameter("contiguous"));
    EXPECT_EQ("-0.14     5.56   -20.79   8.0", sec2.getParameter("include_sphere"));
    EXPECT_EQ("3.69    12.71   -19.56   6.0", sec2.getParameter("exclude_sphere"));
    EXPECT_EQ("-0.14     5.56   -20.79   4.0", sec2.getParameter("contiguous_seed_sphere"));
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
