
#include "../src/utils.hpp"

#include <gtest/gtest.h>
#include <cstdio>
#include <fstream>
#include <string>
#include <vector>

// -- from_string method ------------------------------------------------------

TEST(from_string, EmptyString)
{
    int n = 0;
    EXPECT_FALSE(from_string(n, ""));
    EXPECT_EQ(0, n);

    n = 1;
    EXPECT_FALSE(from_string(n, ""));
    EXPECT_EQ(1, n);
}

TEST(from_string, StringToInteger)
{
    int n;
    EXPECT_TRUE(from_string(n, "2"));
    EXPECT_EQ(2, n);

    EXPECT_TRUE(from_string(n, "+2"));
    EXPECT_EQ(2, n);

    EXPECT_TRUE(from_string(n, "-2"));
    EXPECT_EQ(-2, n);
}

TEST(from_string, StringToFloat)
{
    float n;
    EXPECT_TRUE(from_string(n, "2.6"));
    EXPECT_FLOAT_EQ(n, 2.6);

    EXPECT_TRUE(from_string(n, "+2.6"));
    EXPECT_FLOAT_EQ(n, 2.6);

    EXPECT_TRUE(from_string(n, "-2.6"));
    EXPECT_FLOAT_EQ(n, -2.6);
}


// -- Left Trim method --------------------------------------------------------
TEST(ltrim, NoWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::ltrim("Hello World!"));
}

TEST(ltrim, LeftWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::ltrim("  Hello World!"));
}

TEST(ltrim, RightWhiteSpace)
{
    EXPECT_EQ("Hello World!  ", String::ltrim("Hello World!  "));
}

TEST(ltrim, LeftRightWhiteSpace)
{
    EXPECT_EQ("Hello World!  ", String::ltrim("  Hello World!  "));
}


// -- Right Trim method  ------------------------------------------------------
TEST(rtrim, NoWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::rtrim("Hello World!"));
}

TEST(rtrim, LeftWhiteSpace)
{
    EXPECT_EQ("  Hello World!", String::rtrim("  Hello World!"));
}

TEST(rtrim, RightWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::rtrim("Hello World!  "));
}

TEST(rtrim, LeftRightWhiteSpace)
{
    EXPECT_EQ("  Hello World!", String::rtrim("  Hello World!  "));
}


// -- Left & Right Trim method  -----------------------------------------------
TEST(trim, NoWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::trim("Hello World!"));
}

TEST(trim, LeftWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::trim("  Hello World!"));
}

TEST(trim, RightWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::trim("Hello World!  "));
}

TEST(trim, LeftRightWhiteSpace)
{
    EXPECT_EQ("Hello World!", String::trim("  Hello World!  "));
}


// -- First alpha method ------------------------------------------------------
TEST(first_alpha, FirstIsAlpha)
{
    EXPECT_EQ("H", String::first_alpha("Hello World!"));
    EXPECT_EQ("H", String::first_alpha("H12llo World!"));
}

TEST(first_alpha, FirstIsNotAlpha)
{
    EXPECT_EQ("e", String::first_alpha("1ello World!"));
    EXPECT_EQ("l", String::first_alpha("12llo World!"));
}

TEST(first_alpha, NoAlpha)
{
    EXPECT_EQ("", String::first_alpha("123"));
}


// -- String format method ----------------------------------------------------
TEST(format, NoFormatting)
{
    EXPECT_EQ(" Hello World!  ", String::format(" Hello World!  "));
}

TEST(format, FormattingIntFloat)
{
    EXPECT_EQ("Hello World: 042    42.17!", 
              String::format("Hello World: %03d %8.2f!", 42, 42.17));
}


// -- String split method -----------------------------------------------------
TEST(split, SimpleSplit)
{
    std::vector<std::string> ref = {"Hello", "World!"};
    EXPECT_EQ(ref, String::split("Hello World!"));

    ref = {"Hello", "World!", "Foo", "Bar"};
    EXPECT_EQ(ref, String::split("Hello World! Foo Bar"));
}


// -- Open file for reading ---------------------------------------------------
TEST(File, Openread)
{
    const char * fname = "dummy";

    // Write dummy file.
    std::ofstream cfgout(fname);
    ASSERT_TRUE(cfgout.is_open());
    cfgout.close();

    // Try opening file (should not fail).
    std::ifstream infile;
    utils::openread(fname, infile);
    infile.close();
    std::remove("dummy");

    // Try opening wrong file (should fail).
    EXPECT_DEATH(utils::openread("foo", infile),
                 "epock) ..:..:.. - FATAL ERROR: Can't open file:");
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

