
#include "epock-unittest.hpp"
#include "../src/ArgumentParser.hpp"
#include "version.hpp"

#include <vector>
#include <string>


class ArgumentParserTest: public ::testing::Test
{
    public:
        ArgumentParserTest(): parser() {}

    protected:
        ArgumentParser parser;
};

TEST_F(ArgumentParserTest, Initialization)
{
}

TEST_F(ArgumentParserTest, ShowHelp)
{
    parser.show_help();
}

TEST_F(ArgumentParserTest, UnknownShortOption)
{
    std::vector<std::string> argv = {"epock", "-dum"};
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "Unrecognized option ");
}

TEST_F(ArgumentParserTest, UnknownArgument)
{
    std::vector<std::string> argv = {"epock", "dum"};
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "Unrecognized argument ");
}

TEST_F(ArgumentParserTest, Help)
{
    std::vector<std::string> argv = {"epock", "-h"};
    EXPECT_EXIT(parser.parseArgs(argv), ::testing::ExitedWithCode(0),
                PROJECT_EXECUTABLE + " v" + PROJECT_VERSION);

    std::vector<std::string> argv2 = {"epock", "--help"};
    EXPECT_EXIT(parser.parseArgs(argv2), ::testing::ExitedWithCode(0),
                PROJECT_EXECUTABLE + " v" + PROJECT_VERSION);

    std::vector<std::string> argv3 = {"epock", "--helpmeplease"};
    EXPECT_DEATH(parser.parseArgs(argv3), "epock) ..:..:.. - FATAL ERROR: "
                                          "Unrecognized option ");
}

TEST_F(ArgumentParserTest, Version)
{
    std::vector<std::string> argv = {"epock", "--version"};
    EXPECT_EXIT(parser.parseArgs(argv), ::testing::ExitedWithCode(0), PROJECT_VERSION);
}

TEST_F(ArgumentParserTest, MissingValue1)
{
    std::vector<std::string> argv = {"epock", "-s"};
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-s' but got nothing.*$");

    argv[1] = "-f";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-f' but got nothing.*$");

    argv[1] = "-c";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-c' but got nothing.*$");

    argv[1] = "-o";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-o' but got nothing.*$");

    argv[1] = "--radii";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '--radii' but got nothing.*$");

    argv[1] = "-b";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-b' but got nothing.*$");
    
    argv[1] = "-e";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-e' but got nothing.*$");
    
    argv[1] = "--dt";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '--dt' but got nothing.*$");
}

TEST_F(ArgumentParserTest, MissingValue2)
{
    std::vector<std::string> argv = {"epock", "-s", "-foo"};
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "expected value after option '-s' but got '-foo'.*$");
}

TEST_F(ArgumentParserTest, GotMandatoryArguments)
{
    std::vector<std::string> argv = {"epock", "-s", "foo"};
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "-s and -c options are mandatory \\(so not optionnal...\\).*$");

    argv[1] = "-c";
    EXPECT_DEATH(parser.parseArgs(argv), "epock) ..:..:.. - FATAL ERROR: "
                                         "-s and -c options are mandatory \\(so not optionnal...\\).*$");
}


TEST_F(ArgumentParserTest, GetValueString)
{
    std::vector<std::string> argv = {"epock", "-s", "foo", "-c", "bar"};
    parser.parseArgs(argv);
    EXPECT_EQ("foo", parser.topname);
    EXPECT_EQ("bar", parser.cfgname);
}

TEST_F(ArgumentParserTest, GetValueFloat)
{
    std::vector<std::string> argv = {"epock", "-s", "foo", "-c", "bar", "-b", "10"};
    parser.parseArgs(argv);
    EXPECT_FLOAT_EQ(10, parser.start);
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}