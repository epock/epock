
#include "epock-unittest.hpp"
#include "../src/Region.hpp"
#include "../src/Point.hpp"
#include <gtest/gtest.h>


/* --------------------------------------------------------------------------
 * Sphere regions tests.
 * -------------------------------------------------------------------------- */

class SphereRegionTest: public ::testing::Test
{
    public:
        SphereRegionTest(): r() {}

        virtual void SetUp()
        {
            ASSERT_FLOAT_EQ(0, r.getRadius());
            ASSERT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, r.getCenter()));

            // Sphere region of radius 1.0 is centered at (0, 0, 0).
            r.setRadius(1.0);
            ASSERT_FLOAT_EQ(1, r.getRadius());
        }

    protected:
        SphereRegion r;
};

TEST_F(SphereRegionTest, Initialization)
{
    // Empty test to make sure initialization process is ok.
}

TEST_F(SphereRegionTest, Boundaries)
{
    std::array<float, 6> bounds;
    r.boundaries(bounds);
    EXPECT_TRUE(ArraysMatch({-1.0, 1.0, -1.0, 1.0, -1.0, 1.0}, bounds));
}

TEST_F(SphereRegionTest, Contains)
{
    EXPECT_TRUE(r.contains(Point(0.0, 0.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(1.0, 0.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(0.0, 1.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(0.0, 0.0, 1.0)));
    EXPECT_TRUE(r.contains(Point(0.9, 0.3, 0.0)));

    EXPECT_FALSE(r.contains(Point(1.0, 0.1, 0.0)));
    EXPECT_FALSE(r.contains(Point(1.0, 1.0, 0.0)));
}

TEST_F(SphereRegionTest, Intersects)
{
    // Test if spheres of different radii intersect the region
    // (should intersect as they share the same center as the region).
    Point p(0.0, 0.0, 0.0);
    EXPECT_TRUE(r.intersects(p, 0.0));
    EXPECT_TRUE(r.intersects(p, 0.5));
    EXPECT_TRUE(r.intersects(p, 1.0));
    EXPECT_TRUE(r.intersects(p, 1.1));
    EXPECT_TRUE(r.intersects(p, -10));

    // Test if spheres with different positions intersect the region.
    float radius = 1.0;
    EXPECT_TRUE(r.intersects(Point(0.0, 0.0, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(1.0, 1.0, 1.0), radius));
    EXPECT_TRUE(r.intersects(Point(2.0, 0.0, 0.0), radius));

    EXPECT_FALSE(r.intersects(Point(2.0, 2.0, 2.0), radius));
    EXPECT_FALSE(r.intersects(Point(2.1, 0.0, 0.0), radius));
}


/* --------------------------------------------------------------------------
 * Box regions tests.
 * -------------------------------------------------------------------------- */

class BoxRegionTest: public ::testing::Test
{
    public:
        BoxRegionTest(): r() {}

        virtual void SetUp()
        {
            ASSERT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, r.getDim()));
            ASSERT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, r.getCenter()));

            // Box region of side length 1.0 is centered at (0, 0, 0).
            r.setDim({1.0, 1.0, 1.0});
            ASSERT_TRUE(ArraysMatch({1.0, 1.0, 1.0}, r.getDim()));
        }

    protected:
        BoxRegion r;
};

TEST_F(BoxRegionTest, Initialization)
{
    // Empty test to make sure initialization process is ok.
}

TEST_F(BoxRegionTest, Boundaries)
{
    std::array<float, 6> bounds;
    r.boundaries(bounds);
    EXPECT_TRUE(ArraysMatch({-0.5, 0.5, -0.5, 0.5, -0.5, 0.5}, bounds));
}

TEST_F(BoxRegionTest, Contains)
{
    EXPECT_TRUE(r.contains(Point(0.0, 0.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(0.5, 0.5, 0.5)));
    EXPECT_FALSE(r.contains(Point(0.51, 0.0, 0.0)));
}

TEST_F(BoxRegionTest, Intersects)
{
    // Test if spheres of different radii intersect the region
    // (should intersect as they share the same center as the region).
    Point p(0.0, 0.0, 0.0);
    EXPECT_TRUE(r.intersects(p, 0.0));
    EXPECT_TRUE(r.intersects(p, 0.5));
    EXPECT_TRUE(r.intersects(p, 1.0));
    EXPECT_TRUE(r.intersects(p, 1.1));
    EXPECT_TRUE(r.intersects(p, -10));

    // Test if spheres with different positions intersect the region.
    float radius = 1.0;
    EXPECT_TRUE(r.intersects(Point(0.0, 0.0, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(-0.5, 0.0, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(0.5, 0.0, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(-0.5, -0.5, -0.5), radius));
    EXPECT_TRUE(r.intersects(Point(1.0, 1.0, 1.0), radius));
    EXPECT_TRUE(r.intersects(Point(1.5, 1.5, 1.5), radius));
    EXPECT_FALSE(r.intersects(Point(1.6, 1.5, 1.5), radius));
}


/* --------------------------------------------------------------------------
 * Cylinder regions tests.
 * -------------------------------------------------------------------------- */

class CylinderRegionTest: public ::testing::Test
{
    public:
        CylinderRegionTest(): r() {}

        virtual void SetUp()
        {
            ASSERT_FLOAT_EQ(0, r.getRadius());
            ASSERT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, r.getX1().x));
            ASSERT_TRUE(ArraysMatch({0.0, 0.0, 0.0}, r.getX2().x));

            // Cylinder region of length 2 centered at (0, 0, 0).
            r.setX1(Point(0.0, 0.0, 0.0));
            r.setX2(Point(2.0, 0.0, 0.0));
            r.setRadius(1.0);
        }

    protected:
        CylinderRegion r;
};

TEST_F(CylinderRegionTest, Initialization)
{
    // Empty test to make sure initialization process is ok.
}

TEST_F(CylinderRegionTest, Boundaries)
{
    std::array<float, 6> bounds;
    r.boundaries(bounds);
    EXPECT_TRUE(ArraysMatch({-1.0, 3.0, -1.0, 1.0, -1.0, 1.0}, bounds));
}

TEST_F(CylinderRegionTest, Contains)
{
    EXPECT_TRUE(r.contains(Point(0.0, 0.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(1.0, 0.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(0.0, 1.0, 0.0)));
    EXPECT_TRUE(r.contains(Point(0.0, 0.0, 1.0)));
    EXPECT_FALSE(r.contains(Point(0.0, 1.01, 0.0)));
}

TEST_F(CylinderRegionTest, Intersects)
{
    // Test if spheres of different radii intersect the region
    // (should intersect).
    Point p(0.0, 0.0, 0.0);
    EXPECT_TRUE(r.intersects(p, 0.0));
    EXPECT_TRUE(r.intersects(p, 0.5));
    EXPECT_TRUE(r.intersects(p, 1.0));
    EXPECT_TRUE(r.intersects(p, 1.1));
    EXPECT_TRUE(r.intersects(p, -10));

    // Test if spheres with different positions intersect the region.
    float radius = 1.0;
    EXPECT_TRUE(r.intersects(Point(0.0, 0.0, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(0.0, 1.01, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(0.0, 1.90, 0.0), radius));
    EXPECT_TRUE(r.intersects(Point(0.0, 2.0, 0.0), radius));
    EXPECT_FALSE(r.intersects(Point(0.0, 2.1, 0.0), radius));
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
