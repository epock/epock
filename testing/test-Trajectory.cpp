

#include "epock-unittest.hpp"
#include "test-Trajectory.hpp"
#include "../src/Trajectory.hpp"
#include "../src/utils.hpp"

#include <cstdio>
#include <fstream>


class TrajectoryTestBase: public ::testing::Test
{
    public:
        virtual ~TrajectoryTestBase() { if (trj) delete trj; }

        virtual void SetUp()
        {
            trj->setFilename(filename);
        }

        virtual void TearDown()
        {
            remove(filename.c_str());
        }

    protected:
        void WriteTrajectory()
        {
            ofs.open(filename);
            ASSERT_TRUE(ofs.is_open());
            ofs << content;
        }

        Trajectory * trj;
        std::string filename;
        std::ofstream ofs;
        std::string content;
};

class TrajectoryPDBTest: public TrajectoryTestBase
{
    public:
        TrajectoryPDBTest()
        {
            filename = "trj.pdb";
            trj = new TrajectoryPDB();
        }
};

class TrajectoryGROTest: public TrajectoryTestBase
{
    public:
        TrajectoryGROTest()
        {
            filename = "trj.gro";
            trj = new TrajectoryGRO();
        }
};

class TrajectoryTRRTest: public TrajectoryTestBase
{
    public:
        TrajectoryTRRTest()
        {
            filename = "trj.trr";
            trj = new TrajectoryTRR();
        }
};

class TrajectoryXTCTest: public TrajectoryTestBase
{
    public:
        TrajectoryXTCTest()
        {
            filename = "trj.xtc";
            trj = new TrajectoryXTC();
        }
};

class TrajectoryXTCReadTest: public TrajectoryXTCTest
{
    public:
        TrajectoryXTCReadTest() { filename = "trj-nodelete.xtc"; }
    
        // Overwrite the TrajectoryTestBase::TearDown method
        // so that it does not remove the trajectory at the end
        // of the test.
        virtual void TearDown() {}
};

class TrajectoryTRRReadTest: public TrajectoryTRRTest
{
    public:
        TrajectoryTRRReadTest() { filename = "trj-nodelete.trr"; }
        virtual void TearDown() {}
};


//////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark HasNextFrame Tests

///////////////////////////////////////////////////////////////////////////////

TEST_F(TrajectoryPDBTest, readTrajectory)
{
    content = PDBTRJSTRING;
    WriteTrajectory();
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    size_t framei = 0;
    while (trj->hasNextFrame())
    {
        bool success = trj->nextFrame();
        if(success)
        {
            for (size_t i=0; i<trj->getNumberOfAtoms(); ++i)
            {
                ASSERT_FLOAT_EQ(framei, trj->getXAt(i));
            }
        }
        framei++;
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}

TEST_F(TrajectoryGROTest, readTrajectory)
{
    content = GROTRJSTRING;
    WriteTrajectory();
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    size_t framei = 0;
    while (trj->hasNextFrame())
    {
        bool success = trj->nextFrame();
        if(success)
        {
            for (size_t i=0; i<trj->getNumberOfAtoms(); ++i)
            {
                ASSERT_FLOAT_EQ(framei, trj->getXAt(i));
            }
        }
        framei++;
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}

TEST_F(TrajectoryXTCReadTest, readTrajectory)
{
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    size_t framei = 0;
    while (trj->hasNextFrame())
    {
        bool success = trj->nextFrame();
        if(success)
        {
            for (size_t i=0; i<trj->getNumberOfAtoms(); ++i)
            {
                ASSERT_FLOAT_EQ(framei, trj->getXAt(i));
            }
        }
        framei++;
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}

TEST_F(TrajectoryTRRReadTest, readTrajectory)
{
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    size_t framei = 0;
    while (trj->hasNextFrame())
    {
        bool success = trj->nextFrame();
        if(success)
        {
            for (size_t i=0; i<trj->getNumberOfAtoms(); ++i)
            {
                ASSERT_FLOAT_EQ(framei, trj->getXAt(i));
            }
        }
        framei++;
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}


//////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark HasNextFrame Tests

///////////////////////////////////////////////////////////////////////////////

TEST_F(TrajectoryPDBTest, hasNextFrame)
{
    content = PDBTRJSTRING;
    WriteTrajectory();
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    while (trj->hasNextFrame())
    {
        trj->nextFrame();
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}

TEST_F(TrajectoryGROTest, hasNextFrame)
{
    content = GROTRJSTRING;
    WriteTrajectory();
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    while (trj->hasNextFrame())
    {
        trj->nextFrame();
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}

TEST_F(TrajectoryXTCReadTest, hasNextFrame)
{
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    while (trj->hasNextFrame())
    {
        trj->nextFrame();
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}

TEST_F(TrajectoryTRRReadTest, hasNextFrame)
{
    ASSERT_NO_THROW(trj->update());
    ASSERT_EQ(10, trj->getNumberOfAtoms());

    while (trj->hasNextFrame())
    {
        trj->nextFrame();
    }
    EXPECT_EQ(10, trj->getNumberOfFrames());
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Read Number of Atoms Tests

///////////////////////////////////////////////////////////////////////////////

TEST_F(TrajectoryPDBTest, getNumberOfAtoms)
{
    content = PDBTRJSTRING;
    WriteTrajectory();
    ASSERT_NO_THROW(trj->update());
    EXPECT_EQ(10, trj->getNumberOfAtoms());
}

TEST_F(TrajectoryGROTest, getNumberOfAtoms)
{
    content = GROTRJSTRING;
    WriteTrajectory();
    ASSERT_NO_THROW(trj->update());
    EXPECT_EQ(10, trj->getNumberOfAtoms());
}

TEST_F(TrajectoryXTCReadTest, getNumberOfAtoms)
{
    ASSERT_NO_THROW(trj->update());
    EXPECT_EQ(10, trj->getNumberOfAtoms());
}

TEST_F(TrajectoryTRRReadTest, getNumberOfAtoms)
{
    ASSERT_NO_THROW(trj->update());
    EXPECT_EQ(10, trj->getNumberOfAtoms());
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark File Not Found Tests

///////////////////////////////////////////////////////////////////////////////

TEST_F(TrajectoryPDBTest, NotFound)
{
    EXPECT_THROW(trj->update(), FileNotFoundException);
}

TEST_F(TrajectoryGROTest, NotFound)
{
    EXPECT_THROW(trj->update(), FileNotFoundException);
}

TEST_F(TrajectoryTRRTest, NotFound)
{
    EXPECT_THROW(trj->update(), FileNotFoundException);
}

TEST_F(TrajectoryXTCTest, NotFound)
{
    EXPECT_THROW(trj->update(), FileNotFoundException);
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Read Empty Tests

///////////////////////////////////////////////////////////////////////////////

TEST_F(TrajectoryPDBTest, readEmpty)
{
    content = "";
    WriteTrajectory();
    EXPECT_THROW(trj->update(), EndOfFileException);
}

TEST_F(TrajectoryGROTest, readEmpty)
{
    content = "";
    WriteTrajectory();
    EXPECT_THROW(trj->update(), EndOfFileException);
}

TEST_F(TrajectoryTRRTest, readEmpty)
{
    content = "";
    WriteTrajectory();
    EXPECT_THROW(trj->update(), EndOfFileException);
}


TEST_F(TrajectoryXTCTest, readEmpty)
{
    content = "";
    WriteTrajectory();
    EXPECT_THROW(trj->update(), EndOfFileException);
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Main Function

///////////////////////////////////////////////////////////////////////////////

// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

