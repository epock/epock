
#include "../src/Vec3f.hpp"

#include <gtest/gtest.h>


class Vec3fTest: public ::testing::Test
{
    
    public:
        Vec3fTest(): 
            zeroVec(0.0, 0.0, 0.0),
            oneVec(1.0, 1.0, 1.0),
            posVec(1.0, 2.0, 3.0),
            negVec(-1.0, -2.0, -3.0)
        {}

        Vec3f zeroVec, oneVec, posVec, negVec;
};


// -- Access -----------------------------------------------------------------
TEST_F(Vec3fTest, AccessOperators)
{
    EXPECT_EQ(1, posVec.x); 
    EXPECT_EQ(2, posVec.y); 
    EXPECT_EQ(3, posVec.z);

    EXPECT_EQ(1, posVec[0]); 
    EXPECT_EQ(2, posVec[1]); 
    EXPECT_EQ(3, posVec[2]);

    Vec3f dumVec(4, -5, 6);
    dumVec[0] = 1;
    dumVec[1] = 2;
    dumVec[2] = 3;
    EXPECT_EQ(1, dumVec[0]); 
    EXPECT_EQ(2, dumVec[1]); 
    EXPECT_EQ(3, dumVec[2]);
}


// -- Comparisons ------------------------------------------------------------
TEST_F(Vec3fTest, Equality)
{
    EXPECT_EQ(Vec3f(0.0, 0.0, 0.0), zeroVec);
    EXPECT_EQ(zeroVec, zeroVec);
    EXPECT_NE(zeroVec, oneVec);
    EXPECT_NE(posVec, negVec);
}

TEST_F(Vec3fTest, Inferiority)
{
    EXPECT_TRUE(zeroVec < Vec3f(1.0, 0.0, 0.0));
    EXPECT_TRUE(zeroVec < Vec3f(0.0, 1.0, 0.0));
    EXPECT_TRUE(zeroVec < Vec3f(0.0, 0.0, 1.0));
    EXPECT_TRUE(zeroVec < oneVec);
    EXPECT_TRUE(zeroVec < posVec);
    EXPECT_TRUE(negVec < zeroVec);
    EXPECT_FALSE(zeroVec < zeroVec);
    EXPECT_FALSE(posVec < negVec);
}


// -- Linear algebra ---------------------------------------------------------
TEST_F(Vec3fTest, Addition)
{
    EXPECT_EQ(zeroVec, zeroVec + zeroVec);
    EXPECT_EQ(oneVec, zeroVec + oneVec);
    EXPECT_EQ(Vec3f(2.0, 4.0, 6.0), posVec + posVec);
    EXPECT_EQ(zeroVec, posVec + negVec);
}

TEST_F(Vec3fTest, Substraction)
{
    EXPECT_EQ(zeroVec, zeroVec - zeroVec);
    EXPECT_EQ(Vec3f(-1.0, -1.0, -1.0), zeroVec - oneVec);
    EXPECT_EQ(zeroVec, posVec - posVec);
    EXPECT_EQ(Vec3f(2.0, 4.0, 6.0), posVec - negVec);
}

TEST_F(Vec3fTest, DotProduct)
{
    Vec3f dumVec(4, -5, 6);
    EXPECT_EQ(0, zeroVec.dot(zeroVec));
    EXPECT_EQ(0, zeroVec.dot(oneVec));
    EXPECT_EQ(0, zeroVec.dot(posVec));
    EXPECT_EQ(0, zeroVec.dot(negVec));
    EXPECT_EQ(12, posVec.dot(dumVec));
    EXPECT_EQ(posVec.dot(dumVec), dumVec.dot(posVec));
    EXPECT_EQ(posVec.dot(negVec + dumVec), posVec.dot(negVec) + posVec.dot(dumVec));
}

TEST_F(Vec3fTest, CrossProduct)
{
    Vec3f dumVec(4, -5, 6);
    EXPECT_EQ(zeroVec, posVec.cross(posVec));
    EXPECT_EQ(zeroVec, posVec.cross(negVec));
    EXPECT_EQ(Vec3f(27, 6, -13), posVec.cross(dumVec));
}

TEST_F(Vec3fTest, Length)
{
    EXPECT_EQ(0, zeroVec.length());
    EXPECT_FLOAT_EQ(1.732050807, oneVec.length());
    EXPECT_FLOAT_EQ(3.741657386, posVec.length());
    EXPECT_FLOAT_EQ(3.741657386, negVec.length());
    EXPECT_FLOAT_EQ(posVec.length(), negVec.length());
}

TEST_F(Vec3fTest, Norm)
{
    EXPECT_EQ(zeroVec, posVec.norm() + negVec.norm());
}

TEST_F(Vec3fTest, DistSquared)
{
    EXPECT_FLOAT_EQ(0.0, zeroVec.dist2(zeroVec));
    EXPECT_FLOAT_EQ(oneVec.length()*oneVec.length(), zeroVec.dist2(oneVec));
    EXPECT_FLOAT_EQ(zeroVec.dist2(posVec), zeroVec.dist2(negVec));
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

