
#include "epock-unittest.hpp"
#include "../src/Cavity.hpp"


#include <vector>
#include "Atom.hpp"
#include "Region.hpp"

class CavityTest: public ::testing::Test
{
    public:
        CavityTest(): name("cavity") {}

        virtual void SetUp()
        {
            // atoms contains 1 atom located at (0,0,0).
            Atom a;
            a.vdw = 1.0;
            atoms.push_back(a);
            cav.setMolecule(&atoms);

            SphereRegion * sphere = new SphereRegion({0.0f, 0.0f, 0.0f}, 10);
            cav.addIncludeRegion(sphere);
        }

    protected:
        Cavity cav;
        std::string name;
        std::vector<Atom> atoms;
};


TEST_F(CavityTest, foo)
{
    EXPECT_EQ(1, 1);
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Main Function

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

