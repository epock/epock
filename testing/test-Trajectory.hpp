
#ifndef __TEST_TRAJECTORY_HPP__
#define __TEST_TRAJECTORY_HPP__

#include <string>

static const std::string PDBTRJSTRING = "MODEL       0\n"
"ATOM      1  N   LYS A   1       0.000   0.000   0.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       0.000   0.000   0.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       0.000   0.000   0.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       0.000   0.000   0.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       0.000   0.000   0.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       0.000   0.000   0.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       0.000   0.000   0.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       0.000   0.000   0.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       0.000   0.000   0.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       0.000   0.000   0.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       1\n"
"ATOM      1  N   LYS A   1       1.000   1.000   1.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       1.000   1.000   1.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       1.000   1.000   1.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       1.000   1.000   1.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       1.000   1.000   1.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       1.000   1.000   1.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       1.000   1.000   1.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       1.000   1.000   1.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       1.000   1.000   1.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       1.000   1.000   1.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       2\n"
"ATOM      1  N   LYS A   1       2.000   2.000   2.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       2.000   2.000   2.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       2.000   2.000   2.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       2.000   2.000   2.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       2.000   2.000   2.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       2.000   2.000   2.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       2.000   2.000   2.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       2.000   2.000   2.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       2.000   2.000   2.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       2.000   2.000   2.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       3\n"
"ATOM      1  N   LYS A   1       3.000   3.000   3.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       3.000   3.000   3.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       3.000   3.000   3.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       3.000   3.000   3.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       3.000   3.000   3.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       3.000   3.000   3.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       3.000   3.000   3.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       3.000   3.000   3.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       3.000   3.000   3.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       3.000   3.000   3.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       4\n"
"ATOM      1  N   LYS A   1       4.000   4.000   4.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       4.000   4.000   4.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       4.000   4.000   4.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       4.000   4.000   4.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       4.000   4.000   4.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       4.000   4.000   4.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       4.000   4.000   4.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       4.000   4.000   4.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       4.000   4.000   4.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       4.000   4.000   4.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       5\n"
"ATOM      1  N   LYS A   1       5.000   5.000   5.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       5.000   5.000   5.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       5.000   5.000   5.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       5.000   5.000   5.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       5.000   5.000   5.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       5.000   5.000   5.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       5.000   5.000   5.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       5.000   5.000   5.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       5.000   5.000   5.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       5.000   5.000   5.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       6\n"
"ATOM      1  N   LYS A   1       6.000   6.000   6.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       6.000   6.000   6.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       6.000   6.000   6.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       6.000   6.000   6.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       6.000   6.000   6.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       6.000   6.000   6.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       6.000   6.000   6.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       6.000   6.000   6.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       6.000   6.000   6.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       6.000   6.000   6.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       7\n"
"ATOM      1  N   LYS A   1       7.000   7.000   7.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       7.000   7.000   7.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       7.000   7.000   7.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       7.000   7.000   7.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       7.000   7.000   7.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       7.000   7.000   7.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       7.000   7.000   7.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       7.000   7.000   7.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       7.000   7.000   7.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       7.000   7.000   7.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       8\n"
"ATOM      1  N   LYS A   1       8.000   8.000   8.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       8.000   8.000   8.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       8.000   8.000   8.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       8.000   8.000   8.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       8.000   8.000   8.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       8.000   8.000   8.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       8.000   8.000   8.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       8.000   8.000   8.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       8.000   8.000   8.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       8.000   8.000   8.000  1.00  1.94           H\n"
"ENDMDL\n"
"MODEL       9\n"
"ATOM      1  N   LYS A   1       9.000   9.000   9.000  1.00  1.40           N\n"
"ATOM      2  CA  LYS A   1       9.000   9.000   9.000  1.00  0.52           C\n"
"ATOM      3  C   LYS A   1       9.000   9.000   9.000  1.00  0.39           C\n"
"ATOM      4  O   LYS A   1       9.000   9.000   9.000  1.00  0.33           O\n"
"ATOM      5  CB  LYS A   1       9.000   9.000   9.000  1.00  1.53           C\n"
"ATOM      6  CG  LYS A   1       9.000   9.000   9.000  1.00  2.38           C\n"
"ATOM      7  CD  LYS A   1       9.000   9.000   9.000  1.00  3.11           C\n"
"ATOM      8  CE  LYS A   1       9.000   9.000   9.000  1.00  3.58           C\n"
"ATOM      9  NZ  LYS A   1       9.000   9.000   9.000  1.00  4.21           N\n"
"ATOM     10  H1  LYS A   1       9.000   9.000   9.000  1.00  1.94           H\n"
"ENDMDL\n";

static const std::string GROTRJSTRING = "Generated by trjconv :  t=   0.00000\n"
"   10\n"
"    1LYS      N    1   0.0000   0.0000   0.0000\n"
"    1LYS     CA    2   0.0000   0.0000   0.0000\n"
"    1LYS      C    3   0.0000   0.0000   0.0000\n"
"    1LYS      O    4   0.0000   0.0000   0.0000\n"
"    1LYS     CB    5   0.0000   0.0000   0.0000\n"
"    1LYS     CG    6   0.0000   0.0000   0.0000\n"
"    1LYS     CD    7   0.0000   0.0000   0.0000\n"
"    1LYS     CE    8   0.0000   0.0000   0.0000\n"
"    1LYS     NZ    9   0.0000   0.0000   0.0000\n"
"    1LYS     H1   10   0.0000   0.0000   0.0000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   1.00000\n"
"   10\n"
"    1LYS      N    1   0.1000   0.1000   0.1000\n"
"    1LYS     CA    2   0.1000   0.1000   0.1000\n"
"    1LYS      C    3   0.1000   0.1000   0.1000\n"
"    1LYS      O    4   0.1000   0.1000   0.1000\n"
"    1LYS     CB    5   0.1000   0.1000   0.1000\n"
"    1LYS     CG    6   0.1000   0.1000   0.1000\n"
"    1LYS     CD    7   0.1000   0.1000   0.1000\n"
"    1LYS     CE    8   0.1000   0.1000   0.1000\n"
"    1LYS     NZ    9   0.1000   0.1000   0.1000\n"
"    1LYS     H1   10   0.1000   0.1000   0.1000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   2.00000\n"
"   10\n"
"    1LYS      N    1   0.2000   0.2000   0.2000\n"
"    1LYS     CA    2   0.2000   0.2000   0.2000\n"
"    1LYS      C    3   0.2000   0.2000   0.2000\n"
"    1LYS      O    4   0.2000   0.2000   0.2000\n"
"    1LYS     CB    5   0.2000   0.2000   0.2000\n"
"    1LYS     CG    6   0.2000   0.2000   0.2000\n"
"    1LYS     CD    7   0.2000   0.2000   0.2000\n"
"    1LYS     CE    8   0.2000   0.2000   0.2000\n"
"    1LYS     NZ    9   0.2000   0.2000   0.2000\n"
"    1LYS     H1   10   0.2000   0.2000   0.2000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   3.00000\n"
"   10\n"
"    1LYS      N    1   0.3000   0.3000   0.3000\n"
"    1LYS     CA    2   0.3000   0.3000   0.3000\n"
"    1LYS      C    3   0.3000   0.3000   0.3000\n"
"    1LYS      O    4   0.3000   0.3000   0.3000\n"
"    1LYS     CB    5   0.3000   0.3000   0.3000\n"
"    1LYS     CG    6   0.3000   0.3000   0.3000\n"
"    1LYS     CD    7   0.3000   0.3000   0.3000\n"
"    1LYS     CE    8   0.3000   0.3000   0.3000\n"
"    1LYS     NZ    9   0.3000   0.3000   0.3000\n"
"    1LYS     H1   10   0.3000   0.3000   0.3000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   4.00000\n"
"   10\n"
"    1LYS      N    1   0.4000   0.4000   0.4000\n"
"    1LYS     CA    2   0.4000   0.4000   0.4000\n"
"    1LYS      C    3   0.4000   0.4000   0.4000\n"
"    1LYS      O    4   0.4000   0.4000   0.4000\n"
"    1LYS     CB    5   0.4000   0.4000   0.4000\n"
"    1LYS     CG    6   0.4000   0.4000   0.4000\n"
"    1LYS     CD    7   0.4000   0.4000   0.4000\n"
"    1LYS     CE    8   0.4000   0.4000   0.4000\n"
"    1LYS     NZ    9   0.4000   0.4000   0.4000\n"
"    1LYS     H1   10   0.4000   0.4000   0.4000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   5.00000\n"
"   10\n"
"    1LYS      N    1   0.5000   0.5000   0.5000\n"
"    1LYS     CA    2   0.5000   0.5000   0.5000\n"
"    1LYS      C    3   0.5000   0.5000   0.5000\n"
"    1LYS      O    4   0.5000   0.5000   0.5000\n"
"    1LYS     CB    5   0.5000   0.5000   0.5000\n"
"    1LYS     CG    6   0.5000   0.5000   0.5000\n"
"    1LYS     CD    7   0.5000   0.5000   0.5000\n"
"    1LYS     CE    8   0.5000   0.5000   0.5000\n"
"    1LYS     NZ    9   0.5000   0.5000   0.5000\n"
"    1LYS     H1   10   0.5000   0.5000   0.5000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   6.00000\n"
"   10\n"
"    1LYS      N    1   0.6000   0.6000   0.6000\n"
"    1LYS     CA    2   0.6000   0.6000   0.6000\n"
"    1LYS      C    3   0.6000   0.6000   0.6000\n"
"    1LYS      O    4   0.6000   0.6000   0.6000\n"
"    1LYS     CB    5   0.6000   0.6000   0.6000\n"
"    1LYS     CG    6   0.6000   0.6000   0.6000\n"
"    1LYS     CD    7   0.6000   0.6000   0.6000\n"
"    1LYS     CE    8   0.6000   0.6000   0.6000\n"
"    1LYS     NZ    9   0.6000   0.6000   0.6000\n"
"    1LYS     H1   10   0.6000   0.6000   0.6000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   7.00000\n"
"   10\n"
"    1LYS      N    1   0.7000   0.7000   0.7000\n"
"    1LYS     CA    2   0.7000   0.7000   0.7000\n"
"    1LYS      C    3   0.7000   0.7000   0.7000\n"
"    1LYS      O    4   0.7000   0.7000   0.7000\n"
"    1LYS     CB    5   0.7000   0.7000   0.7000\n"
"    1LYS     CG    6   0.7000   0.7000   0.7000\n"
"    1LYS     CD    7   0.7000   0.7000   0.7000\n"
"    1LYS     CE    8   0.7000   0.7000   0.7000\n"
"    1LYS     NZ    9   0.7000   0.7000   0.7000\n"
"    1LYS     H1   10   0.7000   0.7000   0.7000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   8.00000\n"
"   10\n"
"    1LYS      N    1   0.8000   0.8000   0.8000\n"
"    1LYS     CA    2   0.8000   0.8000   0.8000\n"
"    1LYS      C    3   0.8000   0.8000   0.8000\n"
"    1LYS      O    4   0.8000   0.8000   0.8000\n"
"    1LYS     CB    5   0.8000   0.8000   0.8000\n"
"    1LYS     CG    6   0.8000   0.8000   0.8000\n"
"    1LYS     CD    7   0.8000   0.8000   0.8000\n"
"    1LYS     CE    8   0.8000   0.8000   0.8000\n"
"    1LYS     NZ    9   0.8000   0.8000   0.8000\n"
"    1LYS     H1   10   0.8000   0.8000   0.8000\n"
"   0.00000   0.00000   0.00000\n"
"Generated by trjconv :  t=   9.00000\n"
"   10\n"
"    1LYS      N    1   0.9000   0.9000   0.9000\n"
"    1LYS     CA    2   0.9000   0.9000   0.9000\n"
"    1LYS      C    3   0.9000   0.9000   0.9000\n"
"    1LYS      O    4   0.9000   0.9000   0.9000\n"
"    1LYS     CB    5   0.9000   0.9000   0.9000\n"
"    1LYS     CG    6   0.9000   0.9000   0.9000\n"
"    1LYS     CD    7   0.9000   0.9000   0.9000\n"
"    1LYS     CE    8   0.9000   0.9000   0.9000\n"
"    1LYS     NZ    9   0.9000   0.9000   0.9000\n"
"    1LYS     H1   10   0.9000   0.9000   0.9000\n"
"   0.00000   0.00000   0.00000\n";

#endif
