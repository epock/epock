
#include "../src/Point.hpp"
#include "../src/Vec3f.hpp"

#include <gtest/gtest.h>
#include <array>
#include <vector>


template <class T>
bool ElementsAre(T ref, T target)
{
    if (ref.size() != target.size())  return false;
    for (size_t i=0; i<ref.size(); ++i) if (ref[i] != target[i]) return false;
    return true;
}

class PointTest: public ::testing::Test
{
    
    public:
        PointTest(): 
            zeroPoint(0, 0, 0),
            onePoint(Point(1, 1, 1)),
            posPoint(Vec3f(1, 2, 3)),
            negPoint()
        {
            std::array<float, 3> a = {-1.0, -2.0, -3.0};
            negPoint = Point(a);
        }

    protected:
        Point zeroPoint, onePoint, posPoint, negPoint;
};


// -- Initialization ----------------------------------------------------------
TEST_F(PointTest, Initialization)
{
    EXPECT_FLOAT_EQ(0.0, zeroPoint.x.x);
    EXPECT_FLOAT_EQ(0.0, zeroPoint.x.y);
    EXPECT_FLOAT_EQ(0.0, zeroPoint.x.z);

    EXPECT_FLOAT_EQ(1.0, onePoint.x.x);
    EXPECT_FLOAT_EQ(1.0, onePoint.x.y);
    EXPECT_FLOAT_EQ(1.0, onePoint.x.z);

    EXPECT_FLOAT_EQ(1.0, posPoint.x.x);
    EXPECT_FLOAT_EQ(2.0, posPoint.x.y);
    EXPECT_FLOAT_EQ(3.0, posPoint.x.z);

    EXPECT_FLOAT_EQ(-1.0, negPoint.x.x);
    EXPECT_FLOAT_EQ(-2.0, negPoint.x.y);
    EXPECT_FLOAT_EQ(-3.0, negPoint.x.z);
}


// -- Distance functions ------------------------------------------------------
TEST_F(PointTest, SquaredDistance)
{
    EXPECT_FLOAT_EQ(0.0, zeroPoint.dist2(zeroPoint));
    EXPECT_FLOAT_EQ(3.0, zeroPoint.dist2(onePoint));
    EXPECT_FLOAT_EQ(zeroPoint.dist2(posPoint), zeroPoint.dist2(negPoint));
}

TEST_F(PointTest, Distance)
{
    EXPECT_FLOAT_EQ(0.0, zeroPoint.dist(zeroPoint));
    EXPECT_FLOAT_EQ(1.732051, zeroPoint.dist(onePoint));
    EXPECT_FLOAT_EQ(zeroPoint.dist(posPoint), zeroPoint.dist(negPoint));
}


// -- Point boundaries --------------------------------------------------------
TEST_F(PointTest, points_boundaries)
{
    std::vector<Point> v;
    std::array<double, 6> expected, actual;

    // Test empty vector.
    expected = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));

    // 1 point located at (0, 0, 0).
    v.push_back(zeroPoint);
    expected = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));

    // 1 point located at (1, 1, 1).
    v.pop_back();
    v.push_back(onePoint);
    expected = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));

    // 2 points located at (-1, -2, -3) and (1, 2, 3).
    v.pop_back();
    v.push_back(negPoint); v.push_back(posPoint);
    expected = {-1.0, 1.0, -2.0, 2.0, -3.0, 3.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));
}

TEST_F(PointTest, points_boundaries_with_pointers)
{
    std::vector<Point *> v;
    std::array<double, 6> expected, actual;

    // Test empty vector.
    expected = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));

    // 1 point located at (0, 0, 0).
    v.push_back(&zeroPoint);
    expected = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));

    // 1 point located at (1, 1, 1).
    v.pop_back();
    v.push_back(&onePoint);
    expected = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));

    // 2 points located at (-1, -2, -3) and (1, 2, 3).
    v.pop_back();
    v.push_back(&negPoint);
    v.push_back(&posPoint);
    expected = {-1.0, 1.0, -2.0, 2.0, -3.0, 3.0};
    EXPECT_TRUE(ElementsAre(expected, points_boundaries(v)));
}


// -- Main function  ----------------------------------------------------------
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

