#!/usr/bin/env python

# The Protein Data Bank format for atom coordinates
PDB_FMT = "%-6s%5s %4s%c%-4s%c%4s%s   %8.3f%8.3f%8.3f%6.2f%6.2f      %-4s%2s"

PDBBASE = """\
ATOM      1  N   LYS A   1      -8.655   5.770   8.371  1.00  1.40           N  
ATOM      2  CA  LYS A   1      -7.542   5.187   9.163  1.00  0.52           C  
ATOM      3  C   LYS A   1      -6.210   5.619   8.561  1.00  0.39           C  
ATOM      4  O   LYS A   1      -6.156   6.468   7.693  1.00  0.33           O  
ATOM      5  CB  LYS A   1      -7.641   3.666   9.159  1.00  1.53           C  
ATOM      6  CG  LYS A   1      -7.750   3.179  10.602  1.00  2.38           C  
ATOM      7  CD  LYS A   1      -9.158   3.466  11.126  1.00  3.11           C  
ATOM      8  CE  LYS A   1      -9.145   4.744  11.965  1.00  3.58           C  
ATOM      9  NZ  LYS A   1      -9.497   4.416  13.375  1.00  4.21           N  
ATOM     10  H1  LYS A   1      -8.409   6.745   8.105  1.00  1.94           H  """

class Atom(object):
    __slots__ = ("rec", "index", "name", "altloc", "resname",
                 "chain", "resid", "icode", "x", "y", "z",
                 "occ", "b", "element")

    def __init__(self, rec="ATOM", index=-1, name="", altloc="",
                 resname="", chain="", resid=0, icode="",
                 x=0.0, y=0.0, z=0.0, occ=1.0, b=0.0, element="",
                 residue=0, asa=float("nan")):
        self.rec = rec
        self.index = index
        self.name = name
        self.altloc = altloc
        self.resname = resname
        self.chain = chain
        self.resid = resid
        self.icode = icode
        self.x = x
        self.y = y
        self.z = z
        self.occ = occ
        self.b = b
        self.element = element if element else first_alpha(self.name)

    def topdb(self):
        indexbuf = "*****"
        residbuf = "****"
        altlocchar = " " if not self.altloc else self.altloc[0]
        insertion = " " if not self.icode else self.icode
        chain = " " if not self.chain else self.chain

        if self.index < 100000:
            indexbuf = "{0:5d}".format(self.index)
        elif self.index < 1048576:
            indexbuf = "{0:05x}".format(self.index)
        
        if self.resid < 10000:
            residbuf = "{0:4d}".format(self.resid)
        elif resid < 65536:
            residbuf = "{0:04x}".format(self.resid)

        namebuf = self.name.center(4)
        if len(self.name) > 2:
            namebuf = "{0:>4s}".format(self.name)

        return PDB_FMT % (self.rec, indexbuf, namebuf, altlocchar,
                          self.resname, chain[0], residbuf, insertion[0],
                          self.x, self.y, self.z, self.occ, self.b, 
                          "", self.element)


def first_alpha(s):
    """Return the first non-numeric character in a string.
    This function is used if the filed for element is absent."""
    for c in s:
        if c.isalpha():
            return c
    return ""


def pdb2atoms(content):
    """Return a list of atoms extracted from a PDB formatted string."""
    mol = []
    lines = content.splitlines()
    for line in lines:
        if line[0:6].strip() == "ATOM":
            index = int(line[6:11])
            name = line[12:16].strip()
            altloc = line[16].strip()
            resname = line[17:20].strip()
            chain = line[21]
            resid = int(line[22:26])
            icode = line[26]
            x = float(line[30:38])
            y = float(line[38:46])
            z = float(line[46:54])
            occ = float(line[54:60])
            b = float(line[60:66])
            a = Atom(rec="ATOM", index=index, name=name,
                     altloc=altloc, resname=resname,
                     chain=chain, resid=resid, icode=icode,
                     x=x, y=y, z=z, occ=occ, b=b)
            mol.append(a)
    return mol


def main():
    # Extract atoms from a PDB string (basically usefull to set atom
    # name, resname, etc. to non random values).
    atoms = pdb2atoms(PDBBASE)

    # Make a trajectory of 10 frames.
    # For each frame, all atoms will have (x,y,z) set to the frame number.
    baseframe = "MODEL    {0:4d}\n{1}ENDMDL\n"
    trajstr = ""
    for i in xrange(10):
        s = ""
        for atom in atoms:
            atom.x = atom.y = atom.z = float(i)
            s += atom.topdb() + "\n"
        trajstr += baseframe.format(i, s)
    print trajstr,





if __name__ == '__main__':
    main()

