#!/usr/bin/env python

import os
import subprocess


def hg_root():
    p = subprocess.Popen("hg root".split(), stdout=subprocess.PIPE)
    out, err = p.communicate()
    if p.returncode != 0:
        return "."
    return out.strip()


def main():
    rootdir = hg_root()
    sourcedir = os.path.join(rootdir, "src")
    testdir = os.path.join(rootdir, "testing")

    headers = [fn for fn in os.listdir(sourcedir) if fn.endswith(".hpp")]
    for h in headers:
        isok = False
        testbasename = "test-{0}.cpp".format(os.path.splitext(h)[0])
        testfile = os.path.join(testdir, testbasename)
        if os.path.exists(testfile):
            isok = True

        isoks = "OK" if isok else "-"
        print "{0:30s} {1}".format(h, isoks)


if __name__ == "__main__":
    main()
