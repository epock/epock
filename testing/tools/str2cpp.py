#!/usr/bin/env python

"""Read a file and print out a C++ compatible string."""


import argparse
import os.path
import sys


def parse_command_line():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("infile", nargs="?", default=sys.stdin,
                        type=argparse.FileType('r'),
                        help="topology file (or basically whatever file)")
    return parser.parse_args()


def main():
    args = parse_command_line()
    content = args.infile.read()
    args.infile.close()

    fname = args.infile.name
    if fname == "<stdin>":
        fname = "stdin"
    prefix = os.path.splitext(os.path.basename(fname))[0]

    lines = content.splitlines()
    lines = ['"' + line + '\\n"' for line in lines]
    lines = "\n".join(lines)
    print 'static const std::string {0} = {1};'.format(prefix, lines)

if __name__ == '__main__':
    main()
