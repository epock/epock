
#ifndef __CONFIG_PARSER_EPOCK_H__
#define __CONFIG_PARSER_EPOCK_H__

#include "ConfigParser.hpp"
#include "Cavity.hpp"
#include <string>
#include <vector>


namespace EpockDefaultParameters
{
    static std::string defaultPadding = "1.40";
    static std::string defaultPaddingMartini = "2.10";
    static std::string defaultGridSpacing = "1.00";
    static std::string defaultPrecision = "2.00";
}

class ConfigParserEpock: public ConfigParser
{
    public:
        ConfigParserEpock():
            _cavities(nullptr),
            _paths(),
            _useMartini(false)
        {}

        ConfigParserEpock(const ConfigParserEpock & orig) = delete;
        ConfigParserEpock & operator=(const ConfigParserEpock & orig) = delete;

        void read(void);

        inline void setCavities(std::vector<Cavity *> * vec) { _cavities = vec; }
        inline bool getUseMartini(void) const { return _useMartini; }
        inline void useMartini(const bool b=true) { setUseMartini(b); }
        inline void setUseMartini(const bool b) 
        { 
            _useMartini = b;
            if (_useMartini) EpockDefaultParameters::defaultPadding = EpockDefaultParameters::defaultPaddingMartini;
        }

        inline std::vector<std::vector<std::string>> getPathSearch(void) const
        {
            return _paths;
        }

    protected:
        std::vector<Cavity *> * _cavities;
        std::vector<std::vector<std::string>> _paths;
        bool _useMartini;
        
        void _parseCavity(const ConfigSection & section);
};


#endif