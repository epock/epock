
/*
 * This program only aims to know if user's compiler supports some
 * of C++ 11 features.
 * If not, the user can be classified in the "not up-to-date" list
 * and no support should be provided...
 */


#include <string>
#include <vector>
#include <iostream>

int main(void)
{
    std::vector<std::string> v = {"foo", "bar", "baz"};
    for (auto &s : v)
        std::cout << s << std::endl;
    return 0;
}
