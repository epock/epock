

#ifndef __ANALYSIS_PORE_PROFILE_HPP__
#define __ANALYSIS_PORE_PROFILE_HPP__

#include "Analysis.hpp"
#include "xdrfile.h"

class AnalysisPoreProfile : public Analysis
{
    public:
        AnalysisPoreProfile(Cavity & cav,
                            Topology & top, 
                            Trajectory & traj);

        ~AnalysisPoreProfile();

        AnalysisPoreProfile(const AnalysisPoreProfile & a) = delete;
        AnalysisPoreProfile & operator=(const AnalysisPoreProfile & a) = delete;

    protected:
        void _prerun(void);
        void _compute(void);
        void _writeHeader(void);

        XDRFILE * _xtc;
        rvec    * _xxtc;
        size_t    _stepxtc;
};


#endif