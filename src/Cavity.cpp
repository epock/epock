
#include "Cavity.hpp"
#include "Topology.hpp"
#include "logging.hpp"
#include "utils.hpp"
#include "xdrfile_xtc.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>


#pragma mark -
#pragma mark Volume calculation methods

float Cavity::calculate()
{
    calculatePoints();  
    return _volume;
}


void Cavity::calculatePoints()
{
    size_t npoints = 0;
    
    // Allocate more space if needed.
    if (_countContrib and _atomContrib[0].size() == _nFrames) 
    { 
        for (size_t i=0; i<_atomContrib.size(); ++i)
        {
            auto & row = _atomContrib[i];
            row.resize(row.size() + 1000);
        }       
    }
    
    // At this point, every point cavity flag (iscavity) has been set to true 
    // (see initGrid called from setCoordinates).
    
    // Find the points that do not overlap the protein and store the references
    // into a second vector.
    for (const Atom & atom: _coor)
    {
        float xmin = atom.x.x - atom.vdw - _padding;
        float ymin = atom.x.y - atom.vdw - _padding;
        float zmin = atom.x.z - atom.vdw - _padding;
        float xmax = atom.x.x + atom.vdw + _padding;
        float ymax = atom.x.y + atom.vdw + _padding;
        float zmax = atom.x.z + atom.vdw + _padding;

        float r2 = (atom.vdw + _padding) * (atom.vdw + _padding);

        std::array<size_t, 3> atmin = _gridCavity.nearestPointIndices(xmin, ymin, zmin);
        std::array<size_t, 3> atmax = _gridCavity.nearestPointIndices(xmax, ymax, zmax);
        std::array<size_t, 3> gdim = _gridCavity.dim();
        for(size_t i = atmin[0]; i <= atmax[0] && i < gdim[0]; ++i)
            for(size_t j = atmin[1]; j <= atmax[1] && j < gdim[1]; ++j)
                for(size_t k = atmin[2]; k <= atmax[2] && k < gdim[2]; ++k)
                {
                    if (_gridCavity(i, j, k) == nullptr) continue;
                    float distsq = _gridCavity(i, j, k)->dist2(atom);
                    if (r2 > distsq)
                    {
                        _gridCavity(i, j, k)->iscavity = false;
                        _gridCavity(i, j, k) = nullptr;
                        if (_countContrib)
                        {
                            _atomContrib[atom.getSerial()][_nFrames]++;
                        }
                    }
                }
    }

    if (_useContig) removeIsolatedPoints();

    _cavityPoints.clear();
    for (Point & p: _allPoints)
    {
        if (p.iscavity)
        {
            _cavityPoints.push_back(&p);
        }
    }

    npoints = _cavityPoints.size();
    if (not npoints)
    {
        _volume = 0.0;
        return;
    }
    
    // If precision is 0, calculate the volume just as POVME would do
    if (_precision < 0.00001)
    {
        _volume = npoints * (_gridSpacing * _gridSpacing * _gridSpacing);
    }
    // Else use a second grid to calculate the actual volume.
    else
    {
        _volume = volumeSetSpheres(_cavityPoints, _padding, _precision);
    }
    
    // Increase internal counter of frames.
    _nFrames++;
}


#pragma mark -
#pragma mark Remove isolated points methods

// Find points that are close the seed points, and points that 
// are close to points close to seed points...    
void Cavity::_findContig(Point * seedPoint)
{
    for (Point * testPoint : seedPoint->neighbors)
    {
        if (testPoint->iscavity and not testPoint->visited)
        {
            testPoint->visited = true;
            _findContig(testPoint);
        }
    }
}


// Remove points that are not seed points and are far from other points.
void Cavity::removeIsolatedPoints()
{
    std::vector<Point *> seed;

    for (size_t i=0; i<_allPoints.size(); ++i)
    {
        Point & pt = _allPoints[i];
        pt.visited = false;
        if (pt.iscavity and pt.isseed) 
        {
            pt.visited = true;
            seed.push_back(&pt);
        }
    }

    for (auto point : seed) _findContig(point);

    // At this stage, points that have not been visited are not cavity
    // because they are too far from cavity points.
    for (Point & pt : _allPoints)
    {
        if (not pt.visited)
        {
            pt.iscavity = false;
        }
    }
}


#pragma mark -
#pragma mark Special setters

void Cavity::setGrid(const std::string & fname)
{
    _allPoints.clear(); 
    std::vector<Point> atombuf;
    Topology::readPDB(fname, atombuf);
    
    for (const Point & p : atombuf)
    {
        _allPoints.push_back(p);
    }
    
    if (_allPoints.empty())
    {
        std::cerr << "ERROR: No grid points found in '" << fname << "'" << std::endl;
        exit(1);
    }
    
    // Initialize the grid itself.
    
    // Find the nearest distance between two points and use it as a dx.
    float dx = 0.0;
    size_t npoints = _allPoints.size();
    dx = (npoints < 2 ? 1.0 : _allPoints[0].dist2(_allPoints[1]));
    for (size_t i=0; i<npoints - 1; ++i)
    {
        for (size_t j=i+1; j<npoints; ++j)
        {
            float d = _allPoints[i].dist2(_allPoints[j]);
            dx = (d < dx ? d : dx);
        }
    }
    _gridSpacing = sqrt(dx);
    

    logging::info(String::format("%s: grid spacing determined from %s is %.2f.",
                                _name.c_str(), fname.c_str(), _gridSpacing));

    initGrid();
}


// Set the protein coordinates.
// Only keeps atoms that are contained within _pdbRegion.
void Cavity::setCoordinates(const std::vector<Atom> & atoms)
{
    _coor.clear();
    for (const Atom & a : atoms)
    {
        if (_pdbRegion.contains(a))
        {
            _coor.push_back(a);
        }
    }
	initGrid();
}

void Cavity::setMolecule(std::vector<Atom> * atoms)
{ 
    _mol = atoms;
    initAtomContrib(_mol->size());
}


#pragma mark -
#pragma mark Initialization methods

// Create a vector that will contain the points that 
// are in the user defined seed region.
void Cavity::_initSeed()
{
    for (size_t i=0; i<_allPoints.size(); ++i)
    {
        Point & pt = _allPoints[i];
        for (const auto seedRegion : _seedRegions)
        {
            if (seedRegion->contains(pt))
            {
                pt.isseed = true;
            }
        }
    }
}


// Initialize the _atomContrib array that count the number of contacts 
// between an atom and a grid point along a simulation.
void Cavity::initAtomContrib(size_t natoms)
{
    _atomContrib.resize(natoms);
    for (unsigned i=0; i<_atomContrib.size(); ++i)
    {
        _atomContrib[i].resize(1000, 0);
    }
}


// Initialize _gridCavity to fit the points present in _allPoints
void Cavity::initGrid()
{
    std::array<double, 6> bounds = points_boundaries(_allPoints);

    bounds[0] -= _gridSpacing * 2;
    bounds[2] -= _gridSpacing * 2;
    bounds[4] -= _gridSpacing * 2;
    bounds[1] += _gridSpacing * 2;
    bounds[3] += _gridSpacing * 2;
    bounds[5] += _gridSpacing * 2;

    _gridCavity = Grid3D<Point *>(bounds, _gridSpacing, nullptr);
    
    for (Point & p: _allPoints)
    {
        p.iscavity = true;
        _gridCavity.nearestPoint(p.x.x, p.x.y, p.x.z) = &p;
    }
}


// Initialize the cavity: calculate the grid point positions and 
// deduce the region to consider in the coordinate file
void Cavity::init()
{
    if (not _useExternalGrid)
    {
        initIncludeRegions();
    }
    // else, Cavity::setGrid should have been called earlier.
    initPDBRegion();
    initGrid();
     _initNeighbors();
     _initSeed();
}


// Initialize Point::neighbors vector.
// For each point, it will contain every point within _seedCutoff of itself.
void Cavity::_initNeighbors(void)
{
    float seedCutoff = sqrt(_seedCutoffSq);
    for (Point & pt : _allPoints)
    {
        float xmin = pt.x.x - seedCutoff;
        float ymin = pt.x.y - seedCutoff;
        float zmin = pt.x.z - seedCutoff;
        float xmax = pt.x.x + seedCutoff;
        float ymax = pt.x.y + seedCutoff;
        float zmax = pt.x.z + seedCutoff;

        std::array<size_t, 3> atmin = _gridCavity.nearestPointIndices(xmin, ymin, zmin);
        std::array<size_t, 3> atmax = _gridCavity.nearestPointIndices(xmax, ymax, zmax);
        std::array<size_t, 3> gdim = _gridCavity.dim();
        
        for(size_t i = atmin[0]; i <= atmax[0] && i < gdim[0]; ++i)
        {
            for(size_t j = atmin[1]; j <= atmax[1] && j < gdim[1]; ++j)
            {
                for(size_t k = atmin[2]; k <= atmax[2] && k < gdim[2]; ++k)
                {
                    Point * neighbor = _gridCavity(i, j, k);
                    if (neighbor != nullptr and neighbor ->dist2(pt) < _seedCutoffSq)
                    {
                        pt.neighbors.push_back(neighbor);
                    }
                }
            }
        }
    }
}


// Calculate the minimum box that includes all grid points.
void Cavity::initPDBRegion()
{
    std::array<double, 6> bounds = points_boundaries(_allPoints);
    
    double minx, maxx;
    double miny, maxy;
    double minz, maxz;
    minx = bounds[0]; maxx = bounds[1];
    miny = bounds[2]; maxy = bounds[3];
    minz = bounds[4]; maxz = bounds[5];
    
    _pdbRegion = BoxRegion();
    _pdbRegion.setCenter((minx + maxx) * 0.5, 
                         (miny + maxy) * 0.5, 
                         (minz + maxz) * 0.5);
    _pdbRegion.setDim(maxx - minx + 2 * _padding + 1,
                      maxy - miny + 2 * _padding + 1,
                      maxz - minz + 2 * _padding + 1);
}


// Calculate the position of the grid points and store in an
// internal vector.
void Cavity::initIncludeRegions()
{
    _allPoints.clear();
    std::array<float, 6> bounds;
    if (! _includeRegions.size())
    {
        throw MyException("Trying to initialize include regions while "
                          "include regions have not been set "
                          "(_includeRegions is empty).");
    }
    _includeRegions.front()->boundaries(bounds);
    
    
    // Define a big box that includes all include regions.
    //  for (const Region * const r : _includeRegions)
    for (size_t i=0; i<_includeRegions.size(); ++i)
    {
        const Region * const r = _includeRegions[i];
        std::array<float, 6> cur_bounds;
        r->boundaries(cur_bounds);
        if      (cur_bounds[0] < bounds[0]) bounds[0] = cur_bounds[0];
        else if (cur_bounds[1] > bounds[1]) bounds[1] = cur_bounds[1];
        if      (cur_bounds[2] < bounds[2]) bounds[2] = cur_bounds[2];
        else if (cur_bounds[3] > bounds[3]) bounds[3] = cur_bounds[3];
        if      (cur_bounds[4] < bounds[4]) bounds[4] = cur_bounds[4];
        else if (cur_bounds[5] > bounds[5]) bounds[5] = cur_bounds[5];
    }
    
    // Build a regular grid.
    for (float x=bounds[0]; x<bounds[1]+_gridSpacing; x+=_gridSpacing)
    {
        for (float y=bounds[2]; y<bounds[3]+_gridSpacing; y+=_gridSpacing)
        {
            for (float z=bounds[4]; z<bounds[5]+_gridSpacing; z+=_gridSpacing) 
            {
                Point p = Point(x,y,z);
                for (const Region * const r : _includeRegions)
                {
                    // Is the sphere centered on the point 
                    // with radius = padding inside the region (as opposed 
                    // to is the point inside the region).
                    if (r->intersects(p, -_padding))
                    {
                        // Only include this points if it is not in
                        // an exclude region.
                        bool isExcluded = false;
                        for (const Region * const er : _excludeRegions)
                        {
                            if (er->contains(p))
                            {
                                isExcluded = true;
                                break;
                            }
                        }
                        if (not isExcluded)
                        {
                            _allPoints.push_back(p);
                        }
                        break;
                    }
                }
            }
        }
    }

    // Dies if grid contains no point after initialization.
    if (_allPoints.size() == 0)
    {
        logging::fatal("Grid contains no point after initialization "
                       "(check include and exclude regions overlap).");
    }
}


#pragma mark -
#pragma mark TXT I/O methods

void Cavity::_writeResidueContrib(const std::string & fname)
{
    // _atomContrib subarrays have been allocated block by block to be more efficient.
    // So the array's size is greater than the actual number of frames.
    // We hence first resize it to fit the number of frames.
    for (auto & row : _atomContrib)
    {
        row.resize(_nFrames);
    }
    
    float zbar = 0.0;
    size_t ressize = 0;
    std::vector<size_t> resCount(_nFrames, 0);
    std::string resref = _mol->at(0).getIdentifier(false);

    std::ofstream output(fname);
    output.setf(std::ios::fixed);
    if (output.is_open())
    {    
        for (size_t atomi=0; atomi<_mol->size(); ++atomi)
        {
            const Atom & a = _mol->at(atomi);
            const std::vector<size_t> & atomCount = _atomContrib.at(atomi);
            if (a.getIdentifier(false) == resref)
            {
                zbar += a.x.z;
                ressize++;                
                for (size_t framei=0; framei<atomCount.size(); ++framei)
                {
                    resCount[framei] += atomCount[framei];
                }
            }
            else
            {
                // If count is not 0 at some point
                if (std::any_of(resCount.begin(), resCount.end(), [] (size_t k) { return k > 0; }))
                {
                    output.width(20);
                    output << std::left << resref << " ";
                    output.width(8); output.precision(2);
                    output << std::right << zbar / ressize << "  ";
                    for (const size_t & c : resCount)
                    {
                        output.width(5);
                        output << std::right << c << " ";
                    }
                    output << "\n";
                }
                // Reset variables for next residue
                resref = a.getIdentifier(false);
                zbar = a.x.z;
                ressize = 1;
                resCount.assign(resCount.size(), 0);
            }
        }
    }
    else
    {
        std::cerr << "ERROR: Cannot open output file '" << fname << "'" << std::endl;
        exit(1);
    }
}

void Cavity::_writeAtomContrib(const std::string & fname)
{
    // _atomContrib subarrays have been allocated block by block to be more efficient.
    // So the array's size is greater than the actual number of frames.
    // We hence first resize it to fit the number of frames.
    for (auto & row : _atomContrib)
    {
        row.resize(_nFrames);
    }
    
    std::ofstream output(fname);
    output.setf(std::ios::fixed);
    if (output.is_open())
    {
        for (size_t i=0; i<_atomContrib.size(); ++i)
        {
            const Atom & a = _mol->at(i);
            const std::vector<size_t> & atomCount = _atomContrib[i];
            if (std::any_of(atomCount.begin(), atomCount.end(), [] (size_t k) { return k > 0; }))
            {
                output.width(20); 
                output << std::left << a.getIdentifier() << " ";
                output.width(8); output.precision(2); 
                output << std::right << a.x.z << " ";
                for (const size_t c : atomCount)
                {
                    output.width(5); output << std::right << c << " ";
                }
                output << std::endl;
            }
        }
        output.close();
    }
    else
    {
        std::cerr << "ERROR: Cannot open output file '" << fname << "'" << std::endl;
        exit(1);
    }   
}

void Cavity::writeContrib(const std::string & fname)
{
    if (_countContrib == countAtomContrib)
    {
        _writeAtomContrib(fname);
    }
    else if (_countContrib == countResidueContrib)
    {
        _writeResidueContrib(fname);
    }
    
}

void Cavity::writeContrib()
{
    std::string fname =  _name + "_contrib.dat";
    writeContrib(fname);
}


#pragma mark -
#pragma mark PDB I/O methods

// Write a PDB formatted file of the points constituing the cavity
void Cavity::writePDB(const std::string & fname) const
{
    std::ofstream output(fname);
    if (output.is_open())
    {
        for (const Point & pt:_allPoints)
        {
            if (pt.iscavity)
            {
                output << pt.topdb() << std::endl;
            }
        }
        output.close();
    }
    else
    {
        std::cerr << "ERROR: Cannot open output file '" << fname << "'" << std::endl;
        exit(1);
    }
}

void Cavity::write() const
{
    writePDB(_name + ".pdb");
}


void Cavity::writePDB() const
{
    std::string fname =  _name + ".pdb";
    writePDB(fname);
}


// Write of PDB formatted file representing all the points in the include
// regions.
void Cavity::ir2pdb(const std::string & fname) const
{
    std::ofstream output(fname);
    if (output.is_open())
    {
        for (const Point & pt:_allPoints)
        {
            output << pt.topdb() << std::endl;
        }
        output.close();
    }
    else
    {
        std::cerr << "ERROR: Cannot open output file '" << fname << "'" << std::endl;
        exit(1);
    }
}

void Cavity::ir2pdb() const
{
    std::string fname = _name + ".pdb";
    ir2pdb(fname);
}


#pragma mark -
#pragma mark Constructors & Destructors

Cavity::Cavity():
_name(""),
_gridSpacing(0.5),
_padding(1.4),
_precision(_gridSpacing),
_volume(0.0),
_useExternalGrid(false),
_includeRegions(),
_excludeRegions(),
_seedRegions(),
_allPoints(),
_cavityPoints(),
_gridCavity(),
_coor(),
_mol(nullptr),
_pdbRegion(), 
_atomContrib(),
_countContrib(countNone),
_seedCutoffSq(0.0),
_useContig(false),
_nFrames(0),
_usePoreProfile(false)
{
}

Cavity::~Cavity()
{
    for (size_t i=0; i<_includeRegions.size(); ++i)
    {
        delete _includeRegions[i];
    }
    for (size_t i=0; i<_excludeRegions.size(); ++i)
    {
        delete _excludeRegions[i];
    }
    for (size_t i=0; i<_seedRegions.size(); ++i)
    {
        delete _seedRegions[i];
    }
}


#pragma mark -
#pragma mark Static functions.

// Calculate the volume of a set of spheres with the same radius.
float volumeSetSpheres (const std::vector<Point *> points, 
                        const double radius, 
                        const double prec)
{
    std::array<double, 6> bounds = points_boundaries(points);
    const double radius2 = radius * radius;

    bounds[0] -= radius * 2;
    bounds[2] -= radius * 2;
    bounds[4] -= radius * 2;
    bounds[1] += radius * 2;
    bounds[3] += radius * 2;
    bounds[5] += radius * 2;
    
    Grid3D<bool> g(bounds, prec, 0);

    for (const Point * const p : points)
    {
        std::array<size_t, 3> ijkmin = g.nearestPointIndices(p->x.x-radius-prec,
                                                             p->x.y-radius-prec,
                                                             p->x.z-radius-prec);
        std::array<size_t, 3> ijkmax = g.nearestPointIndices(p->x.x+radius+prec,
                                                             p->x.y+radius+prec,
                                                             p->x.z+radius+prec);
        for (size_t i=ijkmin[0]; i<ijkmax[0]; ++i)
        {
            for (size_t j=ijkmin[1]; j<ijkmax[1]; ++j)
            {
                for (size_t k=ijkmin[2]; k<ijkmax[2]; ++k)
                {
                    Point gp(g.coordinates(i, j, k));                    
                    if (gp.dist2(p) <= radius2)
                    {
                        g.at(i, j, k) = true;
                    }
                }
            }
        }
    }
    size_t npoints = accumulate(g.getGrid().begin(), g.getGrid().end(), 0);
    return  npoints * (prec * prec * prec);
}


// Calculate the volume of a set of atoms.
float volumeSetAtoms (const std::vector<Atom> points, const double prec)
{
    std::array<double, 6> bounds = points_boundaries(points);
    
    // Find the biggest atom radius.
    float r = 0.0;
    for (auto a : points) r = (a.vdw > r ? a.vdw : r);
    
    bounds[0] -= r * 2;
    bounds[2] -= r * 2;
    bounds[4] -= r * 2;
    bounds[1] += r * 2;
    bounds[3] += r * 2;
    bounds[5] += r * 2;
    
    Grid3D<bool> g(bounds, prec, 0);

    for (const Atom & p : points)
    {
        double radius = p.vdw;
        double radius2 = radius * radius;
        std::array<size_t, 3> ijkmin = g.nearestPointIndices(p.x.x-radius-prec,
                                                             p.x.y-radius-prec,
                                                             p.x.z-radius-prec);
        std::array<size_t, 3> ijkmax = g.nearestPointIndices(p.x.x+radius+prec,
                                                             p.x.y+radius+prec,
                                                             p.x.z+radius+prec);
        for (size_t i=ijkmin[0]; i<ijkmax[0]; ++i)
        {
            for (size_t j=ijkmin[1]; j<ijkmax[1]; ++j)
            {
                for (size_t k=ijkmin[2]; k<ijkmax[2]; ++k)
                {
                    Point gp(g.coordinates(i, j, k));                    
                    if (gp.dist2(p) <= radius2)
                    {
                        g.at(i, j, k) = true;
                    }
                }
            }
        }
    }
    size_t npoints = accumulate(g.getGrid().begin(), g.getGrid().end(), 0);
    return  npoints * (prec * prec * prec);
}


