
#include "Analysis.hpp"


void Analysis::run(void)
{
    _compute();
}

void Analysis::_prerun(void)
{
}

void Analysis::_postrun(void)
{
}

void Analysis::_openOutputFile(void)
{
    _output = new std::ofstream(_outputname, std::ofstream::out);

    if (not _output->is_open())
    {
        std::cerr << "ERROR: Cannot open output file '" << _outputname;
        std::cerr << "'" << std::endl;
        exit(1);
    }

    _output->setf(std::ios::fixed);
    _output->setf(std::ios::right);
}

Analysis::Analysis(Cavity & cav, Topology & top, Trajectory & traj):
    _cavity(cav), 
    _top(top), 
    _traj(traj),
    _outputname(),
    _output()
{
    _prerun();
}


Analysis::~Analysis() 
{ 
    _postrun();
    if (_output != nullptr) 
        delete _output; 
} 