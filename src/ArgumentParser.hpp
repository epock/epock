
#include <iostream>
#include <string>
#include <vector>

#include "logging.hpp"
#include "utils.hpp"
#include "version.hpp"

class ArgumentParser
{
    public:
        std::string prog = PROJECT_EXECUTABLE;
        std::string version = PROJECT_VERSION;
        std::string usage = "";
        std::string shortDescription = "";
        std::vector<std::string> help;
        
        std::string topname = "";
        std::string trajname = "";
        std::string cfgname = "";
        std::string radiiname = "";
        std::string outputname = "volume.dat";

        float start = -1;
        float end = -1;
        float timestep = -1.0;
        bool outputVolume = true;
        bool outputCavXTC = false;
        bool dryrun = false;
        bool martini = false;
        bool customRadii = false;
        bool calcMolecule = false;
        bool verbose = false;
        
        ArgumentParser():help()
        {
            shortDescription = "Calculates the volume of cavities along a trajectory.";
            usage = "usage: " + prog + " [options] -s coor.pdb -c conf.cfg";
            help =
            {
                "File I/O:",
                "---------",
                "  -s          Input          Coordinate file (pdb format)",
                "  -c          Input          Configuration file",
                "  -f          Input, Opt     Trajectory file (gromacs xtc format)",
                "  -o          Output, Opt    Volume output file (default: volume.dat)",
                "",
                "Options:",
                "--------",
                "  -b                   First frame (ps) to read from trajectory",
                "  -e                   Last frame (ps) to read from trajectory",
                "  --ox                 Output cavity trajectory file (default: no)",
                "  --dt <timestep>      Only use frame when t MOD dt = first time (ps)",
                "  --martini            Use martini vdW radii i.e. 4.7 for beads and 2.1 for probe",
                "  --mol                Calculate volume of a molecule instead of a cavity",
                "  --radii <filename>   Use custom atom radii",
                "  --dry-run            Stop after basic initialization and write the grid as a PDB file",
                "  -h, --help           Show this message and exit",
                "  --version            Show the version number and exit",
                "  -v                   Enable verbose mode",
                ""
            };
        }
        
        // Description:
        // Display a message (supposed to be a usage message) on stderr.
        void show_help() const
        {
            std::cerr << prog << " v" << version << std::endl << std::endl;
            std::cerr << shortDescription << std::endl << std::endl;
            std::cerr << usage << std::endl << std::endl;
            for (const std::string s: help)
            {
                std::cerr << s << std::endl;
            }
        }


        void parseArgs(const int argc, const char * const argv[])
        {
            std::vector<std::string> vargv(argc);
            for (int i=0; i<argc; ++i) vargv[i] = argv[i];
            parseArgs(vargv);
        }

        // Description:
        // Parse command-line and initialize internal variables.
        void parseArgs(const std::vector<std::string> & argv)
        {
            // Command-line parsing.
            for (size_t i=1; i<argv.size(); ++i)
            {
                const std::string arg = argv[i];
                if (arg[0] == '-')
                {
                    if (arg == "-h" or arg == "--help")
                    {
                        show_help();
                        exit(0);
                    }
                    else if (arg == "--version")
                    {
                        std::cerr << version << std::endl;
                        exit(0);
                    }
                    else if (arg == "-s") { getValue(++i, argv, topname); }
                    else if (arg == "-f") { getValue(++i, argv, trajname); }
                    else if (arg == "-c") { getValue(++i, argv, cfgname); }
                    else if (arg == "-b") { getValue(++i, argv, start); }
                    else if (arg == "-e") { getValue(++i, argv, end); }
                    else if (arg == "--dt") { getValue(++i, argv, timestep); }
                    else if (arg == "-o")
                    {
                        getValue(++i, argv, outputname);
                        outputVolume = true;
                    }
                    else if (arg == "-v") { verbose = true; }
                    else if (arg == "--ox") { outputCavXTC = true; }
                    else if (arg == "--dry-run") { dryrun = true; }
                    else if (arg == "--martini") { martini = true; }
                    else if (arg == "--mol") { calcMolecule = true; }
                    else if (arg == "--radii")
                    {
                        customRadii = true;
                        getValue(++i, argv, radiiname);;
                    }
                    else
                    {
                        show_help();
                        logging::fatal("Unrecognized option '" + arg + "'");
                    }
                }
                else
                {
                    logging::fatal("Unrecognized argument '" + arg + "'");
                }
            }
            if (topname.empty() or cfgname.empty())
            {
                show_help();
                logging::fatal("-s and -c options are mandatory (so not optionnal...).");
            }
            if (not trajname.empty())
            {
                outputVolume = true;
            }
            else
            {
                trajname = topname;
            }
            if (customRadii and martini)
            {
                logging::fatal("options martini and radii are mutualy exclusive");
            }
        }

    protected:
        void checkHasValue(const size_t i, const std::vector<std::string> & argv)
        {
            if (i >= argv.size())
            {
                show_help();
                logging::fatal(String::format("expected value after option '%s' but got nothing.", argv[i-1].c_str()));
            }
            if (argv[i][0] == '-')
            {
                show_help();
                logging::fatal(String::format("expected value after option '%s' but got '%s'.", argv[i-1].c_str(), argv[i].c_str()));
            }
        }

        template<typename T>
        void getValue(const size_t i, const std::vector<std::string> & argv, T & value)
        {
            checkHasValue(i, argv);
            from_string<T>(value, argv[i]);
        }

        // Specialisation for string values.
        void getValue(const size_t i, const std::vector<std::string> & argv, std::string & value)
        {
            checkHasValue(i, argv);
            value = argv[i];
        }

};
