
#include "ConfigParserEpock.hpp"
#include "logging.hpp"
#include "utils.hpp"
#include <utility>      // std::pair

using namespace std;

void ConfigParserEpock::read(void)
{
    this->addDefault("grid_spacing", EpockDefaultParameters::defaultGridSpacing);
    this->addDefault("padding", EpockDefaultParameters::defaultPadding);
    this->addDefault("precision", EpockDefaultParameters::defaultPrecision);
    ConfigParser::read();
    for (const ConfigSection & section:this->getSections())
    {
        std::string secname = String::tolower(section.getName());
        _paths.push_back({});
        _parseCavity(section);
    }

    // Paths check: did the user ask to find paths between actually existing
    // cavities? (do the names exist?)
    for (size_t cavid=0; cavid<_cavities->size(); ++cavid)
    {
        Cavity * origin = _cavities->at(cavid);
        std::string originName = origin->getName();
        for (auto targetName : _paths[cavid])
        {
            bool isValidTargetName = false;
            for (Cavity * c : *_cavities)
            {
                if (c->getName() == targetName)
                {
                    isValidTargetName = true;
                    break;
                }
            }
            if (not isValidTargetName)
            {
                std::string msg = "no such cavity " + targetName;
                logging::fatal(msg);
            }
        }
    }
}


void ConfigParserEpock::_parseCavity(const ConfigSection & section)
{
    bool contigRegionSet=false;
    Cavity * c = new Cavity();
    c->setName(section.getName());
    std::string gridFilename = "";
    for (auto p:section.getParameters())
    {
        const std::string opt = String::tolower(p.first);
        const std::string val = String::toupper(p.second);

        ////////////////////////// Spheres ///////////////////////////////////
        if (opt == "include_sphere")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 4)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "include_sphere format is 'include_sphere = x y z radius'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }   
            float x, y, z, r;
            bool bx, by, bz, br;
            bx = from_string<float>(x, tokens[0]);
            by = from_string<float>(y, tokens[1]);
            bz = from_string<float>(z, tokens[2]);
            br = from_string<float>(r, tokens[3]);
            if (not (bx and by and bz and br))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 4 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            SphereRegion *sphere = new SphereRegion(x, y, z, r);
            c->addIncludeRegion(sphere);
        }
        else if (opt == "exclude_sphere")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 4)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "exclude_sphere format is 'exclude_sphere = x y z radius'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }
            float x, y, z, r;
            bool bx, by, bz, br;
            bx = from_string<float>(x, tokens[0]);
            by = from_string<float>(y, tokens[1]);
            bz = from_string<float>(z, tokens[2]);
            br = from_string<float>(r, tokens[3]);
            if (not (bx and by and bz and br))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 4 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            SphereRegion *sphere = new SphereRegion(x, y, z, r);
            c->addExcludeRegion(sphere);
        }
        
        ////////////////////////// Boxes ///////////////////////////////////
        else if (opt == "include_box")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 6)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "include_box format is 'include_box = xc yc zc xdim ydim zdim'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }   
            float xc, yc, zc, xdim, ydim, zdim;
            bool bxc, byc, bzc, bxdim, bydim, bzdim;
            bxc = from_string<float>(xc, tokens[0]);
            byc = from_string<float>(yc, tokens[1]);
            bzc = from_string<float>(zc, tokens[2]);
            bxdim = from_string<float>(xdim, tokens[3]);
            bydim = from_string<float>(ydim, tokens[4]);
            bzdim = from_string<float>(zdim, tokens[5]);
            if (not (bxc and byc and bzc and bxdim and bydim and bzdim))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 6 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            BoxRegion *box = new BoxRegion({xc, yc, zc}, {xdim, ydim, zdim});
            c->addIncludeRegion(box);
        }
        else if (opt == "exclude_box")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 6)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "exclude_box format is 'exclude_box = xc yc zc xdim ydim zdim'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }   
            float xc, yc, zc, xdim, ydim, zdim;
            bool bxc, byc, bzc, bxdim, bydim, bzdim;
            bxc = from_string<float>(xc, tokens[0]);
            byc = from_string<float>(yc, tokens[1]);
            bzc = from_string<float>(zc, tokens[2]);
            bxdim = from_string<float>(xdim, tokens[3]);
            bydim = from_string<float>(ydim, tokens[4]);
            bzdim = from_string<float>(zdim, tokens[5]);
            if (not (bxc and byc and bzc and bxdim and bydim and bzdim))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 6 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            BoxRegion *box = new BoxRegion({xc, yc, zc}, {xdim, ydim, zdim});
            c->addExcludeRegion(box);
        }
        
        ////////////////////////// Cylinders ///////////////////////////////
        else if (opt == "include_cylinder")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 7)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "include_cylinder format is 'include_cylinder = x1 y1 z1 x2 x2 z2 radius'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }                   
            float x1, x2, y1, y2, z1, z2, r;
            bool bx1, bx2, by1, by2, bz1, bz2, br;
            bx1 = from_string<float>(x1, tokens[0]);
            by1 = from_string<float>(y1, tokens[1]);
            bz1 = from_string<float>(z1, tokens[2]);
            bx2 = from_string<float>(x2, tokens[3]);
            by2 = from_string<float>(y2, tokens[4]);
            bz2 = from_string<float>(z2, tokens[5]);
            br  = from_string<float>(r, tokens[6]);
            if (not (bx1 and bx2 and by1 and by2 and bz1 and bz2 and br))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 7 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            CylinderRegion *cylinder = new CylinderRegion(x1, y1, z1, 
              x2, y2, z2,
              r);
            c->addIncludeRegion(cylinder);
        }
        else if (opt == "exclude_cylinder")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 7)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "exclude_cylinder format is 'exclude_cylinder = x1 y1 z1 x2 x2 z2 radius'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }
            float x1, x2, y1, y2, z1, z2, r;
            bool bx1, bx2, by1, by2, bz1, bz2, br;
            bx1 = from_string<float>(x1, tokens[0]);
            by1 = from_string<float>(y1, tokens[1]);
            bz1 = from_string<float>(z1, tokens[2]);
            bx2 = from_string<float>(x2, tokens[3]);
            by2 = from_string<float>(y2, tokens[4]);
            bz2 = from_string<float>(z2, tokens[5]);
            br  = from_string<float>(r, tokens[6]);
            if (not (bx1 and bx2 and by1 and by2 and bz1 and bz2 and br))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 7 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            CylinderRegion *cylinder = new CylinderRegion(x1, y1, z1, 
              x2, y2, z2,
              r);
            c->addExcludeRegion(cylinder);
        }
        
        ////////////////////// Initialization by grid //////////////////////
        else if (opt == "grid")
        {
            std::string str = val;
            str.erase(std::remove(str.begin(), str.end(), '"'), str.end());
            str.erase(std::remove(str.begin(), str.end(), '\''), str.end());
            gridFilename = str;
        }
        
        ////////////////////// Congiuous parameters/////////////////////////
        else if (opt == "contiguous")
        {
            if (val == "OFF" or val == "FALSE" or val == "0")
            {
                c->setUseContig(false);
            }
            else if (val == "ON" or val == "TRUE" or val == "1")
            {
                c->setUseContig(true);
            }
            else
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to bool\n."
                                                "Please choose between ON, OFF, TRUE, FALSE, 0 or 1\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
        }
        else if (opt == "contiguous_seed_sphere")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 4)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "contiguous_seed_sphere format is 'contiguous_seed_sphere = x y z radius'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }                   
            float x, y, z, r;
            bool bx, by, bz, br;
            bx = from_string<float>(x, tokens[0]);
            by = from_string<float>(y, tokens[1]);
            bz = from_string<float>(z, tokens[2]);
            br = from_string<float>(r, tokens[3]);
            if (not (bx and by and bz and br))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 4 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            SphereRegion *sphere = new SphereRegion(x, y, z, r);
            c->addSeedRegion(sphere);
            contigRegionSet = true;
        }
        else if (opt == "contiguous_seed_box")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 6)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "contiguous_seed_box format is 'contiguous_seed_box = xc yc zc xdim ydim zdim'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }
            float xc, yc, zc, xdim, ydim, zdim;
            bool bxc, byc, bzc, bxdim, bydim, bzdim;
            bxc = from_string<float>(xc, tokens[0]);
            byc = from_string<float>(yc, tokens[1]);
            bzc = from_string<float>(zc, tokens[2]);
            bxdim = from_string<float>(xdim, tokens[3]);
            bydim = from_string<float>(ydim, tokens[4]);
            bzdim = from_string<float>(zdim, tokens[5]);
            if (not (bxc and byc and bzc and bxdim and bydim and bzdim))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 6 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            BoxRegion *box = new BoxRegion({xc, yc, zc}, {xdim, ydim, zdim});
            c->addSeedRegion(box);
            contigRegionSet = true;
        }  
        else if (opt == "contiguous_seed_cylinder")
        {
            std::vector<std::string> tokens = String::split(val);
            if (tokens.size() != 7)
            {
                std::string msg = String::format("Bad format: '%s = %s'\n"
                                                "contiguous_seed_cylinder format is 'contiguous_seed_cylinder = x1 y1 z1 x2 x2 z2 radius'",
                                                p.first.c_str(), p.second.c_str());
                logging::fatal(msg);
            }                   
            float x1, x2, y1, y2, z1, z2, r;
            bool bx1, bx2, by1, by2, bz1, bz2, br;
            bx1 = from_string<float>(x1, tokens[0]);
            by1 = from_string<float>(y1, tokens[1]);
            bz1 = from_string<float>(z1, tokens[2]);
            bx2 = from_string<float>(x2, tokens[3]);
            by2 = from_string<float>(y2, tokens[4]);
            bz2 = from_string<float>(z2, tokens[5]);
            br  = from_string<float>(r, tokens[6]);
            if (not (bx1 and bx2 and by1 and by2 and bz1 and bz2 and br))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.\n"
                                                "Expected 7 floatting-point numbers\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            CylinderRegion *cylinder = new CylinderRegion(x1, y1, z1, 
                                                          x2, y2, z2,
                                                          r);
            c->addSeedRegion(cylinder);
            contigRegionSet = true;
        }
        else if (opt == "contiguous_cutoff")
        {
            float f;
            if (not from_string<float>(f, val))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            c->setSeedCutoff(f);
        }
        
        ////////////////////// Other parameters ////////////////////////////
        else if (opt == "grid_spacing")
        {
            float f;
            if (not from_string<float>(f, val))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            c->setGridSpacing(f);
        }
        else if (opt == "padding")
        {
            float f;
            if (not from_string<float>(f, val))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            c->setPadding(f);
        }
        else if (opt == "precision")
        {
            float f;
            if (not from_string<float>(f, val))
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to float.",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
            // Precision is actually the grid spacing of the small grid
            // used to calculate the actual volume of the remaning grid 
            // points.
            // For convenience, the user provides the inverse of this value
            // So that when the precision is increased, grid spacing is
            // smaller (so the precision is actually increased!).
            if (f < 0.000001) c->setPrecision(0.0);
            else c->setPrecision(1.0 / f);
        }           
        else if (opt == "contribution")
        {
            if (val == "ATOM")
            {
                c->setCountContrib(countAtomContrib);
            }
            else if (val == "RESIDUE")
            {
                c->setCountContrib(countResidueContrib);
            }
            else if (val == "NONE" or val == "FALSE")
            {
                c->setCountContrib(false);
            }

            else
            {
                std::string msg = String::format("Invalid value for parameter '%s': %s\n"
                                                "Please choose between 'atom' and 'residue'\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);                
            }
        }
        else if (opt == "profile")
        {
            if (val == "OFF" or val == "FALSE" or val == "0")
            {
                c->setUsePoreProfile(false);
            }
            else if (val == "ON" or val == "TRUE" or val == "1")
            {
                c->setUsePoreProfile(true);
            }
            else
            {
                std::string msg = String::format("%s: invalid conversion from '%s' to bool\n."
                                                "Please choose between ON, OFF, TRUE, FALSE, 0 or 1\n",
                                                opt.c_str(), val.c_str());
                logging::fatal(msg);
            }
        }
        else if (opt == "path")
        {
            _paths.back().push_back(val);
        }
        else
        {
            std::string msg = String::format("Unknown parameter '%s' in configuration file.",
                                            opt.c_str());
            logging::fatal(msg);
        }
    }
    if (not gridFilename.empty())
    {
        c->setUseExternalGrid(true);
        c->setGrid(gridFilename);
    }
    else if (c->getNumberOfIncludeRegions() < 1)
    {
        std::string msg = String::format("no include region read for cavity '%s'.",
                                        c->getName().c_str());
        logging::fatal(msg);
    }
    if (c->getUseContig())
    {
        if (c->getSeedCutoffSq() < 0.0001)
        {
            std::string msg = String::format("contiguous points search is set to true but "
                                            "contigous_cutoff is set to 0.0 (or not set).");
            logging::warning(msg);
        }
        if (not contigRegionSet)
        {
            std::string msg = String::format("contiguous points search is set to true but "
                                            "no seed region has been defined.");
            logging::warning(msg);
        }
    }
    c->init();
    _cavities->push_back(c);        
}
