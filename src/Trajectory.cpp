

#include "Trajectory.hpp"
#include "utils.hpp"

#include <iostream>

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark TrajectoryTRR

///////////////////////////////////////////////////////////////////////////////

void TrajectoryTRR::update(void)
{
    _xdr = xdrfile_open(_filename.c_str(), "r");
    if (not _xdr) throw FileNotFoundException(_filename);
    _status = _readNatoms();
    if (_status != exdrOK) throw EndOfFileException();
    _x = new rvec[_natoms];
    _v = new rvec[_natoms];
    _f = new rvec[_natoms];
}

size_t TrajectoryTRR::_readNatoms(void)
{
    return read_trr_natoms((char *)_filename.c_str(), &_natoms);
}

bool TrajectoryTRR::nextFrame()
{
    _status = read_trr(_xdr, _natoms, &_step, &_time, &_lambda, _box, _x, _v, _f);
    if (_status != exdrOK) return false;
    
    for (int i=0; i<_natoms; ++i)
    {
        // Convert nm to Angstroms.
        _x[i][0] *= 10;
        _x[i][1] *= 10;
        _x[i][2] *= 10;
    }
    _nframes++;
    return true;
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark TrajectoryXTC

///////////////////////////////////////////////////////////////////////////////

bool TrajectoryXTC::nextFrame()
{
    _status = read_xtc(_xdr, _natoms, &_step, &_time, _box, _x, &_prec);
    if (_status != exdrOK) return false;
    for (int i=0; i<_natoms; ++i)
    {
        // Convert nm to Angstroms.
        _x[i][0] *= 10;
        _x[i][1] *= 10;
        _x[i][2] *= 10;
    }
    _nframes++;
    return true;
}

size_t TrajectoryXTC::_readNatoms(void)
{
    return read_xtc_natoms((char *)_filename.c_str(), &_natoms);
}

bool TrajectoryXTC::hasNextFrame() const
{ 
    return _status == exdrOK; 
}

void TrajectoryXTC::update(void)
{
    _xdr = xdrfile_open(_filename.c_str(), "r");
    if (not _xdr) throw FileNotFoundException(_filename);
    _status = _readNatoms();
    if (_status != exdrOK) throw EndOfFileException();
    _x = new rvec[_natoms];
}

TrajectoryXTC::~TrajectoryXTC()
{
    xdrfile_close(_xdr);
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark TrajectoryPDB

///////////////////////////////////////////////////////////////////////////////

bool TrajectoryPDB::nextFrame()
{
    std::string line;

    // Find the next ATOM line.
    // Return false if reached end-of-file.
    while (std::getline(*_tstream, line) and line.substr(0, 4) != "ATOM");
    if (_tstream->eof()) return false;

    // Read the next _natoms lines.
    int i = 0;
    while (i < _natoms)
    {
        if (line.substr(0, 4) == "ATOM")
        {
            from_string<float>(_x[i][0], line.substr(30, 8));
            from_string<float>(_x[i][1], line.substr(38, 8));
            from_string<float>(_x[i][2], line.substr(46, 8));
            i++;
        }
        std::getline(*_tstream, line);
    }
    _nframes++;
    _time++;
    return true;
}

size_t TrajectoryPDB::_readNatoms(void)
{
    std::string line;
    std::ifstream infile;
    utils::openread(_filename, infile);
    _natoms = 0;
    while (std::getline(infile, line) and line.substr(0, 3) != "END")
    {
        std::string rec (line.substr(0, 6));
        if (rec == "ATOM  " or rec == "HETATM") _natoms++;
    }
    if (_natoms == 0)
    {
        return exdrENDOFFILE;
    }
    return exdrOK;
}

bool TrajectoryPDB::hasNextFrame() const
{ 
    return not _tstream->eof(); 
}

void TrajectoryPDB::update(void)
{
    _tstream = new std::ifstream(_filename);
    if (not _tstream->is_open()) throw FileNotFoundException(_filename);
    _status = _readNatoms();
    if (_status == exdrENDOFFILE) throw EndOfFileException();
    _x = new rvec[_natoms];
}

TrajectoryPDB::~TrajectoryPDB()
{
    delete _tstream;
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark TrajectoryGRO

///////////////////////////////////////////////////////////////////////////////

bool TrajectoryGRO::nextFrame()
{
    std::string line;

    // Skip header and number of atom lines
    if (not std::getline(*_tstream, line)) return false;
    if (not std::getline(*_tstream, line)) return false ;
    
    for (int i=0; i<_natoms; ++i)
    {
        if (not std::getline(*_tstream, line))
        {
            std::cerr << "ERROR: trajectory frame " << i+1  << ": excepted " << _natoms;
            std::cerr << " atoms but reached end-of-file after " << i+1;
            std::cerr << " atoms." << std::endl;
            exit(1);
        }
        from_string<float>(_x[i][0], line.substr(20, 8));
        from_string<float>(_x[i][1], line.substr(28, 8));
        from_string<float>(_x[i][2], line.substr(36, 8));

        // Convert nm to Angstroms.
        _x[i][0] *= 10;
        _x[i][1] *= 10;
        _x[i][2] *= 10;
    }
    // Skip box line
    std::getline(*_tstream, line);

    _nframes++;
    _time++;
    return true;
}

size_t TrajectoryGRO::_readNatoms(void)
{
    std::string line;
    std::ifstream infile;
    utils::openread(_filename, infile);
    _natoms = 0;
    if (not std::getline(infile, line)) return exdrENDOFFILE;
    if (not std::getline(infile, line)) return exdrENDOFFILE;
    from_string<int>(_natoms, line);
    return exdrOK;
}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Trajectory

///////////////////////////////////////////////////////////////////////////////
Trajectory::~Trajectory()
{
    delete _x;
}


///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark TrajectoryBase

///////////////////////////////////////////////////////////////////////////////

TrajectoryBase::TrajectoryBase(): 
    _step(0), 
    _natoms(0), 
    _status(0), 
    _time(0.0), 
    _prec(1000.0),
    _nframes(0),
    _filename()
{}

///////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark Common functions

///////////////////////////////////////////////////////////////////////////////

Trajectory * newTrajectory(const std::string & trajname, const size_t natoms)
{
    Trajectory * traj = nullptr;
    std::string ext = trajname.substr(trajname.find_last_of("."));
    if(ext == ".xtc")
    {
        traj = new TrajectoryXTC();
    }
    else if (ext == ".trr")
    {
        traj = new TrajectoryTRR();
    }
    else if (ext == ".gro")
    {
        traj = new TrajectoryGRO();
    }    
    else if (ext == ".pdb")
    {
        traj = new TrajectoryPDB();
    }
    else
    {
        std::cerr << "ERROR: trajectory file: unrecognized extension '" << ext << "'." << std::endl;
        exit(1);        
    }
    
    traj->setFilename(trajname);
    
    // Exceptions could be thrown during initialization.
    try
    {
        traj->update();
    }
    catch (FileNotFoundException)
    {
        std::cerr << "ERROR: Cannot open " << traj->getFilename() << std::endl;
        exit(FILENOTFOUND);
    }
    catch(EndOfFileException)
    {
        std::cerr << "ERROR: Unable to read number of atoms in trajectory." << std::endl;
        exit(12);
    }
    
    // Sanitary check.
    
    if (traj->getNumberOfAtoms() == 0)
    {
        std::cerr << "ERROR: no atom read from trajectory." << std::endl;
        exit(1);
    }
    
    if (natoms != traj->getNumberOfAtoms())
    {
        std::cerr << "ERROR: the number of atoms in trajectory file (" << traj->getNumberOfAtoms() << ") ";
        std::cerr << "is different from the number of atoms in the coordinate ";
        std::cerr << "file (" << natoms << ")." << std::endl;
        exit(2);            
    }
    return traj;
}
