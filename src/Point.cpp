
#include "Point.hpp"
#include <sstream>


std::ostream& operator<<(std::ostream& os, const Point &pt)
{
    os << "Point(x=" << pt.x.x << ", y=" << pt.x.y << ", z=" << pt.x.z << ")";
    return os;
}


/*
 * Constructors
 */
Point::Point(): 
    x(9999.9, 9999.9, 9999.9),
    iscavity(false), 
    issurf(false),
    visited(false),
    isseed(false),
    neighbors()
{}

Point::Point(const Vec3f & v):
    x(v),
    iscavity(false),
    issurf(false),
    visited(false),
    isseed(false),
    neighbors()
{}

Point::Point(const float xx, const float yy, const float zz):
    x(xx, yy, zz),
    iscavity(false), 
    issurf(false),
    visited(false),
    isseed(false),
    neighbors()
{}

Point::Point(const Point &p):
    x(p.x),
    iscavity(p.iscavity), 
    issurf(p.issurf),
    visited(p.visited),
    isseed(p.isseed),
    neighbors(p.neighbors)
{}

Point::Point(const std::array<float, 3> & v):
    x(v),
    iscavity(false),
    issurf(false),
    visited(false),
    isseed(false),
    neighbors()
{}



// Description:
// Return a PDB formatted string representing the points informations.
std::string Point::topdb() const
{
    std::ostringstream oss;
    oss.setf(std::ios::fixed);
//    oss.width(6); oss << std::left << "HETATM" << std::right;
    oss.width(6); oss << std::left << "ATOM  " << std::right;
    oss.width(5); oss << 0 << " ";
    if(issurf)
    {
        oss.width(4); oss << "S"; // name (surface)
    }
    else
    {
        oss.width(4); oss << "O"; // name (cavity)
    }
    oss.width(1); oss << "";                                    // altloc
    oss.width(4); oss << std::left << "CAV" << std::right;
    oss.width(1); oss << "X";                                   // chain
    oss.width(4); oss << 0;                                     // resid
    oss.width(1); oss << "" << "   ";                           // icode
    oss.width(8); oss.precision(3); oss << x.x;
    oss.width(8); oss.precision(3); oss << x.y;
    oss.width(8); oss.precision(3); oss << x.z;
    oss.width(6); oss.precision(2); oss << 1.0;                 // occupancy
    oss.width(6); oss.precision(2); oss << 0.0;                 // b-factor
    oss << "          ";
    oss.width(2); oss << "";
    return oss.str();
}
