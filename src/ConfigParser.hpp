
#ifndef __CONFIGPARSER_H__
#define __CONFIGPARSER_H__

#include <exception>
#include <map>
#include <string>
#include <vector>



class NoSuchParameterException: public std::exception
{
    public:
        NoSuchParameterException(std::string param): _param(param) {}

        virtual const char* what() const throw()
        {
            std::string s = "No such parameter '";
            s += _param + "'";
            return s.c_str();
        }

    protected:
        std::string _param;
};

class NoSuchSectionException: public std::exception
{
    public:
        NoSuchSectionException(std::string sec): _sec(sec) {}

        virtual const char* what() const throw()
        {
            std::string s = "No such section '";
            s += _sec + "'";
            return s.c_str();
        }

    protected:
        std::string _sec;
};


struct ConfigParam
{
    ConfigParam(const std::string & name, const std::string & val):
        first(name), 
        second(val) 
    {}
    
    virtual ~ConfigParam() {}
    
    std::string first;
    std::string second;
};  


class ConfigSection
{
    public:
        ConfigSection():_name(), _parameters() {};
        ConfigSection(const std::vector<ConfigParam> & param):
            _name(), _parameters(param) {}
        virtual ~ConfigSection() {}
    
    
        inline std::string getName(void) const { return _name; } 
        inline void setName(const std::string n) { _name = n; }

        inline void clear(void) { _name = ""; _parameters.clear(); }

        void addParameter(const std::string name, const std::string value);
        std::string getParameter(const std::string name) const;

        inline size_t getNumberOfParameters(void) const 
        {
            return _parameters.size();
        }
    
        inline std::vector<ConfigParam> getParameters(void) const
        {
            return _parameters;
        }
    
    
    protected:
        std::string _name;
        std::vector<ConfigParam> _parameters;
};



class ConfigParser
{
    public:
        ConfigParser():_filename(), _sections(), _defaultSection() {}
        ConfigParser(std::string fn):_filename(fn), _sections(), _defaultSection() {}
        virtual ~ConfigParser() {}

        inline std::string getFilename(void) const { return _filename; }
        inline void setFilename(const std::string fn) { _filename = fn; }
        inline void setFilename(const char * fn) { _filename = std::string(fn); }

        inline size_t getNumberOfSections(void) const { return _sections.size(); }

        inline std::vector<ConfigSection> getSections(void) const { return _sections; }
        inline ConfigSection getSection(size_t i) const 
        {
            if (i >= _sections.size())
                throw "Not a valid section index";
            return _sections[i];
        }
        inline ConfigSection getSection(std::string name) const 
        {
            if (name == "DEFAULT" or name == "default")
            {
                return _defaultSection;
            }
            for (const ConfigSection & sec : _sections)
            {
                if (sec.getName() == name)
                {
                    return sec;
                }
            }
            throw NoSuchSectionException(name);
        }
        inline ConfigSection getDefaultSection(void) const
        { 
            return getSection("DEFAULT");
        }


        inline void addDefault(std::string p, std::string v) 
        {
            _defaultSection.addParameter(p, v);
        }
    
        void read();
        
        friend std::ostream& operator<<(std::ostream& os, const ConfigParser &cfg);

    protected:
        std::string _filename;
        std::vector<ConfigSection> _sections;
        ConfigSection _defaultSection;
};


#endif


