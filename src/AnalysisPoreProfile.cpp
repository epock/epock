

#include "AnalysisPoreProfile.hpp"
#include "Point.hpp"
#include "Vec3f.hpp"

#include <iomanip>  // std::setw, std::setprecision
#include <vector>   // std::vector


// TO BE REMOVED
#include <iostream>
using std::cout;
using std::endl;
// END TO BE REMOVED

AnalysisPoreProfile::AnalysisPoreProfile(Cavity & cav,
                                         Topology & top,
                                         Trajectory & traj):
    Analysis(cav, top, traj),
    _xtc(nullptr),
    _xxtc(nullptr),
    _stepxtc(0)
{
    _outputname = String::format("%s_profile.dat", _cavity.getName().c_str());
    _openOutputFile();
    *_output << std::setw(6) << std::setprecision(2);
    _writeHeader();
    _prerun();
}

AnalysisPoreProfile::~AnalysisPoreProfile()
{
    xdrfile_close(_xtc);
    delete _xxtc;
}

void AnalysisPoreProfile::_writeHeader(void)
{
    const auto & g = _cavity.getGridCavity();
    float gs = _cavity.getGridSpacing();
    float zmin = g.getOrigin()[2];
    float zmax = zmin + (g.dim()[2] - 1 + 0.5) * gs;
    for (float z=zmin; z<=zmax; z+=gs)
    {
        *_output << z << " ";
    }
    *_output << "\n";
}

void AnalysisPoreProfile::_prerun(void)
{
    _xtc = xdrfile_open((_cavity.getName()+"_center.xtc").c_str(), "w");
    _xxtc = new rvec[_cavity.getGridCavity().dim()[2]];
}


void AnalysisPoreProfile::_compute(void)
{
    *_output << _traj.getTime() << " ";

    const auto cavPoints = _cavity.getCavityPoints();
    const auto grid = _cavity.getGridCavity();
    const auto gridDim = grid.dim();
    const auto padding = _cavity.getPadding();

    // Will be used to keep surface points, indexed by z value.
    std::vector<std::vector<Point *>> zsurf(grid.dim()[2]);
    for (auto const p : cavPoints)
    {
        std::array<size_t, 3> pi = grid.nearestPointIndices(p->x.x, p->x.y, p->x.z);
        size_t i = pi[0];
        size_t j = pi[1];
        size_t k = pi[2];

        if (not grid(i-1, j, k) or
            not grid(i+1, j, k) or
            not grid(i, j-1, k) or
            not grid(i, j+1, k) or
            not grid(i, j, k-1) or
            not grid(i, j, k+1))
        {
            p->issurf = true;
            zsurf[k].push_back(p);
        }
        else
        {
          p->issurf = false;
        }
    } 

    // Calculate the pore center and radius for each z value.
    std::vector<Vec3f> porecenter(gridDim[2], Vec3f(-9999.0, -9999.0, -9999.0));
    std::vector<double> poreradius(gridDim[2], 0);
    for (auto  p: cavPoints)
    {
        if(p->issurf)
        {
            continue;
        }
        size_t k = grid.nearestPointIndices(p->x.x, p->x.y, p->x.z)[2];

        double radius = 1e8;
        for (auto s : zsurf[k])
        {
            double r = sqrt(p->dist2(*s))+padding;
            if (r < radius)
            {
                radius = r;
            }
        }
        if (radius > poreradius[k])
        {
            poreradius[k] = radius;
            porecenter[k] = p->x;
        }
    }
    
    // Find the last point that belong to the cavity.
    size_t kr = 0;
    for (size_t k = porecenter.size()-1; k < porecenter.size(); --k)
    {
        if (poreradius[k] > 0.01)
        {
            kr = k;
            break;
        }
    }

    // Count dummy points, when radius == 0.
    size_t dumpoints = 0;
    size_t lastpoint = 0;
    for (size_t k = 0; k < porecenter.size(); ++k)
    {
        if (poreradius[k] > 0.01)
        {
            _xxtc[lastpoint][0] = porecenter[k].x * 0.1;
            _xxtc[lastpoint][1] = porecenter[k].y * 0.1;
            _xxtc[lastpoint][2] = porecenter[k].z * 0.1;
            ++lastpoint;
        }
        else
        {
            ++dumpoints;
        }
        *_output << poreradius[k] << " ";
    }
    for (size_t i  = 0; i < dumpoints; ++i)
    {
        _xxtc[lastpoint+i][0] = porecenter[kr].x * 0.1;
        _xxtc[lastpoint+i][1] = porecenter[kr].y * 0.1;
        _xxtc[lastpoint+i][2] = porecenter[kr].z * 0.1;
    }
    *_output << "\n";

    // Write first frame as a PDB file.
    if (_traj.getNumberOfFrames() == 1)
    {
        std::ofstream pc(_cavity.getName()+"_center.pdb", std::ofstream::out);
        for(size_t i=0; i < gridDim[2]; ++i)
        {
          Point p(_xxtc[i][0]*10, _xxtc[i][1]*10, _xxtc[i][2]*10);
          pc << p.topdb() << "\n";
        }
    }

    // Write all frames in output XTC.
    matrix box = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
    write_xtc(_xtc, gridDim[2], _stepxtc, (float) _stepxtc, box, _xxtc, 1000.0);
    _stepxtc++;
}
