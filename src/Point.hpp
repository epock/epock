

#ifndef __POINT_H__
#define __POINT_H__


#include "Vec3f.hpp"

#include <array>
#include <iostream>
#include <vector>
#include <numeric>
#include <cmath>

struct Point {
        Point();
        Point(const Vec3f & v);
        Point(const float x, const float y, const float z);
        Point(const Point &p);
        Point(const std::array<float, 3> & v);

        virtual ~Point() {}
        
        inline float dist2(const float fx, const float fy, const float fz) const
        {
            return (fx - x.x) * (fx - x.x) + 
                   (fy - x.y) * (fy - x.y) + 
                   (fz - x.z) * (fz - x.z);
        }

        inline float dist2(const Point & p) const
        {
            return dist2(p.x.x, p.x.y, p.x.z);
        }
        
        inline float dist2(const Point * const p) const
        {
            return dist2(p->x.x, p->x.y, p->x.z);
        }

        inline float dist(const Point & p) const 
        {
            return sqrt(dist2(p));
        }
    
        friend std::ostream& operator<<(std::ostream& os, const Point &pt);

        std::string topdb() const;

        inline float getX() const { return x.x; }
        inline float getY() const { return x.y; }
        inline float getZ() const { return x.z; }

        inline void setX(const float f) { x.x = f; }
        inline void setY(const float f) { x.x = f; }
        inline void setZ(const float f) { x.x = f; }

        inline void setCoordinates(const Vec3f & v)
        {
            x.x = v.x; x.y = v.y; x.z = v.z;
        }
        inline void setCoordinates(const float thex, const float they, const float thez)
        {
            x.x = thex; x.y = they; x.z = thez;
        }

        Vec3f x;
        bool iscavity;
        bool issurf;
        bool visited;
        bool isseed;

        std::vector<Point *> neighbors;

};

namespace PointSort
{
    inline bool z_less_than (const Point & p1, const Point & p2)
    {
        return p1.x.z < p2.x.z;
    }
}


// Description:
// Calculate the boundaries of a set of points in each dimension.
template <typename T>
std::array<double, 6> points_boundaries(const std::vector<T> & points)
{
    std::array<double, 6> b={0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    
    if (points.size())
    {
        double minx =  points[0].x.x, miny = points[0].x.y , minz = points[0].x.z;
        double maxx =  points[0].x.x, maxy = points[0].x.y , maxz = points[0].x.z;
           
        for (const Point & pt : points)
        {
            if (pt.x.x < minx) minx = pt.x.x;
            if (pt.x.x > maxx) maxx = pt.x.x;
            if (pt.x.y < miny) miny = pt.x.y;
            if (pt.x.y > maxy) maxy = pt.x.y;
            if (pt.x.z < minz) minz = pt.x.z;
            if (pt.x.z > maxz) maxz = pt.x.z;
        }
        b[0] = minx;
        b[1] = maxx;
        b[2] = miny;
        b[3] = maxy;
        b[4] = minz;
        b[5] = maxz;
    }
    return b;
}

// Description:
// Calculate the boundaries of a set of points in each dimension.
template <typename T>
std::array<double, 6> points_boundaries(const std::vector<T *> & points)
{
    std::array<double, 6> b={0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    if (points.size())
    {
        double minx =  points[0]->x.x, miny = points[0]->x.y , minz = points[0]->x.z;
        double maxx =  points[0]->x.x, maxy = points[0]->x.y , maxz = points[0]->x.z;

        for (const T * const pt : points)
        {
            if (pt->x.x < minx) minx = pt->x.x;
            if (pt->x.x > maxx) maxx = pt->x.x;
            if (pt->x.y < miny) miny = pt->x.y;
            if (pt->x.y > maxy) maxy = pt->x.y;
            if (pt->x.z < minz) minz = pt->x.z;
            if (pt->x.z > maxz) maxz = pt->x.z;
        }
        b[0] = minx; 
        b[1] = maxx;
        b[2] = miny; 
        b[3] = maxy;
        b[4] = minz; 
        b[5] = maxz;
    }
    return b;
}



#endif
