
//
// Some functions for topology files I/O.
//

#ifndef __TOPOLOGY_H__
#define __TOPOLOGY_H__

#include "Atom.hpp"

#include <map>
#include <string>
#include <vector>


static const std::map<std::string, float> VDW_RADII = 
{
    {"H", 1.20},
    {"C", 1.70},
    {"N", 1.55},
    {"O", 1.52},
    {"F", 1.47},
    {"P", 1.80},
    {"S", 1.80},
};



class Topology
{
    public:
        Topology();
        Topology(std::string fname);
    
        // Description:
        // Get/Set a topology file name.
        inline void setFilename(const std::string fn) { _fname = fn; }
        inline std::string getFilename(void) { return _fname; }

        // Description:
        // Get the number of atoms read a topology contains.
        inline size_t getNumberOfAtoms(void) { return _atoms.size(); }
        
        // Description:
        // Get references to a topology atom list.
        inline std::vector<Atom> & getAtoms() { return _atoms; }
        inline const std::vector<Atom> & getAtoms() const { return _atoms; }
        
        // Description:
        // Read topology files and push atoms into the topology internal atom
        // list.
        // read(void) guesses the file type based on its extension.
        void read(void);
        static void readPDB(const std::string fname, std::vector<Atom> & atoms);
        static void readGRO(const std::string fname, std::vector<Atom> & atoms);
        static void readPDB(const std::string fname, std::vector<Point> & points);

        // Description:
        // Set atom radii.
        void setRadii(const float & f);
        void setRadii(const std::vector<float> & v);
        void setRadii(const std::string & fname);
    
        static std::map<std::string, float> radiiMap;
    
    protected:
        std::string _fname;
        std::vector<Atom> _atoms;
};


#endif

