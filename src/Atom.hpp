

#ifndef __ATOM_H__
#define __ATOM_H__


#include "Point.hpp"
#include <iostream>
#include <string>

struct Atom:public Point
{
    Atom();
    virtual ~Atom() {}
    
    std::string name;
    std::string resname;
    std::string element;
    std::string chain;
    int         index;
    int         resid;
    float       vdw;
    size_t      serial;
    
    friend std::ostream& operator<<(std::ostream& os, const Atom &a);   
    std::string topdb() const;
    std::string getIdentifier(const bool & withAtomName = true) const;
    
    inline size_t getSerial(void) const { return serial; }
};


#endif


