
#ifndef __CAVITY_H__
#define __CAVITY_H__

#include "Atom.hpp"
#include "Grid.hpp"
#include "Point.hpp"
#include "Region.hpp"
#include "xdrfile/src/xdrfile.h"

#include <fstream>
#include <map>
#include <string>
#include <vector>

float volumeSetAtoms (const std::vector<Atom> points, 
                      const double prec);
float volumeSetSpheres (const std::vector<Point *> points, 
                        const double radius, 
                        const double prec);

enum CountContrib
{
    countNone,
    countAtomContrib,
    countResidueContrib
};


class Cavity
{
    public:
        Cavity();
        Cavity(const Cavity &) = delete;
        Cavity & operator=(const Cavity &) = delete;
        virtual ~Cavity();

        inline void setName(const std::string & name) { _name = name; }
        inline std::string getName(void) const { return _name; }
    
        inline float getVolume(void) const { return _volume; }
            
        inline float getGridSpacing(void) const { return _gridSpacing; }
        inline void setGridSpacing(const float f) { _gridSpacing = f; }
    
        inline float getPadding(void) const { return _padding; }
        inline void setPadding(const float f) { _padding = f; }
    
        inline float getPrecision(void) const { return _precision; }
        inline void setPrecision(const float f) { _precision = f; }
        
        inline unsigned getNumberOfIncludeRegions(void) const { return _includeRegions.size(); }
        inline unsigned getNumberOfExcludeRegions(void) const { return _excludeRegions.size(); }
        
        inline size_t getCountContrib(void) const { return _countContrib; }
        inline void setCountContrib(const size_t u) { _countContrib = u; }
    
        inline void setSeedCutoff(const float cutoff) { _seedCutoffSq = cutoff * cutoff; }
        inline float getSeedCutoffSq(void) { return _seedCutoffSq; }
    
        inline bool getUseContig(void) const { return _useContig; }
        inline bool useContig(void) const { return _useContig; }
        inline void setUseContig(const bool b) { _useContig = b; }

        inline void setUsePoreProfile(const bool b) { _usePoreProfile = b; }
        inline bool getUsePoreProfile(void) const { return _usePoreProfile; }
        inline bool usePoreProfile(void) const { return _usePoreProfile; }
        inline void usePoreProfile(const bool b) { setUsePoreProfile(b); }

        inline Grid3D<Point *> getGridCavity(void) const { return _gridCavity; }
        
        inline std::vector<Point *> getCavityPoints(void) const { return _cavityPoints; } 

        inline const std::vector<Point> & getAllPoints(void) const { return _allPoints; }
        inline size_t getNumberOfPoints(void) const { return _allPoints.size(); }


        void setMolecule(std::vector<Atom> *atoms);
        void setCoordinates(const std::vector<Atom> &atoms);
    
        void init();
        void initIncludeRegions();
        void initGrid();
        void initPDBRegion();
        void initAtomContrib(size_t natoms);
    
        inline void addIncludeRegion(Region * const r)
        {
            _includeRegions.push_back(r);
        }

        inline void addExcludeRegion(Region * const r)
        {
            _excludeRegions.push_back(r);
        }
    
        inline void addSeedRegion(Region * const r)
        {
            _seedRegions.push_back(r);
        }
    
        float calculate();
        void calculatePoints();
    
        void ir2pdb(const std::string & fname) const;
        void ir2pdb() const;

        void writePDB(const std::string & fname) const;
        void writePDB() const;
        void write() const;
    
        void writeContrib(const std::string & fname);
        void writeContrib();
    
        void removeIsolatedPoints();
    
        void setGrid(const std::string & fname);
        bool getUseExternalGrid(void) const { return _useExternalGrid; }
        void setUseExternalGrid(const bool b) { _useExternalGrid = b; }
        void useExternalGrid(const bool b) { _useExternalGrid = b; }
        bool useExternalGrid() { return _useExternalGrid; }

    protected:
        std::string             _name;
    
        float                   _gridSpacing;
        float                   _padding;
        float                   _precision;
        float                   _volume;
        
        bool                    _useExternalGrid;
    
        std::vector<Region *>   _includeRegions;
        std::vector<Region *>   _excludeRegions;
        std::vector<Region *>   _seedRegions;
        std::vector<Point>      _allPoints;
        std::vector<Point *>    _cavityPoints;
        Grid3D<Point *>         _gridCavity;
        std::vector<Atom>       _coor;
        std::vector<Atom>       *_mol;
        BoxRegion               _pdbRegion;
        
        // Description:
        // Maps an atom identifier with its contribution to the atoms volume
        // along time.
        // It is set once the protein coordinates have been set.
        std::vector<std::vector<size_t>> _atomContrib;
        size_t _countContrib;
    
        float                   _seedCutoffSq;
        bool                    _useContig;
        size_t                  _nFrames;

        bool                    _usePoreProfile;
    
        void _writeResidueContrib(const std::string & fname);
        void _writeAtomContrib(const std::string & fname);

        void _initNeighbors(void);
        void _initSeed();
        

        void _findContig(Point * seedPoint);

    private:
};


#endif
