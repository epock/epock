

#include "AnalysisPathSearch.hpp"
#include "Vec3f.hpp"

#include <iomanip>      // std::setw


static bool pathExists(const Cavity & start, const Cavity & goal);


AnalysisPathSearch::AnalysisPathSearch(Cavity & cav,
                                       Cavity & target,
                                       Topology & top,
                                       Trajectory & traj):
    Analysis(cav, top, traj),
    _target(target)
{
    _outputname = String::format("path_%s_%s.dat", 
                                _cavity.getName().c_str(), 
                                _target.getName().c_str());

    _openOutputFile();
    _writeHeader();
}

AnalysisPathSearch::~AnalysisPathSearch()
{
}


void AnalysisPathSearch::_writeHeader(void)
{
    *_output << "      Time pathExists\n";
}


void AnalysisPathSearch::_compute(void)
{
    *_output << std::setw(10) << std::setprecision(2) << _traj.getTime() << " ";
    *_output << pathExists(_cavity, _target) << "\n";
}


///////////////////////// The A start implementation /////////////////////////
static bool pathExists(const Cavity & start, const Cavity & goal)
{
    class Astar
    {
        public:
            Astar(const Vec3f start, const Vec3f goal, const std::vector<Vec3f> points)
            {
                float cutoff = 9.0;
                std::vector<Vec3f> closedset;
                std::vector<Vec3f> openset;
                std::map<Vec3f, Vec3f> camefrom;

                std::map<Vec3f, float> gscore;
                std::map<Vec3f, float> fscore;
                
                openset.push_back(start);
                gscore[start] = 0.0;
                fscore[start] = start.dist2(goal);
                
                while (not openset.empty())
                {
                    Vec3f current = nodeHavingLowestFScore(openset, fscore);
                    if (current == goal)
                    {
                        _pathExists = true;
                        break;
                    }
                    closedset.push_back(current);

                    for (auto neighbor:neighborsNodes(current, points, cutoff))
                    {
                        float tentativeGScore = gscore[current] + current.dist2(neighbor);
                        bool neighborInClosedSet = find(closedset.begin(), closedset.end(), neighbor) != closedset.end();
                        if (neighborInClosedSet and tentativeGScore >= gscore[neighbor])
                        {
                            continue;
                        }
                        else if ((not neighborInClosedSet) or tentativeGScore < gscore[neighbor])
                        {
                            camefrom[neighbor] = current;
                            gscore[neighbor] = tentativeGScore;
                            fscore[neighbor] = gscore[neighbor] + neighbor.dist2(goal);
                            bool neighborInOpenSet = find(openset.begin(), openset.end(), neighbor) != openset.end();
                            if (not neighborInOpenSet)
                            {
                                openset.push_back(neighbor);
                            }
                        }
                    }
                }
            }

            bool pathExists(void) const { return _pathExists; }

        protected:
            // Return the node from openset having the lowest fscore and
            // remove it from openset.
            Vec3f nodeHavingLowestFScore(std::vector<Vec3f> & openset, 
                                         std::map<Vec3f, float> fscore)
            {
                Vec3f minnode = openset[0];
                float minscore = fscore[minnode];
                size_t minid = 0;
                for (size_t i=1; i < openset.size(); ++i)
                {
                    Vec3f node = openset[i];
                    float score = fscore[node];
                    if (score < minscore)
                    {
                        minscore = score;
                        minnode = node;
                        minid = i;
                    }
                }
                openset.erase(openset.begin() + minid);
                return minnode;
            }

            // Return the list of nodes within within cutoff of n.
            std::vector<Vec3f> neighborsNodes(const Vec3f & n, 
                                              const std::vector<Vec3f> points, 
                                              float cutoff)
            {
                float cutoff2 = cutoff * cutoff;
                std::vector<Vec3f> neighbors;   
                for (auto p:points) 
                {
                    if (n.dist2(p) <= cutoff2) 
                        neighbors.push_back(p);
                }
                return neighbors;
            }

            bool _pathExists = false;
    };

    // To run A*, we need a start and a goal.
    // For simplicity, we will use a single start and a single point,
    // basically random points form the current cavity and the target cavity.
    // So this function should only be used with the contiguous option.

    std::vector<Vec3f> points;
    for (auto p:start.getCavityPoints())
    { 
        if (p->iscavity) points.push_back(p->x);
    }
    for (auto p:goal.getCavityPoints())
    { 
        if (p->iscavity) points.push_back(p->x);
    }
    Vec3f beg = points[0];
    Vec3f end = points[points.size() - 1];

    return Astar(beg, end, points).pathExists();
}
