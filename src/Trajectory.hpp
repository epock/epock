

#ifndef __TRAJECTORY_HPP__
#define __TRAJECTORY_HPP__

#include "xdrfile.h"
#include "xdrfile_xtc.h"
#include "xdrfile_trr.h"

#include <fstream>
#include <string>



enum ERRORS
{
    FILENOTFOUND,
    XTCERROR
};

class TrajectoryBase
{
    public:
        TrajectoryBase();
        virtual ~TrajectoryBase() {}
    
        inline int getStep(void) const { return _step; }
        inline size_t getNumberOfAtoms(void) const { return (size_t)_natoms; }
        inline int getStatus(void) const { return _status; }
        inline float getTime(void) const { return _time; }
        inline float getPrecision(void) const { return _prec; }
        inline size_t getNumberOfFrames(void) const { return _nframes; }
    
        inline std::string getFilename(void) const { return _filename; }
        inline void setFilename(const std::string s) { _filename = s; }
    
        // Description:
        // Read next trajectory frame.
        // Return true is everything went well.
        virtual bool nextFrame() = 0;
        virtual bool hasNextFrame() const = 0;
        virtual void update(void) = 0;

    
    protected:
        int     _step;
        int     _natoms;
        int     _status;
        float   _time;
        float   _prec;
        size_t  _nframes;
        std::string _filename;
};

class Trajectory : public TrajectoryBase
{
    public:
        Trajectory():_x(nullptr) {}
        Trajectory(const Trajectory & ) = delete;
        Trajectory & operator=(const Trajectory & t) = delete;
        virtual ~Trajectory();
    
        inline float getXAt(const size_t i) const { return _x[i][0]; }
        inline float getYAt(const size_t i) const { return _x[i][1]; }
        inline float getZAt(const size_t i) const { return _x[i][2]; }
    
        
    protected:
        rvec * _x;
        
        virtual size_t  _readNatoms(void) = 0;

};

class TrajectoryXTC : public Trajectory
{
    public:
        TrajectoryXTC():_xdr(nullptr), _box() {}
        TrajectoryXTC(const TrajectoryXTC & ) = delete;
        TrajectoryXTC & operator=(const TrajectoryXTC & t) = delete;
        ~TrajectoryXTC();
    
        bool nextFrame();
        bool hasNextFrame() const;
        void update(void);
    
    protected:
        XDRFILE *   _xdr;
        matrix      _box;
        
        size_t _readNatoms(void);

};

class TrajectoryTRR : public TrajectoryXTC
{
    public:
        bool nextFrame();
        void update(void);
    
    protected:
        float _lambda = 0.0;
        rvec * _v = nullptr;
        rvec * _f = nullptr;
    
        size_t _readNatoms(void);
};


class TrajectoryPDB : public Trajectory
{
    public:
        TrajectoryPDB():_tstream(nullptr) {}
        TrajectoryPDB(const TrajectoryPDB & ) = delete;
        TrajectoryPDB & operator=(const TrajectoryPDB & t) = delete;
        ~TrajectoryPDB();
    
        bool nextFrame();
        bool hasNextFrame() const;
        void update(void);
        
        size_t getNumberOfFrames() const { return _nframes; };
    
    protected:
        std::ifstream * _tstream;
    
        size_t _readNatoms(void);
    
};

class TrajectoryGRO : public TrajectoryPDB
{
    public:
        bool nextFrame();
    
    protected:
        size_t _readNatoms(void);
};


// Description:
// Initialize the trajectory: open it and perform some sanitaty checks.
// Exit on faillure.
Trajectory * newTrajectory(const std::string & trajname, const size_t natoms);


#endif

