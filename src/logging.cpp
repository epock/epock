
#include "logging.hpp"

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>

#include <unistd.h>     // isatty

#ifdef EXECUTABLE
static const std::string PREFIX=EXECUTABLE;
#else
static const std::string PREFIX="";
#endif

static const std::vector<std::string> terminalVT100Names =
{
  "Eterm",
  "ansi",
  "color-xterm",
  "con132x25",
  "con132x30",
  "con132x43",
  "con132x60",
  "con80x25",
  "con80x28",
  "con80x30",
  "con80x43",
  "con80x50",
  "con80x60",
  "cons25",
  "console",
  "cygwin",
  "dtterm",
  "eterm-color",
  "gnome",
  "gnome-256color",
  "konsole",
  "konsole-256color",
  "kterm",
  "linux",
  "msys",
  "linux-c",
  "mach-color",
  "mlterm",
  "putty",
  "rxvt",
  "rxvt-256color",
  "rxvt-cygwin",
  "rxvt-cygwin-native",
  "rxvt-unicode",
  "rxvt-unicode-256color",
  "screen",
  "screen-256color",
  "screen-256color-bce",
  "screen-bce",
  "screen-w",
  "screen.linux",
  "vt100",
  "xterm",
  "xterm-16color",
  "xterm-256color",
  "xterm-88color",
  "xterm-color",
  "xterm-debian"
};


#include <time.h>

bool terminalIsVT100(FILE * stream);
static std::string timeNow(void)
{
    char buf[80];
    time_t now = time(0);
    struct tm tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%T", &tstruct);
    return buf;
}

using namespace std;

namespace logging
{
    static Logger * defaultLogger = new Logger();

    void progress(const std::string & s) { defaultLogger->progress(s); }
    void progress_done() { defaultLogger->print(""); }

    void log(const std::string & s, size_t level) { defaultLogger->log(s, level); }
    void info(const std::string & s) { defaultLogger->info(s); }
    void warn(const std::string & s) { defaultLogger->warn(s); }
    void warning(const std::string & s) { defaultLogger->warn(s); }
    void error(const std::string & s) { defaultLogger->error(s); }
    void critical(const std::string & s, size_t status) { defaultLogger->critical(s, status); }
    void fatal(const std::string & s, size_t status) { critical(s, status); }
    void debug(const std::string & s, size_t level) { defaultLogger->debug(s, level); }

    void setLevel(size_t level) { defaultLogger->setLevel(level); }

    void setLogFilename(const std::string & fname)
    {
        delete defaultLogger;
        defaultLogger = new Logger(fname);
    }

    void setEOL(std::string eol)
    {
        defaultLogger->setEOL(eol);
    }

    void shutdown(void)
    {
        delete defaultLogger;
    }


    /////////////////////////// The Logger Class //////////////////////////////
    Logger::Logger():
        _os(&std::cerr),
        _streamType(logSTDERR),
        _level(logWARNING),
        _prefix(PREFIX),
        _eol("\n"),
        _useColors(false)
    {
        if (not _prefix.empty())
            _prefix += ") ";
    }

    Logger::Logger(std::string fname):
        _os(),
        _streamType(logOFSTREAM),
        _level(logWARNING),
        _prefix(PREFIX),
        _eol("\n"),
        _useColors(false)
    {
        if (not _prefix.empty())
            _prefix += ") ";
        _os = new std::ofstream(fname, std::ofstream::out);
    }

    Logger::Logger(const Logger & l):
        _os(l._os),
        _streamType(l._streamType),
        _level(l._level),
        _prefix(l._prefix),
        _eol(l._eol),
        _useColors(l._useColors)
    {
    }
    
    Logger::~Logger()
    {
        close();
    }



    Logger & Logger::operator=(const Logger & l)
    {
        _os = l._os;
        _streamType = l._streamType;
        _level = l._level;
        _prefix = l._prefix;
        _eol = l._eol;
        _useColors = l._useColors;
        return *this;
    }

    void Logger::close()
    {
        if (not (_streamType == logSTDOUT or _streamType == logSTDERR))
        {
            if (_os != nullptr)
            {
                delete _os;
                _os = nullptr;  
            }
        }
    }

    void Logger::colorize(std::string s, std::string color) const
    {
        *_os << TTYCOLORS[color] << s << TTYCOLORS["CLEAR"] << TTYCOLORS["WHITE"];
    }
    
    void Logger::log(const std::string & s, size_t level) const
    {
        if (level >= _level)
        {
            *_os << _prefix;
            *_os << timeNow() + " - ";
            if (_useColors)
            {
                colorize(LEVELNAMES[level] + ": ", LEVELCOLORS[level]);
            }
            else
            {
                *_os << LEVELNAMES[level] << ": ";
            }
            *_os << s << _eol;
            _os->flush();
        }
    }

    void Logger::progress(const std::string & s) const
    {
        *_os << "\r";
        *_os << _prefix;
        *_os << timeNow() + " - ";
        *_os << "PROGRESS: ";
        *_os << s;
    }

    void Logger::info(const std::string & s) const
    {
        log(s, logINFO);
    }

    void Logger::warn(const std::string & s) const
    {
        log(s, logWARNING);
    }

    void Logger::warning(const std::string & s) const
    {
        log(s, logWARNING);
    }

    void Logger::error(const std::string & s) const
    {
        log(s, logERROR);
    }

    void Logger::critical(const std::string & s, size_t status=1)
    {
        log(s, logCRITICAL);
        close();
        exit(status);
    }

    void Logger::debug(const std::string & s, size_t level) const
    {
        log(s, level);
    }

    void Logger::print(const std::string & s) const
    {
        *_os << s << _eol;
    }
}



bool terminalIsVT100(FILE * stream)
{
    // Check for a valid terminal.
    std::string term = getenv("TERM");
    auto it = std::find(terminalVT100Names.begin(), terminalVT100Names.end(), term);
    if (it == terminalVT100Names.end())
    {
        return false;
    }

    // Make sure stream  if a tty.
    return isatty(fileno(stream)) ;
}

