

#ifndef __REGION_H__
#define __REGION_H__

#include "Point.hpp"
#include "Vec3f.hpp"

#include <array>

static float const PI =  3.141592653589793;
static float const PI43 = PI * 4.0 / 3.0;


class Region
{
    public:
        Region() {} 
        virtual ~Region() {}

        /* Virtual methods to be overwritten in child classes */

        // Fill b with the region boundaries.
        virtual void boundaries(std::array<float, 6> &b) const = 0;

        // Return if the region and a sphere (a point with a radius) 
        // intersect each other.
        virtual bool intersects(const Point &p, const float padding) const = 0;

        // Return if a point is inside a region.
        bool contains(const Point & p) const
        {
            return intersects(p, 0.0);
        }
};


class SphereRegion:public Region
{
    public:
        SphereRegion():
            _radius(0.0),
            _center(Vec3f(0.0, 0.0, 0.0))
        {}
        SphereRegion(const Vec3f &center):
            _radius(0.0),
            _center(center)
            {}
        SphereRegion(const Vec3f &center, const float radius):
            _radius(radius),
            _center(center)
        {}  
        SphereRegion(const float x, const float y, const float z):
            _radius(0.0),
            _center(Vec3f(x, y, z))
            {}
        SphereRegion(const float x, const float y, const float z, const float r):
            _radius(r),
            _center(Vec3f(x, y, z))
            {}

        inline float getRadius() const { return _radius; }
        inline void setRadius(const float r) { _radius = r; }

        inline void setCenter(const Vec3f &p) { _center = p; }
        inline void setCenter(const float x, const float y, const float z) { _center = Vec3f(x, y, z); }
        inline Vec3f getCenter() const { return _center; }

        void boundaries(std::array<float, 6> &b) const;
        bool intersects(const Point &p, const float padding) const;

    protected:
        float _radius;
        Vec3f _center;
};


class BoxRegion: public Region
{
    public:
        BoxRegion():
            _dim(Vec3f(0.0, 0.0, 0.0)),
            _center(Vec3f(0.0, 0.0, 0.0))
        {}
        BoxRegion(const Vec3f &center):
            _dim(Vec3f(0.0, 0.0, 0.0)),
            _center(center)
        {}
        BoxRegion(const float x, const float y, const float z):
            _dim(Vec3f(0.0, 0.0, 0.0)),
            _center(Vec3f(x, y, z))
        {}
        BoxRegion(const Vec3f &center, const Vec3f dim):
            _dim(dim),
            _center(center)
        {}

        inline Vec3f getDim() const { return _dim; }
        inline void setDim(const Vec3f &p) { _dim = p; }
        inline void setDim(const float x, const float y, const float z) { _dim = Vec3f(x,y,z); }

        inline void setCenter(const Vec3f &p) { _center = p; }
        inline void setCenter(const float x, const float y, const float z) { _center = Vec3f(x, y, z); }
        inline Vec3f getCenter() const { return _center; }
        
        void boundaries(std::array<float, 6> &b) const;
        bool intersects(const Point &p, const float padding) const;


    protected:
        Vec3f _dim;
        Vec3f _center;
};


class CylinderRegion: public Region
{
    public:
        CylinderRegion():
            _x1(Point(0.0, 0.0, 0.0)),
            _x2(Point(0.0, 0.0, 0.0)),
            _radius(0.0)
        {}

        CylinderRegion(const Point p1, const Point p2, const float radius):
            _x1(p1),
            _x2(p2),
            _radius(radius)
        {}

        CylinderRegion(const float x1, const float y1, const float z1,
                       const float x2, const float y2, const float z2,
                       const float radius):
            _x1(Point(x1, y1, z1)),
            _x2(Point(x2, y2, z2)),
            _radius(radius)
        {}


        void boundaries(std::array<float, 6> &b) const;

        inline Point getX1(void) const { return _x1; }
        inline Point getX2(void) const { return _x2; }
        inline float getRadius(void) const { return _radius; }

        inline void setX1(const Point & x) { _x1 = x; }
        inline void setX2(const Point & x) { _x2 = x; }
        inline void setRadius(const float r) { _radius = r; }
    
        bool intersects(const Point &p, const float padding) const;


    protected:
        Point _x1;
        Point _x2;
        float _radius;
};

#endif
