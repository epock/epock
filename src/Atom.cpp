

#include "Atom.hpp"
#include "utils.hpp"

#include <iostream>
#include <sstream>


static size_t atomSerial = 0;


Atom::Atom():
    name("X"),
    resname("XXX"),
    element("X"),
    chain(""),
    index(0),
    resid(0),
    vdw(-1.0),
    serial(atomSerial++)
{}


// Description:
// Return a identifier chain:resname:resid:atom
std::string Atom::getIdentifier(const bool & withAtomName) const
{
    std::string s;
    if (chain != "" and String::trim(chain) != "") s += chain + ":";
    s += resname + ":" + std::to_string(resid);
    if (withAtomName)
    {
        s += + ":" + name;
    }
    return s;
}


// Description:
// Return a PDB formatted string.
std::string Atom::topdb() const
{
    std::ostringstream oss;
    oss.setf(std::ios::fixed);
    oss.width(6); oss << std::left << "ATOM" << std::right;
    oss.width(5); oss << index << " ";
    oss.width(4); oss << name;
    oss.width(1); oss << "";                                    // altloc
    oss.width(4); oss << std::left << resname << std::right;
    oss.width(1); oss << chain;                                 // chain
    oss.width(4); oss << resid;                                 // resid
    oss.width(1); oss << "" << "   ";                           // icode
    oss.width(8); oss.precision(3); oss << x.x;
    oss.width(8); oss.precision(3); oss << x.y;
    oss.width(8); oss.precision(3); oss << x.z;
    oss.width(6); oss.precision(2); oss << 1.0;                 // occupancy
    oss.width(6); oss.precision(2); oss << 0.0;                 // b-factor
    oss << "          ";
    oss.width(2); oss << element;
    return oss.str();
}


// Description:
// Print function.
std::ostream& operator<<(std::ostream& os, const Atom &a)
{
    os << "Atom(";
    os << "index=" << a.index;
    os << ", name=\"" << a.name << "\"";
    os << ", element=\"" << a.element << "\"";
    os << ", resname=\"" << a.resname << "\"";
    os << ", x=" << a.x.x;
    os << ", y=" << a.x.y;
    os << ", z=" << a.x.z;
    os << ", vdw=" << a.vdw;
    os << ")";
    return os;
}

