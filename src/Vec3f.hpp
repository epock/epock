#ifndef __VEC3F_H__
#define __VEC3F_H__

#include <array>
#include <cmath>
#include <iostream>

struct Vec3f
{
    Vec3f(): x(0.0), y(0.0), z(0.0) {}
    Vec3f(const float xx, const float yy, const float zz): x(xx), y(yy), z(zz) {}
    Vec3f(const Vec3f & v): x(v.x), y(v.y), z(v.z) {}
    Vec3f(const std::array<float, 3> & v): x(v[0]), y(v[1]), z(v[2]) {}
    
    // Description:
    // Access to the vector's dimensions by index.
    inline float operator [] (const size_t i) const { return *(&x+i); }
    inline float & operator [] (const size_t i) { return *(&x+i); }

    // Description:
    // Print a representation of the vector.
    friend std::ostream& operator<<(std::ostream& os, const Vec3f & v)
    {
        os << "Vec3f(x=" << v.x << ", y=" << v.y << ", z=" << v.z << ")";
        return os;
    }


#pragma mark -
#pragma mark Operator overrides
    bool operator== (const Vec3f & other) const
    {
        bool a = abs(x - other.x) < 1e-6;
        bool b = abs(y - other.y) < 1e-6;
        bool c = abs(z - other.z) < 1e-6;
        return (a and b and c);
    }

    bool operator!= (const Vec3f & other) const
    {
        return not (*this == other);
    }

    bool operator< (const Vec3f & other) const
    {
        float a = (x - other.x);
        float b = (y - other.y);
        float c = (z - other.z);
        return (a < 0 or b < 0 or c < 0);
    }


#pragma mark -
#pragma mark Linear algebra methods

    // Description:
    // Vector addition.
    inline Vec3f operator + (const Vec3f & v) const
    {
        return Vec3f(x + v.x, y + v.y, z + v.z);
    }
    
    // Description:
    // Vector substraction.
    inline Vec3f operator - (const Vec3f & v) const
    {
        return Vec3f(x - v.x, y - v.y, z - v.z);
    }
    
    // Description:
    // Vector dot product.
    inline float dot(const Vec3f & v) const
    {
        return x * v.x + y * v.y + z * v.z;
    }
    
    // Description:
    // Vector cross product.
    inline Vec3f cross(const Vec3f & v) const
    {
        // a2b3 - a3b2, a3b1 - a1b3, a1b2 - a2b1
        float a = y * v.z - z * v.y;
        float b = z * v.x - x * v.z;
        float c = x * v.y - y * v.x;
        return Vec3f(a, b, c);
    }
    
    // Description:
    // Squared vector length.
    inline float length2(void) const
    {
        return (x * x + y * y + z * z);
    }

    // Description:
    // Vector length.
    inline float length(void) const
    {
        return std::sqrt(length2());
    }
    
    // Description:
    // Returns the vector of length 1 directed along v.
    inline Vec3f norm(void) const
    {
        float invn = 1 / length();
        return Vec3f(x * invn, y * invn, z * invn);
    }
    
    inline float dist2(const Vec3f & v) const
    {
        return (v.x - x) * (v.x - x) + \
               (v.y - y) * (v.y - y) + \
               (v.z - z) * (v.z - z);
    }


    float x, y, z;
};


#endif
