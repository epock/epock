

#ifndef __ANALYSIS_CAVITY_TRAJ_HPP__
#define __ANALYSIS_CAVITY_TRAJ_HPP__

#include "Analysis.hpp"
#include "xdrfile.h"

class AnalysisCavityTraj : public Analysis
{
    public:
        AnalysisCavityTraj(Cavity & cav,
                            Topology & top, 
                            Trajectory & traj);

        ~AnalysisCavityTraj();

        AnalysisCavityTraj(const AnalysisCavityTraj & a) = delete;
        AnalysisCavityTraj & operator=(const AnalysisCavityTraj & a) = delete;

    protected:
        void _prerun(void);
        void _compute(void);
        void _writeHeader(void);

        XDRFILE * _xtc;
        rvec    * _xxtc;
        size_t    _stepxtc;
};


#endif