#ifndef __GRID_H__
#define __GRID_H__

#include "utils.hpp"

#include <array>
#include <algorithm>
#include <cmath>
#include <cstdarg>
#include <string>
#include <vector>


template<typename T>
class Grid3D
{
    public:
        Grid3D(size_t x = 0, size_t y = 0, size_t z = 0, T _def = T()):
            _grid(x*y*z, _def), 
            _x(x), 
            _y(y), 
            _z(z), 
            _origin(), 
            _dx(1)
        {
            _origin[0] = 0.0;
            _origin[1] = 0.0;
            _origin[2] = 0.0;
        }
    
        Grid3D(const std::array<double, 6> & bounds, const double & dx, T _def = T()):
            _grid(0, _def),
            _x(0),
            _y(0),
            _z(0),
            _origin(), 
            _dx(dx)
        {
            _origin[0] = bounds[0];
            _origin[1] = bounds[2];
            _origin[2] = bounds[4];        
            _x = size_t((bounds[1] - bounds[0]) / _dx) + 1;
            _y = size_t((bounds[3] - bounds[2]) / _dx) + 1;
            _z = size_t((bounds[5] - bounds[4]) / _dx) + 1;
            _grid.resize(_x*_y*_z);
            _grid.assign(_x*_y*_z, _def);
        }

        inline size_t size(void) const { return _grid.size(); }
    
        std::vector<T>& getGrid(void) { return _grid; }
        const std::vector<T>& getGrid(void) const { return _grid; }
        
        std::array<size_t, 3> dim() const { return {_x, _y, _z}; }
        std::array<size_t, 3> getDim() const { return dim(); }
    
        std::array<double, 3> const & getOrigin() const { return _origin; }
    

        // Returns a reference to the element at position i,j,k postion 
        // in the grid.
        // The function automatically checks whether i,j,k is within the bounds 
        // of valid elements in the vector, throwing an out_of_range exception 
        // if it is not. This is in contrast with member operator(), that does 
        // not check against bounds.
        typename std::vector<T>::reference at(size_t i, size_t j, size_t k)
        {
            if(i >= _x or j >= _y or k >= _z)
            {
                std::string s = String::format("Grid: invalid index %dx%dx%d (Grid is %dx%dx%d)", i, j, k, _x, _y, _z);
                throw std::out_of_range(s);
            }
            return _grid[i*_y*_z + j*_z + k];
        }
        
        typename std::vector<T>::const_reference at(size_t i, size_t j, size_t k) const
        {
            if(i >= _x or j >= _y or k >= _z)
            {
                std::string s = String::format("Grid: invalid index %dx%dx%d (Grid is %dx%dx%d)", i, j, k, _x, _y, _z);
                throw std::out_of_range(s);
            }
            return _grid[i*_y*_z + j*_z + k];           
        }
        
        // Returns a const reference to the element at position i,j,k in the grid.
        typename std::vector<T>::reference operator () (size_t i, size_t j, size_t k)
        {
            return _grid[i*_y*_z + j*_z + k];
        }
    
        typename std::vector<T>::const_reference operator () (size_t i, size_t j, size_t k) const
        {
            return _grid[i*_y*_z + j*_z + k];
        }
        
        // Returns a reference to the nearest element at position x,y,z in the grid.
        // x,y,z are absolute coordinates in constrat with member at.
        typename std::vector<T>::reference nearestPoint(const double x, const double y, const double z)
        {
            size_t i = size_t(floor((std::max(0.0, x - _origin[0]) / _dx)+0.5));
            size_t j = size_t(floor((std::max(0.0, y - _origin[1]) / _dx)+0.5));
            size_t k = size_t(floor((std::max(0.0, z - _origin[2]) / _dx)+0.5));
            i = std::min(i, _x - 1);
            j = std::min(j, _y - 1);
            k = std::min(k, _z - 1);
            return at(i, j, k);
        }
        
        typename std::vector<T>::const_reference nearestPoint(const double x, const double y, const double z) const
        {
            size_t i = size_t(floor((std::max(0.0, x - _origin[0]) / _dx)+0.5));
            size_t j = size_t(floor((std::max(0.0, y - _origin[1]) / _dx)+0.5));
            size_t k = size_t(floor((std::max(0.0, z - _origin[2]) / _dx)+0.5));
            i = std::min(i, _x - 1);
            j = std::min(j, _y - 1);
            k = std::min(k, _z - 1);
            return at(i, j, k);
        }

        std::array<size_t, 3> nearestPointIndices(const double x, const double y, const double z) const
        {
            size_t i = size_t(floor((std::max(0.0, x - _origin[0]) / _dx)+0.5));
            size_t j = size_t(floor((std::max(0.0, y - _origin[1]) / _dx)+0.5));
            size_t k = size_t(floor((std::max(0.0, z - _origin[2]) / _dx)+0.5));
            i = std::min(i, _x - 1);
            j = std::min(j, _y - 1);
            k = std::min(k, _z - 1);
            return {i, j, k};
        }

        std::array<float, 3> nearestPointCoordinates(const double x, const double y, const double z) const
        {
            auto ijk = nearestPointIndices(x, y, z);
            float thex = _origin[0] + ijk[0] * _dx;
            float they = _origin[1] + ijk[1] * _dx;
            float thez = _origin[2] + ijk[2] * _dx;
            return {thex, they, thez};
        }

        std::array<float, 3> coordinates(const size_t i, const size_t j, const size_t k) const
        {
            float x = _origin[0] + i * _dx;
            float y = _origin[1] + j * _dx;
            float z = _origin[2] + k * _dx;
            return {x, y, z};
        }
 
    
    private:
        std::vector<T> _grid;
        // grid dimensions
        size_t _x, _y, _z;
        // origin of the grid
        std::array<double, 3> _origin;
        // spacing of the grid
        double _dx;
};

#endif
