
#include "ConfigParser.hpp"
#include "utils.hpp"
#include "logging.hpp"

#include <string>


void ConfigParser::read()
{
    std::ifstream infile;
    utils::openread(_filename, infile);

    ConfigSection s = ConfigSection(_defaultSection);

    std::string line;
    while (std::getline(infile, line))
    {
        line = String::trim(line);

        if (line != "" and line[0] != '#')
        {
            /* This is a section header */
            if (line[0] == '[')
            {
                if (s.getName() != "" and s.getName() != "DEFAULT")
                {
                    _sections.push_back(s);
                }
                s.clear();
                s = ConfigSection(_defaultSection);
                s.setName(line.substr(1, line.size()-2));
                /* DEFAULT section must be first */
                if (s.getName() == "DEFAULT" and _sections.size() != 0)
                {
                    std::string msg = String::format("%s: DEFAULT section have to be the first section in the file",
                                                    _filename.c_str());
                    logging::fatal(msg);
                }

                // Error if section name is misformatted.
                if (line[line.size()-1] != ']')
                {
                    std::string msg = String::format("%s: misformatted section name '%s'", 
                                                     _filename.c_str(), line.c_str());
                    logging::fatal(msg);
                }
            }
            /* This is a parameter */
            else
            {
                size_t sepi = line.find("=");
                if (sepi == std::string::npos)
                {
                    std::string msg = String::format("%s: misformatted parameter '%s' (format is param = value)",
                                                    _filename.c_str(), line.c_str());
                    logging::fatal(msg);
                }
                std::string p = line.substr(0, line.find("="));
                std::string v = line.substr(line.find("=")+1, line.size());
                p = String::trim(p);
                v = String::trim(v);
                if (s.getName() == "DEFAULT")
                {
                    _defaultSection.addParameter(p, v);
                }
                else
                {
                    s.addParameter(p, v);
                }
            }
        }
    }
    if (s.getName() != "" and s.getName() != "DEFAULT") _sections.push_back(s); 
    infile.close();
}

// Description:
// Print function.
std::ostream& operator<<(std::ostream& os, const ConfigParser &cfg)
{
    os << "ConfigParser: " << cfg.getFilename() << std::endl;
    
    os << "[DEFAULT]" << std::endl;
    for (auto p:cfg.getDefaultSection().getParameters())
    {
        os << "    " << p.first << " = " << p.second << std::endl;
    }
    
    
    for (auto section:cfg.getSections())
    {
        os << "[" << section.getName() << "]" << std::endl;
        for (auto p:section.getParameters())
        {
            os << "    " << p.first << " = " << p.second << std::endl;
        }
    }
    return os;
}



/******************************************************************
 *
 * ConfigSection methods
 *
 *****************************************************************/

std::string ConfigSection::getParameter(const std::string name) const
{
    for (const ConfigParam & p:_parameters)
    {
        if (p.first == name)
        {
            return p.second;
        }
    }
    throw NoSuchParameterException(name);
    return "";
}

void ConfigSection::addParameter(const std::string name, const std::string value)
{
    _parameters.push_back(ConfigParam(name, value));
}







