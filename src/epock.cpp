

#include "utils.hpp"
#include "version.hpp"
#include "xdrfile.h"
#include "xdrfile_xtc.h"

#include "Analysis.hpp"
#include "AnalysisCavityTraj.hpp"
#include "AnalysisPathSearch.hpp"
#include "AnalysisPoreProfile.hpp"
#include "ArgumentParser.hpp"
#include "Atom.hpp"
#include "Cavity.hpp"
#include "ConfigParserEpock.hpp"
#include "Point.hpp"
#include "Region.hpp"
#include "Topology.hpp"
#include "Trajectory.hpp"

#include <iomanip>      // std::setw, std::setprecision
#include <iostream>     // std::cout, std::cerr
#include <map>          // std::map
#include <ostream>      // std::ostream
#include <string>       // std::string
#include <vector>       // std::vector
#include <ctime>


#include "logging.hpp"

static void writeVolume(std::ostream & output, const std::vector<Cavity *> cavities, const float t);
static void writeVolumeHeader(std::ostream & output, const std::vector<Cavity *> cavities);
static void updateCoordinates(std::vector<Cavity *> & cavities, std::vector<Atom> & atoms, const Trajectory & traj);
static void checkRadii(const Topology & top);


static Topology readTopology(const ArgumentParser & parser)
{
    logging::info("Reading topology file " + parser.topname);
    Topology top(parser.topname);
    if (parser.martini)
    {
        logging::info("Martini detected. Bead radii set to 4.7");
        top.setRadii(4.7);
    }
    else if (parser.customRadii) 
    {
        logging::info("Using custom radius file " + parser.radiiname);
        top.setRadii(parser.radiiname);
    }
    checkRadii(top);
    return top;
}

int main(int argc, char *argv[])
{
    clock_t start, end;
    double elapsed;
    clock_t frameTimer;
    double frameTotalElapsed = 0.0;
    
    start = clock();
    // Command line parsing.
    ArgumentParser parser;
    parser.parseArgs(argc, argv);

    // Enable verbose mode if required.
    if (parser.verbose) logging::setLevel(logging::logINFO);

    // Vector that will contain the analyzes to run.
    std::vector<Analysis *> analyzes;

    // Configuration file parser set up and read.
    logging::info("Parsing configuration file");
    std::vector<Cavity *> cavities;
    ConfigParserEpock confParser;
    confParser.setFilename(parser.cfgname);
    confParser.setCavities(&cavities);
    confParser.setUseMartini(parser.martini);
    confParser.read();

    // Sanitary check.
    if (cavities.size() == 0)
    {
        logging::fatal("No cavity read from " + parser.cfgname);
    }

    // Read coordinate file.
    Topology top = readTopology(parser);

    // If user wants to calculate the volume of the molecule, do it and 
    // exit.
    if (parser.calcMolecule)
    {
        logging::info("Calculating the volume of the molecule");
        float v = volumeSetAtoms(top.getAtoms(), cavities[0]->getPrecision());
        std::string outputString = String::format("Molecular volume: %.2f A^3", v);
        logging::info(outputString);
        if (not parser.verbose or parser.outputVolume)
        {
            std::ostream * output = new std::ofstream(parser.outputname);
            if (not output->good())
            {
                logging::fatal("Cannot open output file " + parser.outputname);
            }
            *output << outputString << std::endl;
        }
        exit(0);
    }
    
    // Dry-run mode: output the grid as a PDB file and exit.
    if (parser.dryrun)
    {
        logging::info("Dry-run mode: writing the cavity bead files");
        for (size_t i=0; i<cavities.size(); ++i)
        {
            Cavity * c = cavities[i];
            c->ir2pdb();
            delete c;
        }
        exit(0);
    }
    
    // More initialization
    for (Cavity * c : cavities)
    {
        if (c->getCountContrib())
        {
            c->setMolecule(&top.getAtoms());
        }
    }

    // Initialize the trajectory: if user doesn't provide a trajectory,
    // then run the tool on the topology which is considered as a single 
    // frame trajectory.
    logging::info("Opening trajectory file " + parser.trajname);
    Trajectory * traj = newTrajectory(parser.trajname, top.getNumberOfAtoms());
    
    // Initialize the volume output file
    std::ostream * output = &std::cout;
    if (parser.outputVolume) 
    { 
        output = new std::ofstream(parser.outputname);
        if (not output->good())
        {
            logging::fatal("Cannot open output file " + parser.outputname);
        }
    }

    //
    // Initialize analyzes.
    //

    std::map<std::string, Cavity *> name2cav;
    for (auto c : cavities)
    {
        name2cav[c->getName()] = c;
    }

    const auto pathSearch = confParser.getPathSearch();
    for (size_t i=0; i<cavities.size(); ++i)
    {
        Cavity * origin = cavities[i];
        for (std::string name : pathSearch[i])
        {
            Cavity * target = name2cav[name];
            Analysis * a = new AnalysisPathSearch(*origin, *target, top, *traj);
            analyzes.push_back(a);
        }
    }
    
    for (const auto c : cavities)
    {
        if (c->getUsePoreProfile())
        {
            Analysis * a = new AnalysisPoreProfile(*c, top, *traj);
            analyzes.push_back(a);
        }
        if (parser.outputCavXTC)
        {
            Analysis * a = new AnalysisCavityTraj(*c, top, *traj);
            analyzes.push_back(a);
        }
    }

    // Write volume file header: cavity's name.
    logging::info("Starting volume calculation");
    writeVolumeHeader(*output, cavities);

    size_t frameid = 0;
    for (frameid=0; traj->hasNextFrame(); ++frameid)
    {
        frameTimer = clock();
        if (parser.verbose)
            logging::progress(String::format("Frame %d", frameid));

        bool readSuccess = traj->nextFrame();
        float curtime = traj->getTime();
        bool hasStarted = curtime >= parser.start;
        bool hasNotYetFinishedButProbablySoon = (parser.end < 0 or curtime <= parser.end);
        
        if (readSuccess and hasStarted and hasNotYetFinishedButProbablySoon)
        {
            bool modt = (parser.timestep < 0 or (fmod(curtime, parser.timestep) < 0.001));
            if (modt)
            {
                updateCoordinates(cavities, top.getAtoms(), *traj);
                for (Cavity * c : cavities)
                {
                    c->calculate();
                }

                for (auto ana : analyzes)
                {
                    ana->run();
                }

                // Buffered writing
                writeVolume(*output, cavities, curtime);
            }
        }
        frameTotalElapsed += double(clock() - frameTimer) / CLOCKS_PER_SEC;
    }
    if (parser.verbose)
        logging::progress_done();
    logging::info("Volume calculation done");

    // Output atom contribution
    for (Cavity * const c : cavities)
    {
        if (c->getCountContrib())
        {
            c->writeContrib();
        }
    }

    //
    // Free some memory.
    //

    // Do not try to delete output if output is stdout.
    if (parser.outputVolume)    delete output;
    for (auto c : cavities)     delete c;
    for (auto a : analyzes)     delete a;
    delete traj;
    
    end = clock();
    elapsed = double(end - start) / CLOCKS_PER_SEC;
    logging::info(String::format("Run time: %.2fs", elapsed));
    logging::info(String::format("Average time per frame: %.2fs", frameTotalElapsed / ((float)frameid)));
    logging::info("Exiting with success.");
    return 0;
}


static void writeVolume(std::ostream & output, const std::vector<Cavity *> cavities, const float t)
{
    output.setf(std::ios::fixed);
    output << std::setw(10) << std::setprecision(2) << t << " ";
    for (Cavity * c : cavities)
    {
        output << std::setw(8) << std::setprecision(2) <<  c->getVolume() << " " ;
    }
    output << "\n";
}

static void writeVolumeHeader(std::ostream & output, const std::vector<Cavity *> cavities)
{
    output << std::setw(10) << "Time" << " ";
    for (const Cavity * const c:cavities)
    {
        output << std::setw(8) << c->getName() << " ";
    }
    output << "\n";
}


// Description:
// Update the atom coordinates with the data read from the trajectory
// and calculate the new grid points.
static void updateCoordinates(std::vector<Cavity *> & cavities, 
                              std::vector<Atom> & atoms, 
                              const Trajectory & traj)
{
    for (size_t i=0; i<traj.getNumberOfAtoms(); ++i)
    {
        Atom & a = atoms[i];
        a.x.x = traj.getXAt(i);
        a.x.y = traj.getYAt(i);
        a.x.z = traj.getZAt(i);
    }
    for (Cavity * c : cavities)
    {  
        c->setCoordinates(atoms);
    }
}

// Description:
// Check if all atom have been assigned a radius (i.e. radius > 0.0).
// If not, print an error message and exit.
static void checkRadii(const Topology & top)
{
    bool shouldDie = false;
    for (auto a : top.getAtoms())
    {
        if (a.vdw < 0.0)
        {
            logging::error("Unknown radius for atom " + a.name);
            shouldDie = true;
        }
    }
    if (shouldDie)
    {
        logging::fatal("Please provide it using the option --radii with a file "
                       "containing the missing radii.");
    }
}
