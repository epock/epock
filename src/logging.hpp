

#ifndef __LOGGING_HPP__
#define __LOGGING_HPP__

#include <map>
#include <ostream>
#include <string>

namespace logging
{
    enum eLogLevel
    {
        logDEBUG,
        logDEBUG1,
        logDEBUG2,
        logDEBUG3,
        logINFO,
        logWARNING,
        logERROR,
        logCRITICAL
    };
    
    enum eLogStream
    {
        logSTRINGSTREAM,
        logSTDOUT,
        logSTDERR,
        logOFSTREAM
    };

    static std::map<size_t, std::string> LEVELNAMES = 
    {
        {logDEBUG, "DEBUG"},
        {logDEBUG1, "DEBUG"},
        {logDEBUG2, "DEBUG"},
        {logDEBUG3, "DEBUG"},
        {logINFO, "INFO"},
        {logWARNING, "WARNING"},
        {logERROR, "ERROR"},
        {logCRITICAL, "FATAL ERROR"}
    };

    static std::map<std::string, std::string> TTYCOLORS =
    {
        {"CLEAR"    , "\033[0m"},
        {"BLACK"    , "\033[30m"},
        {"RED"      , "\033[31m"},
        {"GREEN"    , "\033[32m"},
        {"YELLOW"   , "\033[33m"},
        {"BLUE"     , "\033[34m"},
        {"MAGENTA"  , "\033[35m"},
        {"CYAN"     , "\033[36m"},
        {"WHITE"    , "\033[37m"},
    };

    static std::map<size_t, std::string> LEVELCOLORS = 
    {
        {logDEBUG, "WHITE"},
        {logDEBUG1, "WHITE"},
        {logDEBUG2, "WHITE"},
        {logDEBUG3, "WHITE"},
        {logINFO, "WHITE"},
        {logWARNING, "YELLOW"},
        {logERROR, "RED"},
        {logCRITICAL, "RED"}
    };

    class Logger
    {
        public:
            Logger();
            Logger(std::string fname);
            Logger(const Logger & l);
            ~Logger();

            Logger & operator=(const Logger & l);

            inline void setPrefix(const std::string & s) { _prefix = s; }
            inline std::string getPrefix(void) const { return _prefix; }

            inline size_t getLevel(void) const { return _level; }
            inline void setLevel(const size_t level) { _level = level; }

            inline void setEOL(const std::string eol) { _eol = eol; }
            inline std::string getEOL(void) const { return _eol; }    

            void close();
    
            void log(const std::string & s, size_t level) const;
            void progress(const std::string & s) const;
            void info(const std::string & s) const;
            void warn(const std::string & s) const;
            void warning(const std::string & s) const;
            void error(const std::string & s) const;
            void critical(const std::string & s, size_t status);
            void debug(const std::string & s, size_t level=logDEBUG) const;

            void colorize(std::string s, std::string color) const;

            void print(const std::string & s) const;

        
        protected:
            std::ostream *  _os;
            size_t          _streamType;
            size_t          _level;
            std::string     _prefix;
            std::string     _eol;
            bool            _useColors;
    };

    void setLevel(size_t level);
    void setLogFilename(const std::string & fname);
    void setEOL(std::string eol);

    void shutdown(void);

    void progress(const std::string & s);
    void progress_done();

    void log(const std::string & s, size_t level);
    void info(const std::string & s);
    void warn(const std::string & s);
    void warning(const std::string & s);
    void error(const std::string & s);
    void critical(const std::string & s, size_t status=1);
    void fatal(const std::string & s, size_t status=1);
    void debug(const std::string & s, size_t level=logDEBUG);
    

}



#endif
