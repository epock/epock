
#ifndef __ANALYSIS_HPP__
#define __ANALYSIS_HPP__

#include <iostream>
#include <string>

#include "Cavity.hpp"
#include "Topology.hpp"
#include "Trajectory.hpp"


class Analysis
{
    public:
        Analysis(Cavity & cav, Topology & top, Trajectory & traj);

        virtual ~Analysis();

        Analysis(const Analysis & a) = delete;
        Analysis & operator=(const Analysis & a) = delete;

        void run(void);

    protected:
        void _openOutputFile(void);
        void _prerun(void);
        void _postrun(void);
        virtual void _compute(void) = 0;
        virtual void _writeHeader(void) = 0;


        Cavity &        _cavity;
        Topology &      _top;
        Trajectory &    _traj;
        std::string     _outputname;
        std::ofstream * _output;
};

#endif 