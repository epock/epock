

#include "Region.hpp"

#pragma mark -
#pragma mark SphereRegion methods

// Description:
// Calculate the boundaries of the minimal englobbing box.
// b is filled as follows:
//  b[0] = xmin
//  b[1] = xmax
//  b[2] = ymin
//  b[3] = ymax
//  b[4] = zmin
//  b[5] = zmax
void SphereRegion::boundaries(std::array<float, 6> &b) const
{
    b[0] = _center.x - _radius;
    b[1] = _center.x + _radius;
    b[2] = _center.y - _radius;
    b[3] = _center.y + _radius;
    b[4] = _center.z - _radius;
    b[5] = _center.z + _radius;
}

bool SphereRegion::intersects(const Point &p, const float padding) const
{
    return p.dist2(_center) <= ((_radius + padding) * (_radius + padding));
}

#pragma mark -
#pragma mark BoxRegion methods


// Description:
// Calculate the box minimax and maximax in each dimension
void BoxRegion::boundaries(std::array<float, 6> &b) const
{
    b[0] = _center.x - _dim.x * 0.5;
    b[1] = _center.x + _dim.x * 0.5;
    b[2] = _center.y - _dim.y * 0.5;
    b[3] = _center.y + _dim.y * 0.5;
    b[4] = _center.z - _dim.z * 0.5;
    b[5] = _center.z + _dim.z * 0.5;
}

bool BoxRegion::intersects(const Point &p, const float padding) const
{
    // Make sure padding (which is the sphere radius) is positive.
    // Should probably throw std::range_error but its probably slower.
    float r = fabs(padding);
    float xmin = _center.x - _dim.x * 0.5 - r;
    float xmax = _center.x + _dim.x * 0.5 + r;
    float ymin = _center.y - _dim.y * 0.5 - r;
    float ymax = _center.y + _dim.y * 0.5 + r;
    float zmin = _center.z - _dim.z * 0.5 - r;
    float zmax = _center.z + _dim.z * 0.5 + r;
        
    return p.x.x >= xmin and p.x.x <= xmax and \
           p.x.y >= ymin and p.x.y <= ymax and \
           p.x.z >= zmin and p.x.z <= zmax;
}


#pragma mark -
#pragma mark CylinderRegion methods

// Description:
// Calculate the boundaries of the minimal englobbing box.
void CylinderRegion::boundaries(std::array<float, 6> &b) const
{   
    // This is actually NOT the minimal bounding box (but close!).
    b[0] = (_x1.x.x < _x2.x.x ? _x1.x.x : _x2.x.x) - _radius;
    b[1] = (_x1.x.x > _x2.x.x ? _x1.x.x : _x2.x.x) + _radius;
    b[2] = (_x1.x.y < _x2.x.y ? _x1.x.y : _x2.x.y) - _radius;
    b[3] = (_x1.x.y > _x2.x.y ? _x1.x.y : _x2.x.y) + _radius;
    b[4] = (_x1.x.z < _x2.x.z ? _x1.x.z : _x2.x.z) - _radius;
    b[5] = (_x1.x.z > _x2.x.z ? _x1.x.z : _x2.x.z) + _radius;
}


#include <iostream>
#include <cmath>

// Adapted from  http://www.flipcode.com/archives/Fast_Point-In-Cylinder_Test.shtml
bool CylinderRegion::intersects(const Point &p, const float padding) const
{
    // TODO: this part of code is repeated each time a point is tested 
    // to be inside the cylinder or not.
    // It should not be the case.
    // It is however like that because inbounds is a const method (as 
    // the user probably expect) so we can't modify members.
    Point cap1, cap2;
    
    if (_x1.x.z < _x2.x.z)
    {
        cap1 = _x1; cap1.x.z -= padding;
        cap2 = _x2; cap2.x.z += padding;        
    }
    else
    {
        cap1 = _x1; cap1.x.z += padding;
        cap2 = _x2; cap2.x.z -= padding;        
    }
    
    Vec3f axis = cap2.x - cap1.x;
    float axislensq = cap1.dist2(cap2);
    // END TODO.
    
    Vec3f pdx = p.x - cap1.x;
    
    // Dot the _axis and pd vectors to see if p lies behind the 
    // cylinder cap at pt1.x, pt1.y, pt1.z
    float dot = pdx.x * axis.x + pdx.y * axis.y + pdx.z * axis.z;
    
    // If dot is less than zero the point is behind the pt1 cap.
    // If greater than the cylinder axis line segment length squared
    // then the point is outside the other end cap at pt2.
    if((dot < 0.0f) || (dot > axislensq)) return false;
    
    // Point lies within the parallel caps, so find
    // squared distance from the point to the cylinder axis.
    Vec3f x0 = p.x;
    Vec3f x1 = _x1.x;
    Vec3f x2 = _x2.x;
    float dsq = ((x2 - x1).cross(x1 - x0)).length2() / (x2 - x1).length2();
   
    return dsq <= ((_radius + padding) * (_radius + padding));
}



// Adapted from  http://www.flipcode.com/archives/Fast_Point-In-Cylinder_Test.shtml
//bool CylinderRegion::inbounds(const Point &p, const float padding) const
//{
//  Vec3f pdx = p.x - _x1.x;
//  
//  // Dot the _axis and pd vectors to see if p lies behind the 
//  // cylinder cap at pt1.x, pt1.y, pt1.z
//  float dot = pdx.x * _axis.x + pdx.y * _axis.y + pdx.z * _axis.z;
//
//  // If dot is less than zero the point is behind the pt1 cap.
//  // If greater than the cylinder axis line segment length squared
//  // then the point is outside the other end cap at pt2.
//  if((dot < 0.0f) || (dot > _axislensq)) return false;
//
//  // Point lies within the parallel caps, so find
//  // distance squared from point to line, using the fact that sin^2 + cos^2 = 1
//  // the dot = cos() * |d||pd|, and cross*cross = sin^2 * |d|^2 * |pd|^2
//  // Carefull: '*' means mult for scalars and dotproduct for vectors
//  // In short, where dist is pt distance to cyl axis: 
//  // dist = sin( pd to d ) * |pd|
//  // distsq = dsq = (1 - cos^2( pd to d)) * |pd|^2
//  // dsq = ( 1 - (pd * d)^2 / (|pd|^2 * |d|^2) ) * |pd|^2
//  // dsq = pd * pd - dot * dot / lengthsq
//  //  where lengthsq is d*d or |d|^2 that is passed into this function 
//  
//  // distance squared to the cylinder axis:   
//  float dsq = (pdx.x * pdx.x + pdx.y * pdx.y + pdx.z * pdx.z) - dot * dot / _axislensq;
//  
//  bool b = dsq <= ((_radius + padding) * (_radius + padding));
//  if (b)
//  {
//      cerr << p.x.x << " "<< p.x.y << " "<< p.x.z << " " << dot << endl;
//  }
//  return b;
//}



