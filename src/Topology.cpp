
#include "Topology.hpp"
#include "logging.hpp"
#include "utils.hpp"

#include <fstream>
#include <iostream>
#include <string>
#include <stdexcept>


#pragma mark -
#pragma mark Set Radius Methods

// Description:
// Set atom radius.
void Topology::setRadii(const float & f)
{
    for (Atom & a : _atoms)
    {
        a.vdw = f;
    }
}

void Topology::setRadii(const std::vector<float> & v)
{
    if (_atoms.size() != v.size())
    {
        std::string s = String::format("Trying to initialize %d atoms with %d radii.", 
                                      _atoms.size(), v.size());
        throw std::out_of_range(s);
    }
    for (size_t i=0; i<_atoms.size(); ++i)
    {
        _atoms[i].vdw = v[i];
    }
}

void Topology::setRadii(const std::string & fname)
{
    std::ifstream infile;
    utils::openread(fname, infile);
    
    std::map<std::string, float> rMap;
    float r = 0.0;
    std::string line;
    for (size_t i=1; std::getline(infile, line); ++i)
    {
        
        line = String::trim(line.substr(0, line.find("#")));
        if (not line.empty())
        {
            const std::vector<std::string> tokens = String::split(line);
            if (tokens.size() != 2)
            {
                std::string msg = String::format("%s: bad format at line %d "
                                                 "(expected \"atom_name atom_radius\")",
                                                 fname.c_str(), i);
                logging::fatal(msg);
            }
            if (not from_string<float>(r, tokens[1]))
            {
                std::string msg = String::format("%s: bad format at line %d "
                                                 "(expected \"atom_name atom_radius\")",
                                                 fname.c_str(), i);
                logging::fatal(msg);
            }
            rMap[tokens[0]] = r;
        }
    }
    
    for (Atom & a : _atoms)
    {
        if (rMap.find(a.name) != rMap.end())
        {
            a.vdw = rMap.at(a.name);
        }
    }
}



#pragma mark -
#pragma mark Constructor & Destructor

// The program tries to guess atom radii based on their name.
std::map<std::string, float> radiiMap = VDW_RADII;

// Description:
// Empty constructor.
// Should be used with Topology::read(void).
Topology::Topology():
    _fname(""),
    _atoms()
{
}


// Description:
// Standard method to read a topology.
// File type is guessed according to file's extension.
Topology::Topology(std::string fname):
    _fname(fname),
    _atoms()
{
    read();
}

#pragma mark -
#pragma mark Read Topology Methods

void Topology::read(void)
{
    std::string ext = _fname.substr(_fname.find_last_of("."));
    if (ext == ".pdb")
    {
        readPDB(_fname, _atoms);
    }
    else if (ext == ".gro")
    {
        readGRO(_fname, _atoms);
    }    
    else
    {
        std::string msg = String::format("%s: unrecognized extension '%s'",
                                         _fname.c_str(), ext.c_str());
        logging::fatal(msg);
    }
}

// Description:
// Read a PDB formatted file and fill-in a vector.
void Topology::readPDB(const std::string fname, std::vector<Atom> & atoms)
{
    std::ifstream infile;
    utils::openread(fname, infile);
    
    std::string line;
    std::getline(infile, line);
    
    // Dies if file is empty.
    if (infile.eof())
    {
        std::string msg = String::format("%s: empty file", fname.c_str());
        logging::fatal(msg);
    }

    // Loop over lines.
    do
    {
        std::string rec (line.substr(0, 6));
        if (rec == "ATOM  " or rec == "HETATM")
        {
            Atom a = Atom();
            a.name    = String::trim(line.substr(12, 4));
            a.resname = String::trim(line.substr(17, 3));
            a.chain   = String::trim(line.substr(21, 1));
            from_string<int>(a.index, line.substr(6, 5));
            from_string<int>(a.resid, line.substr(22, 4));
            from_string<float>(a.x.x, line.substr(30, 8));
            from_string<float>(a.x.y, line.substr(38, 8));
            from_string<float>(a.x.z, line.substr(46, 8));
            a.element = String::first_alpha(a.name);
            
            if (VDW_RADII.find(a.element) != VDW_RADII.end())
            {
                a.vdw = VDW_RADII.at(a.element);
            }
            atoms.push_back(a);
        }
    } while (std::getline(infile, line));
    if (atoms.empty())
    {
        std::string msg = String::format("%s: no atom read from file", fname.c_str());
        logging::fatal(msg);
    }
}

void Topology::readPDB(const std::string fname, std::vector<Point> & points)
{
    std::ifstream infile;
    utils::openread(fname, infile);
    
    std::string line;
    std::getline(infile, line);
    
    // Dies if file is empty.
    if (infile.eof())
    {
        std::string msg = String::format("%s: empty file", fname.c_str());
        logging::fatal(msg);
    }

    // Loop over lines.
    do
    {
        std::string rec (line.substr(0, 6));
        if (rec == "ATOM  " or rec == "HETATM")
        {
            float x, y, z;
            from_string<float>(x, line.substr(30, 8));
            from_string<float>(y, line.substr(38, 8));
            from_string<float>(z, line.substr(46, 8));
            points.push_back(Point(x, y, z));
        }
    } while (std::getline(infile, line));
    if (points.empty())
    {
        std::string msg = String::format("%s: no atom read from file", fname.c_str());
        logging::fatal(msg);
    }
}


// Description:
// Read a gromacs GRO formatted file and fill-in a vector.
void Topology::readGRO(const std::string fname, std::vector<Atom> & atoms)
{
    std::ifstream infile;
    utils::openread(fname, infile);
    
    std::string line;
    size_t natoms = 0;
    
    // Should do some sanitary checks here.
    
    // Skip header line.
    std::getline(infile, line);

    // Check for empty file.
    if (infile.eof())
    {
        std::string msg = String::format("%s: empty file", fname.c_str());
        logging::fatal(msg);
    }
    
    // Get the number of atoms.
    std::getline(infile, line);
    if (not from_string<size_t>(natoms, line))
    {
        std::string msg = String::format("%s: could not read the number of "
                                         "atoms on the second line "
                                         "(got '%s' instead)",
                                         fname.c_str(), line.c_str());
        logging::fatal(msg);
    }
    
    for (size_t i=0; i<natoms; ++i)
    {
        if (not std::getline(infile, line))
        {
            std::string msg = String::format("%s: excepted %d atoms but reached "
                                             "end-of-file after %d atoms",
                                             fname.c_str(), natoms, i+1); 
            logging::fatal(msg);
        }
        
        Atom a = Atom();
        from_string<int>(a.resid, line.substr(0, 5));
        a.resname = String::trim(line.substr(5, 5));
        a.name = String::trim(line.substr(10, 5));
        from_string<int>(a.index, line.substr(15, 5));
        from_string<float>(a.x.x, line.substr(20, 8));
        from_string<float>(a.x.y, line.substr(28, 8));
        from_string<float>(a.x.z, line.substr(36, 8));
        a.element = String::first_alpha(a.name);
        a.x.x *= 10;
        a.x.y *= 10;
        a.x.z *= 10;
        
        if (VDW_RADII.find(a.element) != VDW_RADII.end())
        {
            a.vdw = VDW_RADII.at(a.element);
        }
        atoms.push_back(a);
    }
    if (atoms.empty())
    {
        std::string msg = String::format("%s: no atom read from file", fname.c_str());
        logging::fatal(msg);
    }
}








