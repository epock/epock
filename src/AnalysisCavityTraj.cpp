

#include "AnalysisCavityTraj.hpp"
#include "Point.hpp"
#include "Vec3f.hpp"

#include <iomanip>  // std::setw, std::setprecision
#include <vector>   // std::vector


// TO BE REMOVED
#include <iostream>
using std::cout;
using std::endl;
// END TO BE REMOVED

AnalysisCavityTraj::AnalysisCavityTraj(Cavity & cav,
                                       Topology & top,
                                       Trajectory & traj):
    Analysis(cav, top, traj),
    _xtc(nullptr),
    _xxtc(nullptr),
    _stepxtc(0)
{
    _writeHeader();
    _prerun();
}

AnalysisCavityTraj::~AnalysisCavityTraj()
{
    xdrfile_close(_xtc);
    delete _xxtc;
}

void AnalysisCavityTraj::_writeHeader(void)
{
    // Empty since not output file is written (not a real analysis).
}

void AnalysisCavityTraj::_prerun(void)
{
    // Open XTC file.
    std::string fname = _cavity.getName() + ".xtc";
    _xtc = xdrfile_open(fname.c_str(), "w");
    if (not _xtc)   
    {
        std::cerr << "ERROR: Cannot open output file '" << fname << "'" << std::endl;
        exit(1);
    }

    // Allocate some memory for XTC coordinate array.
    _xxtc = new rvec[_cavity.getNumberOfPoints()];

    // Write a PDB file containing all points.
    _cavity.ir2pdb(_cavity.getName() + ".pdb");
}

void AnalysisCavityTraj::_compute(void)
{
    matrix box = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
    size_t natoms = _cavity.getNumberOfPoints();
    float xref=-1000.0, yref=-1000.0, zref=-1000.0;

    // Find the first point that belongs to the cavity.
    for (const auto pt : _cavity.getCavityPoints())
    {
        if (pt->iscavity)
        {
            xref = pt->x.x;
            yref = pt->x.y;
            zref = pt->x.z;
            break;
        }
    }
    
    // Copy points coordinates into a float array.
    const auto points = _cavity.getAllPoints();
    for (size_t i=0; i<natoms; ++i)
    {
        const Point & pt = points[i];
        if (pt.iscavity)
        {
            _xxtc[i][0] = pt.x.x * 0.1;
            _xxtc[i][1] = pt.x.y * 0.1;
            _xxtc[i][2] = pt.x.z * 0.1;
        }
        else
        {
            _xxtc[i][0] = xref * 0.1;
            _xxtc[i][1] = yref * 0.1;
            _xxtc[i][2] = zref * 0.1;
        }
    }

    // Write the step in the xdr file.
    write_xtc(_xtc, natoms, _stepxtc, (float) _stepxtc, box, _xxtc, 1000.0);
    _stepxtc++; 
}
