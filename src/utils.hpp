

#ifndef __UTILS_H__
#define __UTILS_H__

#include "logging.hpp"

#include <algorithm>
#include <cctype>
#include <cstdarg>
#include <exception>
#include <fstream>
#include <functional>
#include <iterator>
#include <locale>
#include <string>
#include <sstream>
#include <vector>




class MyException: public std::exception
{
    public:
        MyException():_msg("MyException have been thrown") {};
        MyException(std::string s):_msg(s) {};
    
    virtual const char * what() const throw()
    {
        return _msg.c_str();
    }
    
    protected:
        std::string _msg;
};

class FileNotFoundException: public std::exception
{
    public:
        FileNotFoundException(std::string fname):
            _msg("File not found: " + fname) {};
    
    virtual const char * what() const throw()
    {
        return _msg.c_str();
    }
    
    protected:
        std::string _msg;
};

class EndOfFileException: public std::exception
{
    public:
        EndOfFileException():
            _msg("Reached end of file") {};
    
    virtual const char * what() const throw()
    {
        return _msg.c_str();
    }
    
    protected:
        std::string _msg;
};


// String utilities.
namespace String
{
    inline std::string toupper(const std::string & s)
    {
        std::string out = s;
        std::transform(out.begin(), out.end(), out.begin(), ::toupper);
        return out;
    }

    inline std::string tolower(const std::string & s)
    {
        std::string out = s;
        std::transform(out.begin(), out.end(), out.begin(), ::tolower);
        return out;
    }

    //
    // String trim functions.
    // Adapted from http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    //

    // Trim from left.
    inline std::string ltrim(const std::string &s) 
    {
        std::string t = std::string(s);
        t.erase(t.begin(), 
                std::find_if(t.begin(), 
                             t.end(), 
                             std::not1(std::ptr_fun<int, int>(std::isspace))));
        return t;
    }

    // Trim from right.
    inline std::string rtrim(const std::string &s)
    {
        std::string t = std::string(s);
        t.erase(std::find_if(t.rbegin(), 
                             t.rend(), 
                             std::not1(std::ptr_fun<int, int>(std::isspace))).base(), 
                t.end());
        return t;
    }

    // Trim from both left & right.
    inline std::string trim(const std::string &s)
    {
        return ltrim(rtrim(s));
    }


    // Return the first alphabetic character of a string.
    inline std::string first_alpha(const std::string &s)
    {
        for (const char &c:s)
        {
            if (isalpha(c))
            {
                std::stringstream ss;
                std::string fa;
                ss << c;
                ss >> fa;
                return fa;
            }
        }
        return "";
    }

    // Return a vector of the words in the string s using the 
    // space as delimiter.
    inline std::vector<std::string> split(const std::string &s)
    
    {
        std::vector<std::string> tokens;
        std::istringstream iss(s);
        copy(std::istream_iterator<std::string>(iss),
             std::istream_iterator<std::string>(),
             std::back_inserter<std::vector<std::string>>(tokens));
        return tokens;
    }
    
    // Description:
    // String formatting.
    // From http://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf
    inline std::string format(const std::string fmt, ...) 
    {
        int size = 100;
        std::string str;
        va_list ap;
        while (1) {
            str.resize(size);
            va_start(ap, fmt);
            int n = vsnprintf((char *)str.c_str(), size, fmt.c_str(), ap);
            va_end(ap);
            if (n > -1 && n < size) {
                str.resize(n);
                return str;
            }
            if (n > -1)
                size = n + 1;
            else
                size *= 2;
        }
        return str;
    }
}


// Convert a string to any type.
// Adapted from http://forums.codeguru.com/showthread.php?231054-C-String-How-to-convert-a-string-into-a-numeric-type
template <class T> 
bool from_string(T& t, const std::string& s)
{
    std::istringstream iss(s);
    return !(iss >> t).fail();
}


namespace utils
{

    inline void openread(std::string fname, std::ifstream & infile)
    {
        infile.open(fname);
        if (not infile.is_open())
        {
            std::string msg = String::format("Can't open file: '%s'", fname.c_str());
            logging::fatal(msg);
        }
    }
    

    template <typename T1, typename T2>
    inline T1 min(const T1 a, const T2 b) { return (a < b ? a : b); }
    
    template <typename T1, typename T2>
    inline T1 max(const T1 a, const T2 b) { return (a > b ? a : b); }
}


#endif
