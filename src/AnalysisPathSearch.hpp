

#ifndef __ANALYSIS_PATH_SEARCH_HPP__
#define __ANALYSIS_PATH_SEARCH_HPP__

#include "Analysis.hpp"

class AnalysisPathSearch : public Analysis
{
    public:
        AnalysisPathSearch(Cavity & cav,
                           Cavity & target,
                           Topology & top, 
                           Trajectory & traj);

        ~AnalysisPathSearch();

        void _compute(void);


    protected:
        void _writeHeader(void);

        Cavity & _target;
};


#endif