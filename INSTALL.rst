Installation Guide
==================

We recommend to use the binary packages available for Mac OS and Linux that
can be downloaded in :ref:`Epock's download section <lab_download_section>`.


From binaries
-------------

Binary packages include Epock's binary, the Tcl plugin for VMD,
and Python scripts to easily plot Epock outputs. 
Here is the content of a typical tarball:

.. code-block:: bash

    epock-1.0.0rc-Darwin-i386/
    ├── INSTALL
    ├── README
    ├── bin
    │   └── epock
    └── share
        └── epock
            ├── plugins
            │   └── vmd
            │       └── epock1.0
            │           ├── epock.tcl
            │           ├── epock_gui.tcl
            │           ├── epock_results_dynamic_window.tcl
            │           ├── epock_results_window.tcl
            │           ├── graph_radius.tcl
            │           ├── pkgIndex.tcl
            │           ├── run_tk.tcl
            │           └── table.tcl
            └── scripts
                ├── plot.py
                ├── plot_contribution.py
                └── plot_profile.py

We recommend to put the Epock binary in a directory already present in your
:bash:`$PATH`, while the VMD plugin and the python script should be in some 
shared directory.

As an example, to install Epock in /usr/local, just type the following 
commands:

.. code-block:: bash
    
    $ tar xf epock-1.0.0rc-Darwin-i386.tar.gz
    $ sudo mv epock-1.0.0rc-Darwin-i386/bin/epock /usr/local/bin/
    $ sudo mv epock-1.0.0rc-Darwin-i386/share/epock /usr/local/share/

The next step is to make the VMD plugin accessible from inside VMD. 
This step is common to all installation procedures and is detailed in
the `Installing the VMD plugin`_ section.


From sources
------------

To compile Epock from source code, the following packages are required

- `CMake <http://www.cmake.org/>`_ version >= 2.8
- A compiler that supports C++11 standards (e.g `GCC <http://gcc.gnu.org/>`_ version >=4.7)

The procedure is then quite straight forward:

.. code-block:: bash

    $ tar xf epock-1.0.0rc.tar.gz
    $ cd epock-1.0.0rc
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install

If you have any difficulty compiling Epock, please contact the developer team.

The next step is to make the VMD plugin accessible from inside VMD. 
This step is common to all installation procedures and is detailed in
the `Installing the VMD plugin`_ section.

For more information, please refer to the :ref:`extended compilation guide
<lab_extended_compilation_guide>`.


Installing the VMD plugin
-------------------------

After copying the Epock plugin to the right directory (see above),
you have to add these lines in your :bash:`$HOME/.vmdrc` file.

.. code-block:: tcl

    set auto_path [linsert $auto_path 0 "/path/to/directory/share/epock/plugins/vmd"]
    vmd_install_extension epock epock_tk "Analysis/Epock"

**Special note: installation from binary package.**
If you installed epock from a binary package in a directory that is not 
in your :bash:`$PATH`, you may want to change the epock executable
path directly in the VMD plugin.
This can be done by modifying the value of :code:`::epock::exec_path`
in :code:`epock_gui.tcl` (line 47).

